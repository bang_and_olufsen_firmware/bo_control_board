#include "nvs_utilities.hpp"

#include <Arduino.h>

namespace nvs_utilities
{
void set_bool_setting(const char* keyname, nvs_handle_t handle, bool value)
{
  esp_err_t err = nvs_set_u8(handle, keyname, value);
  if(err != ESP_OK)
  {
    log_w("Error saving setting %s: %s", keyname, esp_err_to_name(err));
  }
}

void set_float_setting(const char* keyname, nvs_handle_t handle, float value)
{
  esp_err_t err = nvs_set_blob(handle, keyname, &value, sizeof(value));
  if(err != ESP_OK)
  {
    log_w("Error saving setting %s: %s", keyname, esp_err_to_name(err));
  }
}

void set_uint32_setting(const char* keyname, nvs_handle_t handle, uint32_t value)
{
  esp_err_t err = nvs_set_u32(handle, keyname, value);
  if(err != ESP_OK)
  {
    log_w("Error saving setting %s: %s", keyname, esp_err_to_name(err));
  }
}

float get_float_setting(const char* keyname, nvs_handle_t handle, float default_value)
{
  size_t length = sizeof(default_value);
  float  value;
  if(ESP_OK == nvs_get_blob(handle, keyname, &value, &length))
  {
    return value;
  }
  set_float_setting(keyname, handle, default_value);
  return default_value;
}

bool get_bool_setting(const char* keyname, nvs_handle_t handle, bool default_value)
{
  uint8_t tmp;
  if(ESP_OK == nvs_get_u8(handle, keyname, &tmp))
  {
    return tmp;
  }
  set_bool_setting(keyname, handle, default_value);
  return default_value;
}

uint32_t get_uint32_setting(const char* keyname, nvs_handle_t handle, uint32_t default_value)
{
  uint32_t value;
  if(ESP_OK == nvs_get_u32(handle, keyname, &value))
  {
    return value;
  }
  set_uint32_setting(keyname, handle, default_value);
  return default_value;
}

}  // namespace nvs_utilities