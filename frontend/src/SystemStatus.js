import React, { Component } from "react";
import Container from "@mui/material/Container";
import getDeviceStatRows from "./DeviceStats";
import getDeviceBootStatusRows from "./BootStatus";
import { InfoTable } from "./InfoTable";
import { Paper } from "@mui/material";
import { Button } from "@mui/material";
import { Buffer } from "buffer";
import WysiwygIcon from "@mui/icons-material/Wysiwyg";

function createData(name, value) {
  return { name, value };
}

function getMemoryRows(memory) {
  let memoryrows = [];
  for (const [memorytype, memorystats] of Object.entries(memory)) {
    let rows = [
      createData(memorytype, ""),
      createData("Free", memorystats.total_free_bytes + " B"),
      createData("Minimum Free", memorystats.minimum_free_bytes + " B"),
      createData("Allocated", memorystats.total_allocated_bytes + " B"),
      createData("Largest Block", memorystats.largest_free_block + " B"),
      createData("Allocated Blocks", memorystats.allocated_blocks),
      createData("Free Blocks", memorystats.free_blocks),
      createData("Total Blocks", memorystats.total_blocks),
    ];
    rows[0]["name_style"] = "font-weight:bold";
    memoryrows = memoryrows.concat(rows);
  }
  return memoryrows;
}

function getTaskRows(tasks) {
  const sorted = Object.fromEntries(
    Object.entries(tasks).sort(([,a],[,b]) => b.load-a.load)
);

  let taskrows = [];
  for (const [task, taskstats] of Object.entries(sorted)) {
    let rows = [
      createData(task, "Core " + taskstats.core),
      createData("Load", taskstats.load.toFixed(1) + " %"),
      createData("Free stack", taskstats.stack + " B"),
    ];
    rows[0]["name_style"] = "font-weight:bold";
    taskrows = taskrows.concat(rows);
  }
  return taskrows;
}

function getCanRows(stats) {
  return [
    createData("State", stats.state),
    createData("TX queue", stats.msgs_to_tx),
    createData("RX queue", stats.msgs_to_rx),
    createData("TX errors", stats.tx_error_counter),
    createData("RX errors", stats.rx_error_counter),
    createData("TX failed", stats.tx_failed_count),
    createData("RX overrun", stats.rx_overrun_count),
    createData("Arbitations lost", stats.arb_lost_count),
    createData("Bus errors", stats.bus_error_count),
  ];
}

class SystemStatus extends Component {
  constructor(props, context) {
    super(props, context);
    this.sendReboot = this.sendReboot.bind(this);
    this.sendReloadStandFirmware = this.sendReloadStandFirmware.bind(this);
    this.sendReloadSpeakerFirmware = this.sendReloadSpeakerFirmware.bind(this);
  }

  sendReboot() {
    this.props.sendJsonMessage({ internal: { action: "reboot" } });
  }

  sendReloadStandFirmware() {
    this.props.sendJsonMessage({ device: { stand: { action: "reload" } } });
  }

  sendReloadSpeakerFirmware() {
    this.props.sendJsonMessage({ device: { speaker: { action: "reload" } } });
  }

  static getPageName() {
    return "SystemStatus";
  }

  static getPageIcon() {
    return (
      <WysiwygIcon />
    )
  }

  render() {
    return (
        <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Button
              variant="contained"
              style="margin:5px"
              onClick={() => {
                this.sendReboot();
              }}
            >
              Reboot
            </Button>
            <Button
              variant="contained"
              style="margin:5px"
              onClick={() => {
                this.sendReloadStandFirmware();
              }}
            >
              Reload Stand Firmware
            </Button>
            <Button
              variant="contained"
              style="margin:5px"
              onClick={() => {
                this.sendReloadSpeakerFirmware();
              }}
            >
              Reload Speaker Firmware
            </Button>
          </Paper>
          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "column",
            }}
          >
            <InfoTable
              name="Firmware"
              valuecolumn="Value"
              rows={[
                createData(
                  "Uptime",
                  this.props.state.internal.uptime.toFixed(1) + " s"
                ),
                createData(
                  "Compile Date",
                  this.props.state.internal.app.date
                ),
                createData(
                  "Compile Time",
                  this.props.state.internal.app.time
                ),
                createData("Version", this.props.state.internal.app.version),
                createData("SHA256", "0x" + Buffer.from(this.props.state.internal.app.sha256, 'base64').toString('hex')),
              ]}
            />
          </Paper>

          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "column",
              height: 600,
            }}
          >
            <InfoTable
              name="Device Stats"
              valuecolumn="Value"
              rows={getDeviceStatRows(
                "Stand",
                this.props.state.device.stand.stats
              )
                .concat(
                  getDeviceBootStatusRows(
                    "Stand",
                    this.props.state.device.stand.bootstatus
                  )
                )
                .concat(
                  getDeviceStatRows(
                    "Speaker",
                    this.props.state.device.speaker.stats
                  )
                )
                .concat(
                  getDeviceBootStatusRows(
                    "Speaker",
                    this.props.state.device.speaker.bootstatus
                  )
                )}
            />
          </Paper>
          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "column",
              height: 600,
            }}
          >
            <InfoTable
              name="CAN Stats"
              valuecolumn="Value"
              rows={getCanRows(this.props.state.internal.stats.can)}
            />
          </Paper>

          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "column",
              height: 600,
            }}
          >
            <InfoTable
              name="Task Stats"
              valuecolumn="Value"
              rows={getTaskRows(this.props.state.internal.stats.tasks)}
            />
          </Paper>
          <Paper
            sx={{
              p: 2,
              display: "flex",
              flexDirection: "column",
              height: 600,
            }}
          >
            <InfoTable
              name="Memory Stats"
              valuecolumn="Value"
              rows={getMemoryRows(this.props.state.internal.stats.memory)}
            />
          </Paper>
        </Container>
    );
  }
}

export default SystemStatus;
