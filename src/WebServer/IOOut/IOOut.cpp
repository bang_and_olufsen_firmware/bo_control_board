#include "IOOut.hpp"

#include "ArduinoJson.h"
#include "WebServer/WebServerUtils.hpp"
#include "tasks/misc_task/misc_task.hpp"

namespace IOOut
{

void addIOOut(JsonVariant& json)
{
  auto&       controller = misc_task_get_gpio_controller();
  JsonVariant gpio       = WebServerUtils::getOrCreateObject(json, "io_out");

  gpio["off_delay"] = controller.getOffDelay();
  gpio["on_delay"]  = controller.getOnDelay();
}

void handleMessage(const JsonVariant& json)
{
  if(json.containsKey("set_state") && json["set_state"].is<bool>())
  {
    misc_task_get_gpio_controller().setState(
      json["set_state"].as<bool>() ? GPIOController::GPIOControllerState::ON : GPIOController::GPIOControllerState::OFF);
  }
  if(json.containsKey("off_delay") && json["off_delay"].is<uint32_t>())
  {
    misc_task_get_gpio_controller().setOffDelay(json["off_delay"].as<uint32_t>());
  }
  if(json.containsKey("on_delay") && json["on_delay"].is<uint32_t>())
  {
    misc_task_get_gpio_controller().setOnDelay(json["on_delay"].as<uint32_t>());
  }
}
}  // namespace IOOut