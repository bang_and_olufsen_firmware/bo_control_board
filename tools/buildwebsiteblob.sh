#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

STATE_DIR=`mktemp -d`
BIN_DIR=`mktemp -d`
TARGET_DIR=${SCRIPT_DIR}/../src/website_blob

mkdir -p ${TARGET_DIR}

python3 ${SCRIPT_DIR}/../components/frogfs/tools/preprocess.py frontend/build ${STATE_DIR} --config ${SCRIPT_DIR}/../frogfs.yml
python3 ${SCRIPT_DIR}/../components/frogfs/tools/mkfrogfs.py ${STATE_DIR} ${BIN_DIR}/frogfs_website_blob.bin --config ${SCRIPT_DIR}/../frogfs.yml
python3 ${SCRIPT_DIR}/../components/frogfs/tools/bin2c.py ${BIN_DIR}/frogfs_website_blob.bin ${TARGET_DIR}/frogfs_website_blob.c