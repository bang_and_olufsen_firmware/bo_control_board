#include "Internal.hpp"

#include "Update.h"
#include "WebServer/WebServerUtils.hpp"
#include "common/Filters/EWMA.hpp"
#include "driver/twai.h"
#include "esp_heap_caps.h"
#include "esp_ota_ops.h"
#include "mbedtls/base64.h"
#include "tasks/misc_task/misc_task.hpp"
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <vector>
struct TaskStat {
  TaskStat() : filteredLoad(0.05) {}
  uint32_t               lastRuntime     = 0;
  uint32_t               lastRuntimeDiff = 0;
  UBaseType_t            priority        = 0;
  configSTACK_DEPTH_TYPE stackLeft       = 0;
  BaseType_t             coreID          = 0;
  EWMA                   filteredLoad;
  bool                   active = true;
};

struct TaskStats {
  TaskStats() : totalRuntime(0), lastRuntime(0) {}
  uint64_t                                  totalRuntime = 0;
  uint32_t                                  lastRuntime  = 0;
  std::unordered_map<std::string, TaskStat> tasks;
};

static TaskStats task_stats;

namespace Internal
{
static std::string twai_state_str(twai_state_t state)
{
  switch(state)
  {
    case TWAI_STATE_STOPPED:
      return "STOPPED";
    case TWAI_STATE_RUNNING:
      return "RUNNING";
    case TWAI_STATE_BUS_OFF:
      return "BUS_OFF";
    case TWAI_STATE_RECOVERING:
      return "RECOVERING";
    default:
      return "UNKNOWN";
  }
}

static void addTaskStats(JsonVariant& json)
{
  auto     taskCount = uxTaskGetNumberOfTasks();
  auto     tasks     = std::vector<TaskStatus_t>(taskCount);
  uint32_t runtime   = 0;
  taskCount          = uxTaskGetSystemState(tasks.data(), taskCount, &runtime);
  tasks.resize(taskCount);

  for(auto& task : task_stats.tasks)
  {
    task.second.active = false;
  }

  for(auto& task : tasks)
  {
    std::string taskName = task.pcTaskName;
    if(taskName == "IDLE")
    {
      taskName += " " + std::to_string(task.xCoreID);
    }

    auto taskStat             = &task_stats.tasks[taskName];
    taskStat->lastRuntimeDiff = (task.ulRunTimeCounter - taskStat->lastRuntime);
    taskStat->lastRuntime     = task.ulRunTimeCounter;
    taskStat->priority        = task.uxBasePriority;
    taskStat->stackLeft       = task.usStackHighWaterMark;
    taskStat->coreID          = task.xCoreID;
    taskStat->active          = true;
  }

  uint32_t runtimeDiff = (runtime - task_stats.lastRuntime);

  if(runtimeDiff > 0)
  {
    for(auto it = task_stats.tasks.begin(); it != task_stats.tasks.end();)
    {
      auto& task = *it;
      if(!task.second.active)
      {
        it = task_stats.tasks.erase(it);
        continue;
      }
      if(task.second.filteredLoad.output > 1000 || !isfinite(task.second.filteredLoad.output))
      {
        task.second.filteredLoad.output = 0;
      }
      json[task.first]["load"]  = task.second.filteredLoad.update(task.second.lastRuntimeDiff / (runtimeDiff * 0.01));
      json[task.first]["stack"] = task.second.stackLeft;
      json[task.first]["core"]  = task.second.coreID;
      it++;
    }
  }
  task_stats.lastRuntime = runtime;
}

static void addAppInfo(JsonVariant& json)
{
  const auto desc          = esp_ota_get_app_description();
  json["time"]             = desc->time;
  json["date"]             = desc->date;
  json["version"]          = desc->version;
  unsigned char shabuf[48] = {0};

  size_t sha_enc_size = 0;
  mbedtls_base64_encode(shabuf, sizeof(shabuf), &sha_enc_size, desc->app_elf_sha256, sizeof(desc->app_elf_sha256));
  json["sha256"] = shabuf;
}

static void addLedInfo(JsonVariant& json)
{
  const auto led         = misc_task_get_led();
  json["max_brightness"] = led.getMaxBrightness() * 100;
  json["min_brightness"] = led.getMinBrightness() * 100;
  json["led_fade_time"]  = led.getFadeTime();
}

static void addHeapStat(JsonVariant& json, const char* identifier, uint32_t caps)
{
  multi_heap_info_t heap_info;

  heap_caps_get_info(&heap_info, caps);

  json[identifier]["total_free_bytes"]      = heap_info.total_free_bytes;
  json[identifier]["total_allocated_bytes"] = heap_info.total_allocated_bytes;
  json[identifier]["largest_free_block"]    = heap_info.largest_free_block;
  json[identifier]["minimum_free_bytes"]    = heap_info.minimum_free_bytes;
  json[identifier]["allocated_blocks"]      = heap_info.allocated_blocks;
  json[identifier]["free_blocks"]           = heap_info.free_blocks;
  json[identifier]["total_blocks"]          = heap_info.total_blocks;
}

static void addCanStats(JsonVariant& json)
{
  twai_status_info_t twai_info;

  twai_get_status_info(&twai_info);

  json["state"]            = twai_state_str(twai_info.state);
  json["msgs_to_tx"]       = twai_info.msgs_to_tx;
  json["msgs_to_rx"]       = twai_info.msgs_to_rx;
  json["tx_error_counter"] = twai_info.tx_error_counter;
  json["rx_error_counter"] = twai_info.rx_error_counter;
  json["tx_failed_count"]  = twai_info.tx_failed_count;
  json["rx_missed_count"]  = twai_info.rx_missed_count;
  json["rx_overrun_count"] = twai_info.rx_overrun_count;
  json["arb_lost_count"]   = twai_info.arb_lost_count;
  json["bus_error_count"]  = twai_info.bus_error_count;
}

static void addMemoryStats(JsonVariant& json)
{
  addHeapStat(json, "INTERNAL", MALLOC_CAP_INTERNAL);
  addHeapStat(json, "SPIRAM", MALLOC_CAP_SPIRAM);
}

static void addTVLedInfo(JsonVariant& json)
{
  const auto tv_led            = misc_task_get_tv_led();
  json["max_brightness_red"]   = tv_led.getRedConst().getMaxBrightness() * 100;
  json["min_brightness_red"]   = tv_led.getRedConst().getMinBrightness() * 100;
  json["min_brightness_green"] = tv_led.getGreenConst().getMinBrightness() * 100;
  json["max_brightness_green"] = tv_led.getGreenConst().getMaxBrightness() * 100;
  json["orange_compensation"]  = tv_led.getOrangeCompensation() * 100;
  json["transition_time"]      = tv_led.getTransitionTime();
}

void addStats(JsonVariant& json)
{
  JsonVariant internal = WebServerUtils::getOrCreateObject(json, "internal");
  JsonVariant stats    = WebServerUtils::getOrCreateObject(internal, "stats");

  JsonVariant tasks = WebServerUtils::getOrCreateObject(stats, "tasks");
  addTaskStats(tasks);
  JsonVariant memory = WebServerUtils::getOrCreateObject(stats, "memory");
  addMemoryStats(memory);
  JsonVariant can = WebServerUtils::getOrCreateObject(stats, "can");
  addCanStats(can);
  JsonVariant app = WebServerUtils::getOrCreateObject(internal, "app");
  addAppInfo(app);

  JsonVariant led = WebServerUtils::getOrCreateObject(internal, "led");
  addLedInfo(led);

  JsonVariant tv_led = WebServerUtils::getOrCreateObject(internal, "tv_led");
  addTVLedInfo(tv_led);

  internal["uptime"] = esp_timer_get_time() / 1000000.f;
}

void handleTVLEDMessage(const JsonVariant& json)
{
  if(json.containsKey("max_brightness_red") && json["max_brightness_red"].is<float>())
  {
    misc_task_get_tv_led().getRed().setMaxBrightness(json["max_brightness_red"].as<float>() / 100);
  }
  if(json.containsKey("min_brightness_red") && json["min_brightness_red"].is<float>())
  {
    misc_task_get_tv_led().getRed().setMinBrightness(json["min_brightness_red"].as<float>() / 100);
  }
  if(json.containsKey("max_brightness_green") && json["max_brightness_green"].is<float>())
  {
    misc_task_get_tv_led().getGreen().setMaxBrightness(json["max_brightness_green"].as<float>() / 100);
  }
  if(json.containsKey("min_brightness_green") && json["min_brightness_green"].is<float>())
  {
    misc_task_get_tv_led().getGreen().setMinBrightness(json["min_brightness_green"].as<float>() / 100);
  }
  if(json.containsKey("orange_compensation") && json["orange_compensation"].is<float>())
  {
    misc_task_get_tv_led().setOrangeCompensation(json["orange_compensation"].as<float>() / 100);
  }
  if(json.containsKey("transition_time") && json["transition_time"].is<float>())
  {
    misc_task_get_tv_led().setTransitionTime(json["transition_time"].as<float>());
  }
  if(json.containsKey("action"))
  {
    if(json["action"].as<const char*>() == std::string("off"))
    {
      misc_task_get_tv_led().gotoOff();
    }
    if(json["action"].as<const char*>() == std::string("on"))
    {
      misc_task_get_tv_led().gotoOn();
    }
  }
}

void handleMessage(const JsonVariant& json)
{
  if(json.containsKey("action") && json["action"].as<const char*>() == std::string("reboot"))
  {
    esp_restart();
  }
  if(json.containsKey("max_brightness") && json["max_brightness"].is<float>())
  {
    misc_task_get_led().setMaxBrightness(json["max_brightness"].as<float>() / 100);
  }
  if(json.containsKey("min_brightness") && json["min_brightness"].is<float>())
  {
    misc_task_get_led().setMinBrightness(json["min_brightness"].as<float>() / 100);
  }
  if(json.containsKey("led_fade_time") && json["led_fade_time"].is<float>())
  {
    misc_task_get_led().setFadeTime(json["led_fade_time"].as<float>());
  }
}

}  // namespace Internal