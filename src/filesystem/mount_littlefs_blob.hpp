#pragma once
#include "esp_err.h"

esp_err_t mount_littlefs_blob(const void* start, const void* end, const char* base_path);