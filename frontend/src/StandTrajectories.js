import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import StarIcon from "@mui/icons-material/Star";
import ZoomInMapIcon from "@mui/icons-material/ZoomInMap";
import ZoomOutMapIcon from "@mui/icons-material/ZoomOutMap";
import { Button, TextField } from "@mui/material";
import RadarIcon from "@mui/icons-material/Radar";
import AddIcon from "@mui/icons-material/Add";
import ControlCameraIcon from "@mui/icons-material/ControlCamera";
import styled from "@emotion/styled";
import DeleteIcon from "@mui/icons-material/Delete";
import InputAdornment from "@mui/material/InputAdornment";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import FormatIndentDecreaseIcon from "@mui/icons-material/FormatIndentDecrease";
import FormatIndentIncreaseIcon from "@mui/icons-material/FormatIndentIncrease";

const TableRowStyled = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(4n+3)": {
    backgroundColor: theme.palette.action.hover,
  },
  "&:nth-of-type(4n+4)": {
    backgroundColor: theme.palette.action.hover,
  },

  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

const TableRowStyledAlternating = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(2n+1)": {
    backgroundColor: theme.palette.action.hover,
  },

  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

function createData(name, trajectory, default_name) {
  let data = {
    name: name,
    unmodified_name: name,
    waypoints: trajectory.waypoints,
    time: trajectory.time,
  };
  if (default_name == name) {
    data["name"] += " [D]";
  }
  return data;
}

class Waypoints extends Component {
  constructor(props, context) {
    super();

    this.addWaypoint = this.addWaypoint.bind(this);
    this.deleteWaypoint = this.deleteWaypoint.bind(this);
    this.updateTrajectory = this.updateTrajectory.bind(this);
    this.moveWaypointDown = this.moveWaypointDown.bind(this);
    this.reassignWaypoint = this.reassignWaypoint.bind(this);
    this.moveWaypointUp = this.moveWaypointUp.bind(this);
    this.increaseWaypoint = this.increaseWaypoint.bind(this);
    this.decreaseWaypoint = this.decreaseWaypoint.bind(this);
  }

  addWaypoint() {
    let waypoints = this.props.data.waypoints.map((x) => x);
    waypoints.push({
      m1: this.props.position[0].encoder,
      m2: this.props.position[1].encoder,
    });
    this.updateTrajectory(waypoints);
  }

  reassignWaypoint(index) {
    let waypoints = this.props.data.waypoints.map((x) => x);
    waypoints[index] = {
      m1: this.props.position[0].encoder,
      m2: this.props.position[1].encoder,
    };
    this.updateTrajectory(waypoints);
  }

  decreaseWaypoint(index) {
    let waypoints = this.props.data.waypoints.map((x) => x);
    waypoints[index] = {
      m1: waypoints[0].m1 - 0,
      m2: waypoints[0].m2 - 5,
    };
    this.updateTrajectory(waypoints);
  }

  increaseWaypoint(index) {
    let waypoints = this.props.data.waypoints.map((x) => x);
    waypoints[index] = {
      m1: waypoints[0].m1 + 0,
      m2: waypoints[0].m2 + 5,
    };
    this.updateTrajectory(waypoints);
  }

  deleteWaypoint(index) {
    let waypoints = this.props.data.waypoints.map((x) => x);
    waypoints.splice(index, 1);
    this.updateTrajectory(waypoints);
  }

  moveWaypointUp(index) {
    let waypoints = this.props.data.waypoints.map((x) => x);
    if (index == 0 || index >= waypoints.length) {
      return;
    }
    let tmp = waypoints[index - 1];
    waypoints[index - 1] = waypoints[index];
    waypoints[index] = tmp;
    this.updateTrajectory(waypoints);
  }

  moveWaypointDown(index) {
    let waypoints = this.props.data.waypoints.map((x) => x);
    if (index + 1 >= waypoints.length) {
      return;
    }
    let tmp = waypoints[index + 1];
    waypoints[index + 1] = waypoints[index];
    waypoints[index] = tmp;
    this.updateTrajectory(waypoints);
  }

  updateTrajectory(waypoints) {
    this.props.sendJsonMessage({
      device: {
        stand: {
          create_trajectory: {
            name: this.props.data.unmodified_name,
            time: this.props.data.time,
            waypoints: waypoints,
          },
        },
      },
    });
  }
  moveToWaypoint(waypoint) {
    this.props.sendJsonMessage({
      device: {
        stand: {
          move_to_waypoint: waypoint,
        },
      },
    });
  }

  render() {
    const waypoints = this.props.data.waypoints;

    return (
      <TableContainer component={Paper}>
        <Table
          sx={{ minWidth: 200 }}
          size="small"
          aria-label="Trajectory table"
        >
          <TableHead>
            <TableRowStyledAlternating>
              <TableCell>M1</TableCell>
              <TableCell>M2</TableCell>
              <TableCell>Move</TableCell>
              <TableCell>Set</TableCell>
              <TableCell>Up</TableCell>
              <TableCell>Down</TableCell>
              <TableCell>Delete</TableCell>
            </TableRowStyledAlternating>
          </TableHead>
          <TableBody>
            {waypoints.map((waypoint, index) => (
              <TableRowStyledAlternating
                key={this.props.data.name}
                _start
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell style={{ borderBottom: "none" }}>
                  {waypoint.m1.toFixed(1)}º
                </TableCell>
                <TableCell style={{ borderBottom: "none" }}>
                  {waypoint.m2.toFixed(1)}º
                </TableCell>
                <TableCell style={{ borderBottom: "none" }}>
                  <Button
                    variant="contained"
                    style="margin:5px"
                    onClick={() => {
                      this.moveToWaypoint(waypoint);
                    }}
                  >
                    <ControlCameraIcon />
                  </Button>
                </TableCell>

                <TableCell style={{ borderBottom: "none" }}>
                  <Button
                    variant="contained"
                    style="margin:5px"
                    onClick={() => {
                      this.reassignWaypoint(index);
                    }}
                  >
                    <RadarIcon />
                  </Button>
                </TableCell>
                {/* <TableCell style={{ borderBottom: "none" }}>
                  <Button
                    variant="contained"
                    style="margin:5px"
                    onClick={() => {
                      this.increaseWaypoint(index);
                    }}
                  >
                    <FormatIndentIncreaseIcon />
                  </Button>
                </TableCell>
                <TableCell style={{ borderBottom: "none" }}>
                  <Button
                    variant="contained"
                    style="margin:5px"
                    onClick={() => {
                      this.decreaseWaypoint(index);
                    }}
                  >
                    <FormatIndentIncreaseIcon />
                  </Button>
                </TableCell> */}

                <TableCell style={{ borderBottom: "none" }}>
                  {index != 0 && (
                    <Button
                      variant="contained"
                      style="margin:5px"
                      onClick={() => {
                        this.moveWaypointUp(index);
                      }}
                    >
                      <KeyboardArrowUpIcon />
                    </Button>
                  )}
                </TableCell>
                <TableCell style={{ borderBottom: "none" }}>
                  {index + 1 != waypoints.length && (
                    <Button
                      variant="contained"
                      style="margin:5px"
                      onClick={() => {
                        this.moveWaypointDown(index);
                      }}
                    >
                      <KeyboardArrowDownIcon />
                    </Button>
                  )}
                </TableCell>

                <TableCell style={{ borderBottom: "none" }}>
                  <Button
                    variant="contained"
                    style="margin:5px"
                    onClick={() => {
                      this.deleteWaypoint(index);
                    }}
                  >
                    <DeleteIcon />
                  </Button>
                </TableCell>
              </TableRowStyledAlternating>
            ))}
            <TableRowStyledAlternating>
              <TableCell colspan={7} align="center">
                <Button
                  variant="contained"
                  style="margin:5px"
                  onClick={() => {
                    this.addWaypoint();
                  }}
                >
                  <AddIcon />
                </Button>
              </TableCell>
            </TableRowStyledAlternating>
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

class TrajectoryRow extends Component {
  constructor(props, context) {
    super();

    this.sendMoveToStart = this.sendMoveToStart.bind(this);
    this.sendMoveToEnd = this.sendMoveToEnd.bind(this);
    this.sendSetDefault = this.sendSetDefault.bind(this);
    this.sendDelete = this.sendDelete.bind(this);
    this.setOpen = this.setOpen.bind(this);
    this.setState({ open: false });
  }

  setOpen(val) {
    this.setState({ open: val });
  }

  sendDelete(trajectoryName) {
    this.props.sendJsonMessage({
      device: { stand: { delete_trajectory: trajectoryName } },
    });
  }

  sendMoveToStart(trajectoryName) {
    this.props.sendJsonMessage({
      device: { stand: { move_to_trajectory_start: trajectoryName } },
    });
  }

  sendMoveToEnd(trajectoryName) {
    this.props.sendJsonMessage({
      device: { stand: { move_to_trajectory_end: trajectoryName } },
    });
  }

  sendSetDefault(trajectoryName) {
    this.props.sendJsonMessage({
      device: { stand: { set_default_trajectory: trajectoryName } },
    });
  }

  render() {
    return (
      <React.Fragment>
        <TableRowStyled
          key={this.props.data.name}
          _start
          sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
        >
          <TableCell style={{ borderBottom: "none" }}>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => this.setOpen(!this.state.open)}
            >
              {this.state.open ? (
                <KeyboardArrowUpIcon />
              ) : (
                <KeyboardArrowDownIcon />
              )}
            </IconButton>
          </TableCell>

          <TableCell
            component="th"
            scope="row"
            style={{ borderBottom: "none" }}
          >
            {this.props.data.name}
            <br />
            <b>Time: </b> {this.props.data.time.toFixed(1) + " s"}
            <br />
            <b>Waypoints: </b>
            {this.props.data.waypoints.length}
          </TableCell>
          <TableCell align="left" style={{ borderBottom: "none" }}>
            <Button
              variant="contained"
              style="margin:5px"
              onClick={() => {
                this.sendSetDefault(this.props.data.unmodified_name);
              }}
            >
              <StarIcon />
            </Button>
          </TableCell>
          <TableCell align="left" style={{ borderBottom: "none" }}>
            <Button
              variant="contained"
              style="margin:5px"
              onClick={() => {
                this.sendMoveToStart(this.props.data.unmodified_name);
              }}
            >
              <ZoomInMapIcon />
            </Button>
          </TableCell>
          <TableCell align="left" style={{ borderBottom: "none" }}>
            <Button
              variant="contained"
              style="margin:5px"
              onClick={() => {
                this.sendMoveToEnd(this.props.data.unmodified_name);
              }}
            >
              <ZoomOutMapIcon />
            </Button>
          </TableCell>
          <TableCell align="left" style={{ borderBottom: "none" }}>
            {this.props.data.unmodified_name == this.props.data.name && (
              <Button
                variant="contained"
                style="margin:5px"
                onClick={() => {
                  this.sendDelete(this.props.data.unmodified_name);
                }}
              >
                <DeleteIcon />
              </Button>
            )}
          </TableCell>
        </TableRowStyled>
        <TableRowStyled
          key={this.props.data.name}
          _end
          sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
        >
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={this.state.open} timeout="auto" unmountOnExit>
              <Waypoints
                data={this.props.data}
                position={this.props.position}
                sendJsonMessage={this.props.sendJsonMessage}
              />
            </Collapse>
          </TableCell>
        </TableRowStyled>
      </React.Fragment>
    );
  }
}

class NewTrajectoryRow extends Component {
  constructor(props, context) {
    super();

    this.createTrajectory = this.createTrajectory.bind(this);

    this.state = {
      name: "New Trajectory",
      time: 20,
      base: "",
    };
  }

  createTrajectory() {
    let waypoints = [
      {
        m1: this.props.position[0].encoder,
        m2: this.props.position[1].encoder,
      },
    ];
    if (this.state.base in this.props.trajectories) {
      waypoints = this.props.trajectories[this.state.base].waypoints;
    }
    if (this.state.name in this.props.trajectories) {
      waypoints = this.props.trajectories[this.state.name].waypoints;
    }
    this.props.sendJsonMessage({
      device: {
        stand: {
          create_trajectory: {
            name: this.state.name,
            time: parseFloat(this.state.time),
            waypoints: waypoints,
          },
        },
      },
    });
  }

  render() {
    let trajectory_names = [];
    for (const [name, values] of Object.entries(this.props.trajectories)) {
      trajectory_names = trajectory_names.concat([name]);
    }

    return (
      <React.Fragment>
        <TableRowStyled
          key="new_trajectoryField_1"
          sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
        >
          <TableCell
            colspan={2}
            component="th"
            scope="row"
            style={{ borderBottom: "none" }}
          >
            <TextField
              id="outlined-basic"
              value={this.state.name}
              label="New Trajectory"
              variant="outlined"
              onChange={(event) => {
                this.setState({ name: event.target.value });
              }}
            />
          </TableCell>
          <TableCell align="left" colspan={1}>
            <TextField
              value={this.state.time}
              step={0.1}
              type="number"
              id="outlined-basic"
              label="Move Time"
              variant="outlined"
              InputProps={{
                endAdornment: <InputAdornment position="end">s</InputAdornment>,
              }}
              onChange={(event) => {
                this.setState({ time: event.target.value });
              }}
            />
          </TableCell>
          <TableCell align="left" colspan={2}>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
              <InputLabel id="select-base-label">Copy From</InputLabel>
              <Select
                labelId="select-base-label"
                id="select-base"
                value={this.state.base}
                label="Copy from"
                onChange={(event) => {
                  this.setState({ base: event.target.value });
                }}
              >
                {trajectory_names.map((name) => (
                  <MenuItem value={name}>{name}</MenuItem>
                ))}

                <MenuItem value="">None</MenuItem>
              </Select>
            </FormControl>
          </TableCell>
          <TableCell align="left">
            <Button
              variant="contained"
              style="margin:5px"
              onClick={() => {
                this.createTrajectory();
              }}
            >
              <AddIcon />
            </Button>
          </TableCell>
        </TableRowStyled>
      </React.Fragment>
    );
  }
}

class StandTrajectories extends Component {
  constructor(props, context) {
    super();
    this.state = {
      debug: false,
    };
  }

  render() {
    let rows = [];
    for (const [name, values] of Object.entries(
      this.props.state.trajectory_list
    )) {
      rows = rows.concat([createData(name, values, this.props.state.default)]);
    }

    rows.sort(function (a, b) {
      a["name"].localeCompare(b["name"]);
    });

    return (
      <TableContainer component={Paper}>
        <Table
          sx={{ minWidth: 200 }}
          size="small"
          aria-label="Trajectory table"
        >
          <TableHead>
            <TableRowStyled>
              <TableCell></TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Set Default</TableCell>
              <TableCell>Move To Start</TableCell>
              <TableCell>Move To End</TableCell>
              <TableCell>Delete</TableCell>
            </TableRowStyled>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TrajectoryRow
                data={row}
                position={this.props.position}
                sendJsonMessage={this.props.sendJsonMessage}
              />
            ))}
            <NewTrajectoryRow
              trajectories={this.props.state.trajectory_list}
              position={this.props.position}
              sendJsonMessage={this.props.sendJsonMessage}
            />
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

export default StandTrajectories;
