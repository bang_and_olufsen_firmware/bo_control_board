import React, { Component } from "react";
import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { TextField, Button } from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";
import TungstenIcon from "@mui/icons-material/Tungsten";
import LightModeIcon from "@mui/icons-material/LightMode";
import FiberManualRecordIcon from "@mui/icons-material/FiberManualRecord";
class OnboardLEDOptions extends Component {
  constructor(props, context) {
    super();
    this.getMinBrightness = this.getMinBrightness.bind(this);
    this.getMaxBrightness = this.getMaxBrightness.bind(this);
    this.getFadeTime = this.getFadeTime.bind(this);

    this.state = {
      min_brightness_set_time: new Date(),
      max_brightness_set_time: new Date(),
      led_fade_time_set_time: new Date(),
    };
  }

  getMinBrightness() {
    if (
      Date.now() - this.state["min_brightness_set_time"] < 2000 &&
      "min_brightness" in this.state
    ) {
      return this.state.min_brightness;
    }
    return this.props.state.min_brightness;
  }

  getMaxBrightness() {
    if (
      Date.now() - this.state["max_brightness_set_time"] < 2000 &&
      "max_brightness" in this.state
    ) {
      return this.state.max_brightness;
    }
    return this.props.state.max_brightness;
  }

  getFadeTime() {
    if (
      Date.now() - this.state["led_fade_time_set_time"] < 2000 &&
      "led_fade_time" in this.state
    ) {
      return this.state.led_fade_time;
    }
    return this.props.state.led_fade_time;
  }

  render() {
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 200 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={{ borderBottom: "none" }}>
                Onboard LED
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow
              key="max_brightness"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getMaxBrightness()}
                  step={0.1}
                  type="number"
                  min={0}
                  max={100}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Max Brightness"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      max_brightness: event.target.value,
                      max_brightness_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      internal: {
                        max_brightness: parseFloat(event.target.value),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="min_brightness"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getMinBrightness()}
                  step={0.1}
                  type="number"
                  min={0}
                  max={100}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Min Brightness"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      min_brightness: event.target.value,
                      min_brightness_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      internal: {
                        min_brightness: parseFloat(event.target.value),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="led_fade_time"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getFadeTime()}
                  step={0.1}
                  min={5}
                  max={30000}
                  type="number"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">ms</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Fade Time"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      led_fade_time: event.target.value,
                      led_fade_time_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      internal: {
                        led_fade_time: parseFloat(event.target.value),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

class TVLEDOptions extends Component {
  constructor(props, context) {
    super();
    this.getMinBrightnessRed = this.getMinBrightnessRed.bind(this);
    this.getMaxBrightnessRed = this.getMaxBrightnessRed.bind(this);
    this.getMinBrightnessGreen = this.getMinBrightnessGreen.bind(this);

    this.getTransitionTime = this.getTransitionTime.bind(this);
    this.getOrangeCompensation = this.getOrangeCompensation.bind(this);
    this.sendTurnOnTVLED = this.sendTurnOnTVLED.bind(this);
    this.sendTurnOffTVLED = this.sendTurnOffTVLED.bind(this);

    this.state = {
      min_brightness_red_set_time: new Date(),
      max_brightness_red_set_time: new Date(),
      min_brightness_green_set_time: new Date(),
      max_brightness_green_set_time: new Date(),
      transition_time_set_time: new Date(),
      orange_compensation_set_time: new Date(),
    };
  }

  sendTurnOffTVLED() {
    this.props.sendJsonMessage({
      tv_led: { action: "off" },
    });
  }

  sendTurnOnTVLED() {
    this.props.sendJsonMessage({
      tv_led: { action: "on" },
    });
  }

  getMinBrightnessRed() {
    if (
      Date.now() - this.state["min_brightness_red_set_time"] < 2000 &&
      "min_brightness_red" in this.state
    ) {
      return this.state.min_brightness_red;
    }
    return this.props.state.min_brightness_red;
  }

  getMinBrightnessGreen() {
    if (
      Date.now() - this.state["min_brightness_green_set_time"] < 2000 &&
      "min_brightness_green" in this.state
    ) {
      return this.state.min_brightness_green;
    }
    return this.props.state.min_brightness_green;
  }

  getMaxBrightnessRed() {
    if (
      Date.now() - this.state["max_brightness_red_set_time"] < 2000 &&
      "max_brightness_red" in this.state
    ) {
      return this.state.max_brightness_red;
    }
    return this.props.state.max_brightness_red;
  }

  getMaxBrightnessGreen() {
    if (
      Date.now() - this.state["max_brightness_green_set_time"] < 2000 &&
      "max_brightness_green" in this.state
    ) {
      return this.state.max_brightness_green;
    }
    return this.props.state.max_brightness_green;
  }

  getTransitionTime() {
    if (
      Date.now() - this.state["transition_time_set_time"] < 2000 &&
      "transition_time" in this.state
    ) {
      return this.state.transition_time;
    }
    return this.props.state.transition_time;
  }

  getOrangeCompensation() {
    if (
      Date.now() - this.state["orange_compensation_set_time"] < 2000 &&
      "orange_compensation" in this.state
    ) {
      return this.state.orange_compensation;
    }
    return this.props.state.orange_compensation;
  }

  render() {
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 200 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={{ borderBottom: "none" }}>TV LED</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow
              key="tvled_on"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <Button
                  variant="contained"
                  style="margin:5px"
                  onClick={() => {
                    this.sendTurnOnTVLED();
                  }}
                >
                  <LightModeIcon />
                </Button>
                <Button
                  variant="contained"
                  style="margin:5px"
                  onClick={() => {
                    this.sendTurnOffTVLED();
                  }}
                >
                  <FiberManualRecordIcon />
                </Button>
              </TableCell>
            </TableRow>

            <TableRow
              key="max_brightness_red"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getMaxBrightnessRed()}
                  step={0.1}
                  type="number"
                  min={0}
                  max={100}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Max Brightness Red"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      max_brightness_red: event.target.value,
                      max_brightness_red_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      tv_led: {
                        max_brightness_red: parseFloat(event.target.value),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="min_brightness_red"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getMinBrightnessRed()}
                  step={0.1}
                  type="number"
                  min={0}
                  max={100}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Min Brightness Red"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      min_brightness_red: event.target.value,
                      min_brightness_red_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      tv_led: {
                        min_brightness_red: parseFloat(event.target.value),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="max_brightness_green"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getMaxBrightnessGreen()}
                  step={0.1}
                  type="number"
                  min={0}
                  max={100}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Max Brightness Green"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      max_brightness_green: event.target.value,
                      max_brightness_green_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      tv_led: {
                        max_brightness_green: parseFloat(event.target.value),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="min_brightness_green"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getMinBrightnessGreen()}
                  step={0.1}
                  type="number"
                  min={0}
                  max={100}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Min Brightness Green"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      min_brightness_green: event.target.value,
                      min_brightness_green_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      tv_led: {
                        min_brightness_green: parseFloat(event.target.value),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="transition_time"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getTransitionTime()}
                  step={1}
                  type="number"
                  min={100}
                  max={30000}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">ms</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Transition Time"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      transition_time: event.target.value,
                      transition_time_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      tv_led: {
                        transition_time: parseFloat(event.target.value),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="orange_compensation"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getOrangeCompensation()}
                  step={1}
                  type="number"
                  min={0}
                  max={100}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">%</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Orange Compensation"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      orange_compensation: event.target.value,
                      orange_compensation_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      tv_led: {
                        orange_compensation: parseFloat(event.target.value),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

class LEDSettings extends Component {
  constructor(props, context) {
    super();
  }

  static getPageName() {
    return "LED";
  }

  static getPageIcon() {
    return <TungstenIcon />;
  }

  render() {
    return (
      <Container disableGutters={true} maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <OnboardLEDOptions
          state={this.props.state.led}
          sendJsonMessage={this.props.sendJsonMessage}
        />
        <TVLEDOptions
          state={this.props.state.tv_led}
          sendJsonMessage={this.props.sendJsonMessage}
        />
      </Container>
    );
  }
}

export default LEDSettings;
