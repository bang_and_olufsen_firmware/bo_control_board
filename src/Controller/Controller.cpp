#include "Controller.hpp"

#include <string.h>

#include "common/CANMessages.hpp"
#include "endian.h"
#include "esp_timer.h"

Controller::Controller(
  CANController& bus,
  CANBootloader& bootloader,
  CanMessageType heapMsgType,
  CanMessageType uptimeMsgType,
  CanMessageType loadMsgType)
  : bus(bus),
    bootloader(bootloader)
{
  bus.registerCallback(heapMsgType, [this](const twai_message_t& msg) { this->handleHeap(msg); });
  bus.registerCallback(uptimeMsgType, [this](const twai_message_t& msg) { this->handleUptime(msg); });
  bus.registerCallback(loadMsgType, [this](const twai_message_t& msg) { this->handleLoad(msg); });
}

void Controller::handleHeap(const twai_message_t& msg)
{
  total_heap       = bswap32(*(reinterpret_cast<const uint32_t*>(msg.data + 0)));
  used_heap        = bswap32(*(reinterpret_cast<const uint32_t*>(msg.data + 4)));
  last_status_time = esp_timer_get_time();
}

void Controller::handleLoad(const twai_message_t& msg)
{
  load             = *(reinterpret_cast<const float*>(msg.data + 0));
  last_status_time = esp_timer_get_time();
}

void Controller::handleUptime(const twai_message_t& msg)
{
  uptime           = bswap64(*(reinterpret_cast<const uint64_t*>(msg.data + 0)));
  last_status_time = esp_timer_get_time();
}

int64_t Controller::getTimeSinceLastStatus() const
{
  if(last_status_time == 0)
  {
    return -1;
  }
  return esp_timer_get_time() - last_status_time;
}