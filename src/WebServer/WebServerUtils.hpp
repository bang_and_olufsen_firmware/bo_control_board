#pragma once

#include "ArduinoJson.h"
#include <functional>

namespace WebServerUtils
{
inline JsonVariant getOrCreateObject(JsonVariant& json, const char* key)
{
  JsonVariant object;

  if(json.containsKey(key))
  {
    return json[key];
  }
  else
  {
    return json.createNestedObject(key);
  }
}
}  // namespace WebServerUtils