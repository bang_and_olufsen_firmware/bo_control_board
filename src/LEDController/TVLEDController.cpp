#include "TVLEDController.hpp"

#include <Arduino.h>

#include "utilities/nvs_utilities.hpp"
#include <algorithm>

#define KEYNAME_TRANSITION_TIME     "trans_time"
#define KEYNAME_ORANGE_COMPENSATION "orange_comp"
#define NVS_NAMESPACE               "tv_led"

TVLEDController::TVLEDController(OneShotLEDController& red, OneShotLEDController& green) : red(red), green(green)
{
  vSemaphoreCreateBinary(semaphore);

  setState(TVLEDController::State::OFF);

  esp_err_t err = nvs_open(NVS_NAMESPACE, NVS_READWRITE, &nvs_handle);
  if(err != ESP_OK)
  {
    log_w("Error opening NVS for LED settings: %s", esp_err_to_name(err));
  }

  loadSettings();
}

void TVLEDController::loadSettings()
{
  transition_time_ms  = nvs_utilities::get_float_setting(KEYNAME_TRANSITION_TIME, nvs_handle, 10000);
  orange_compensation = nvs_utilities::get_float_setting(KEYNAME_ORANGE_COMPENSATION, nvs_handle, 0.66);
}

void TVLEDController::setOrangeCompensation(float val)
{
  val = std::clamp(val, 0.f, 1.0f);

  xSemaphoreTake(semaphore, portMAX_DELAY);
  nvs_utilities::set_float_setting(KEYNAME_ORANGE_COMPENSATION, nvs_handle, val);
  orange_compensation = val;
  xSemaphoreGive(semaphore);
}

void TVLEDController::setTransitionTime(float val)
{
  val = std::clamp(val, 5.f, 30000.0f);

  xSemaphoreTake(semaphore, portMAX_DELAY);
  nvs_utilities::set_float_setting(KEYNAME_TRANSITION_TIME, nvs_handle, val);
  transition_time_ms = val;
  xSemaphoreGive(semaphore);
}

void TVLEDController::gotoOn()
{
  xSemaphoreTake(semaphore, portMAX_DELAY);

  setState(TVLEDController::State::POWERING_ON);

  xSemaphoreGive(semaphore);
}

void TVLEDController::gotoOff()
{
  xSemaphoreTake(semaphore, portMAX_DELAY);

  setState(TVLEDController::State::POWERING_OFF);

  xSemaphoreGive(semaphore);
}

void TVLEDController::setLEDState()
{
  switch(led_state)
  {
    case TVLEDController::State::OFF:
      green.setOnPower(1);
      red.setOnPower(1);
      red.forceNewFade(true, false);
      green.forceNewFade(false, false);
      break;
    case TVLEDController::State::POWERING_OFF:
      green.setOnPower(orange_compensation);
      red.setOnPower(1);
      red.forceNewFade(true, false);
      green.forceNewFade(true, false);
      break;
    case TVLEDController::State::POWERING_ON:
      green.setOnPower(1);
      red.setOnPower(1);
      red.forceNewFade(false, false);
      green.forceNewFade(true, false);
      break;
    case TVLEDController::State::ON:
    default:
      green.setOnPower(1);
      red.setOnPower(1);
      red.forceNewFade(false, false);
      green.forceNewFade(false, false);
      break;
  }
}

uint64_t TVLEDController::getTimeSinceStateChange() const
{
  return esp_timer_get_time() - last_state_time;
}

void TVLEDController::setState(TVLEDController::State state)
{
  if(state != led_state)
  {
    last_state_time = esp_timer_get_time();
  }
  led_state = state;
}

void TVLEDController::progressState()
{
  switch(led_state)
  {
    case TVLEDController::State::POWERING_ON:
      if(getTimeSinceStateChange() > transition_time_ms * 1000)
      {
        setState(TVLEDController::State::ON);
      }
      break;
    case TVLEDController::State::POWERING_OFF:
      if(getTimeSinceStateChange() > transition_time_ms * 1000)
      {
        setState(TVLEDController::State::OFF);
      }
      break;

    default:
    case TVLEDController::State::ON:
    case TVLEDController::State::OFF:
      break;
  }
}

void TVLEDController::step()
{
  xSemaphoreTake(semaphore, portMAX_DELAY);

  progressState();
  setLEDState();

  xSemaphoreGive(semaphore);

  red.step();
  green.step();
}