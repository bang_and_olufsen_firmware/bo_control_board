import React, { Component } from "react";
import Stand from "./Stand";
import TopBar from "./TopBar";
import Speakerbar from "./Speakerbar";
import Current from "./Current";
import SystemStatus from "./SystemStatus";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Box from "@mui/material/Box";
import CssBaseline from "@mui/material/CssBaseline";
import getDeviceIP from "./IPUtils";
import useWebSocket, { ReadyState } from "react-use-websocket";
import OTA from "./OTA";
import LEDSettings from "./LEDSettings";
import { inflate } from "pako";
import { Buffer } from "buffer";
import Dashboard from "./DashBoard";
import IOController from "./IOController";
import IOOut from "./IOOut";

const defaultTheme = createTheme();

const socketUrl = "ws://" + getDeviceIP() + "/ws";

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.setPage = this.setPage.bind(this);
    this.getMain = this.getMain.bind(this);
    this.monitorWebsocket = this.monitorWebsocket.bind(this);

    this.pages = [Dashboard, Stand, Speakerbar, IOOut, Current, IOController, SystemStatus, OTA, LEDSettings];
    this.state = {
      selectedPage: this.pages[0].getPageName(),
      shouldWebsocketOpen: true,
      lastMsg: null,
      reopening: new Date(),
      lastMessageTimestamp: new Date(),
    };
  }

  componentDidMount() {
    this.setState({ monitor_id: setInterval(this.monitorWebsocket, 1000) });
  }

  componentWillUnmount() {
    clearInterval(this.state.monitor_id);
  }

  monitorWebsocket() {
    if (!this.state.shouldWebsocketOpen) {
      console.log("Reconnecting websocket after timeout");
      this.setState({
        shouldWebsocketOpen: true,
        reopening: new Date(),
      });
    } else if (
      new Date() - this.state.lastMessageTimestamp > 5000 &&
      new Date() - this.state.reopening > 5000
    ) {
      console.log("Disconnecting websocket on timeout");
      this.setState({ shouldWebsocketOpen: false });
    }
  }

  setPage(pagename) {
    this.setState({ selectedPage: pagename });
  }

  getMain() {
    const { sendJsonMessage } = useWebSocket(
      socketUrl,
      {
        onOpen: () => console.log("WebSocket connection opened."),
        onClose: () => console.log("WebSocket connection closed."),
        onError: (error) => console.log("WebSocket error: "),
        onMessage: (message) => {
          if (message) {
            self = this
            message.data.arrayBuffer().then(function (val) {
              let jsonMessage = JSON.parse(
                Buffer.from(inflate(val)).toString()
              );
              self.setState({ lastMessageTimestamp: new Date() });
              self.setState({ lastMsg: jsonMessage });
            });
          }
        },
        onReconnectStop: (attempts) =>
          console.log(
            "Stopped trying to reconnect after " + attempts + " attempts"
          ),
        //Will attempt to reconnect on all close events, such as server shutting down
        shouldReconnect: (closeEvent) => true,
        reconnectAttempts: 1000000000,
        reconnectInterval: 1000,
        retryOnError: true,
      },
      this.state.shouldWebsocketOpen
    );

    const selected = this.state.selectedPage;
    const lastMsg = this.state.lastMsg;

    return (
      <React.Fragment>
        {lastMsg && selected === Dashboard.getPageName() && (
          <Dashboard
            sendJsonMessage={sendJsonMessage}
            state={lastMsg}
          />
        )}

        {lastMsg && selected === Stand.getPageName() && (
          <Stand
            sendJsonMessage={sendJsonMessage}
            state={lastMsg.device.stand}
          />
        )}
        {lastMsg && selected === Speakerbar.getPageName() && (
          <Speakerbar
            sendJsonMessage={sendJsonMessage}
            state={lastMsg.device.speaker}
          />
        )}
        {lastMsg && selected === IOOut.getPageName() && (
          <IOOut
            sendJsonMessage={sendJsonMessage}
            state={lastMsg.io_out}
          />
        )}

        {lastMsg && selected === Current.getPageName() && (
          <Current
            sendJsonMessage={sendJsonMessage}
            state={lastMsg.current.info}
          />
        )}
        {lastMsg && selected === IOController.getPageName() && (
          <IOController
            sendJsonMessage={sendJsonMessage}
            state={lastMsg.io}
          />
        )}
        {lastMsg && selected === SystemStatus.getPageName() && (
          <SystemStatus sendJsonMessage={sendJsonMessage} state={lastMsg} />
        )}
        {lastMsg && selected === OTA.getPageName() && (
          <OTA sendJsonMessage={sendJsonMessage} state={lastMsg} />
        )}
        {lastMsg && selected === LEDSettings.getPageName() && (
          <LEDSettings
            sendJsonMessage={sendJsonMessage}
            state={lastMsg.internal}
          />
        )}
      </React.Fragment>
    );
  }

  render() {
    return (
      <ThemeProvider theme={defaultTheme}>
        <Box sx={{ display: "flex" }}>
          <CssBaseline />
          <TopBar
            selectedPage={this.state.selectedPage}
            timestamp={this.state.lastMessageTimestamp}
            pages={this.pages}
            setPage={this.setPage}
            main={this.getMain()}
          />
        </Box>
      </ThemeProvider>
    );
  }
}

export default App;
