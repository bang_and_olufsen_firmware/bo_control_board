#include "WebServer.hpp"

#include <DNSServer.h>
#include <WiFi.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "Current/Current.hpp"
#include "Device/Device.hpp"
#include "Device/Stand.hpp"
#include "ESP-FTP-Server-Lib.h"
#include "FTPFilesystem.h"
#include "Internal/Internal.hpp"
#include "OTA/OTA.hpp"
#include "WSHandler.hpp"
#include "WebServerUtils.hpp"
#include "esp_timer.h"
#include "filesystem/filesystem.hpp"
#include <iostream>

#define FTP_USER     "ftp"
#define FTP_PASSWORD "ftp"

#include "mongoose/mongoose.h"

static struct mg_mgr mgr;  // Event manager
static TaskHandle_t  webserver_task_handle = NULL;

static DNSServer      dns;
static FTPServer      ftp;
static const uint64_t RECONNECT_TIMEOUT_US = 15 * 1000 * 1000;

namespace webserver
{

static void Webserver_Task(void* arg);

void webserver_init()
{
  xTaskCreate(Webserver_Task, "WEBSERVER", 14000, nullptr, 10, &webserver_task_handle);
}

void Webserver_Task(void* arg)
{
  WiFi.mode(WIFI_STA);
  WiFi.begin(/*"ssid", "pass"*/);
  WiFi.setAutoReconnect(true);

  ftp.addUser(FTP_USER, FTP_PASSWORD);
  ftp.addFilesystem("firmware", &FirmwareFS);

  ftp.begin();
  mg_mgr_init(&mgr);       // Initialise event manager
  mg_log_set(MG_LL_INFO);  // Set log level
  mg_timer_add(&mgr, 100, MG_TIMER_REPEAT, WSHandler::broadcastUpdate, &mgr);
  mg_http_listen(&mgr, "ws://0.0.0.0:80", WSHandler::eventHandler, NULL);  // Create HTTP listener

  uint64_t lastConnected = esp_timer_get_time();
  while(1)
  {
    mg_mgr_poll(&mgr, 50);
    ftp.handle();

    if(WiFi.status() == WL_CONNECTED)
    {
      lastConnected = esp_timer_get_time();
    }
    else if(esp_timer_get_time() - lastConnected > RECONNECT_TIMEOUT_US)
    {
      WiFi.disconnect();
      WiFi.reconnect();
      lastConnected = esp_timer_get_time();
    }
  }
}

}  // namespace webserver