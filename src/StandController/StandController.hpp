#pragma once

#include <ArduinoJson.h>

#include "CANBootloader/CANBootloader.hpp"
#include "CANController/CANController.hpp"
#include "Controller/Controller.hpp"
#include "freertos/queue.h"
#include "nvs_flash.h"
#include <unordered_map>
struct Waypoint {
  Waypoint() : m1_pos(0xFFFF), m2_pos(0xFFFF) {}
  Waypoint(uint16_t m1, uint16_t m2) : m1_pos(m1), m2_pos(m2) {}
  uint16_t m1_pos = 0xFFFF;
  uint16_t m2_pos = 0xFFFF;
};

struct Trajectory {
  Trajectory() {}
  uint32_t                 movement_time = 30000;
  uint8_t                  waypoint_cnt  = 0;
  std::array<Waypoint, 20> waypoints;
  void                     removeWaypoint(int index)
  {
    for(size_t i = index; i < waypoints.size() - 1; i++)
    {
      waypoints[i] = waypoints[i + 1];
    }
    waypoints.end()->m1_pos = 0xFFFF;
    waypoints.end()->m2_pos = 0xFFFF;

    setWaypointCount();
  }

  void Reverse() { std::reverse(waypoints.begin(), waypoints.begin() + waypoint_cnt); }

  void setWaypointCount()
  {
    waypoint_cnt = 0;
    for(size_t i = 0; i < waypoints.size(); i++)
    {
      if(waypoints[i].m1_pos == 0xFFFF || waypoints[i].m2_pos == 0xFFFF)
      {
        break;
      }
      waypoint_cnt = i + 1;
    }
  }
  void SerializeToJson(JsonObject& json) const
  {
    json["time"]           = movement_time * 0.001f;
    JsonArray waypointJson = json.createNestedArray("waypoints");
    for(auto& waypoint : waypoints)
    {
      if(waypoint.m1_pos == 0xFFFF || waypoint.m2_pos == 0xFFFF)
      {
        break;
      }
      JsonObject w = waypointJson.createNestedObject();
      w["m1"]      = (waypoint.m1_pos / 4095.f) * 360;
      w["m2"]      = (waypoint.m2_pos / 4095.f) * 360;
    }
  }
  static bool validateJson(const JsonVariant& traj)
  {
    if(!traj.containsKey("name"))
    {
      return false;
    }
    if(!traj.containsKey("time") || !traj["time"].is<float>())
    {
      return false;
    }
    if(!traj.containsKey("waypoints"))
    {
      return false;
    }
    JsonArray jsonWaypoints = traj["waypoints"];
    for(const auto& w : jsonWaypoints)
    {
      if(!w.containsKey("m1") || !w["m1"].is<float>())
      {
        return false;
      }
      if(!w.containsKey("m2") || !w["m2"].is<float>())
      {
        return false;
      }
    }
    return true;
  }

  static Trajectory fromJson(const JsonVariant& json)
  {
    Trajectory traj;
    traj.movement_time      = json["time"].as<float>() * 1000;
    JsonArray jsonWaypoints = json["waypoints"];
    uint8_t   cnt           = 0;
    for(const auto& w : jsonWaypoints)
    {
      if(cnt >= traj.waypoints.size())
      {
        break;
      }
      traj.waypoints[cnt].m1_pos = w["m1"].as<float>() * 4095.f / 360.f;
      traj.waypoints[cnt].m2_pos = w["m2"].as<float>() * 4095.f / 360.f;
      cnt++;
    }
    return traj;
  }
};

struct TrajectoryState {
 public:
  enum class State {
    IDLE,
    STOPPING,
    WAYPOINTS,
    STARTING,
  };
  Trajectory traj;
  State      state = State::IDLE;
  uint8_t    acked_waypoints;
  uint8_t    m1_waypoint = 0xAA;
  uint8_t    m2_waypoint = 0xAA;
  uint32_t   acked_crc;
};

class StandController : public Controller {
 public:
  StandController(CANController& bus, CANBootloader& bootloader);
  ~StandController() = default;
  void     step() override;
  float    getMotor1Speed() { return motor1.speed; }
  float    getMotor2Speed() { return motor2.speed; }
  uint32_t getMotor1Target() { return motor1.target; }
  uint32_t getMotor2Target() { return motor2.target; }
  float    getEncoder1Speed() { return motor1.encoder_speed; }
  float    getEncoder2Speed() { return motor2.encoder_speed; }

  uint16_t getEncoder1Position() { return motor1.position; }
  uint16_t getEncoder2Position() { return motor2.position; }
  float    getMotor1NeededSpeed() { return motor1.needed_speed; }
  float    getMotor2NeededSpeed() { return motor2.needed_speed; }
  float    getMotor1FeedbackSpeed() { return motor1.motor_feedback_speed; }
  float    getMotor2FeedbackSpeed() { return motor2.motor_feedback_speed; }
  uint32_t getMotor1FeedbackTicks() { return motor1.motor_feedback_ticks; }
  uint32_t getMotor2FeedbackTicks() { return motor2.motor_feedback_ticks; }
  uint32_t getUint32Debug(uint8_t i) { return debug_uint32[i]; }
  float    getFloatDebug(uint8_t i) { return debug_float[i]; }

  void                                        setTrajectory(Trajectory traj);
  void                                        reload() { shouldReload = true; }
  std::unordered_map<std::string, Trajectory> getTrajectories();
  std::pair<Trajectory, bool>                 getTrajectory(const std::string& name);
  std::string                                 getDefaultTrajectory();
  void                                        setTrajectory(Trajectory traj, const std::string& name, bool is_default);
  void                                        deleteTrajectory(const std::string& name);
  void                                        goToTrajectoryEnd(const std::string& name);
  void                                        goToTrajectoryStart(const std::string& name);
  void                                        goToWaypoint(const Waypoint w);
  void                                        stopMovement();

 private:
  void handleEncoderStatus(const twai_message_t& msg);
  void handleEncoderSpeeds(const twai_message_t& msg);
  void handleNeededSpeeds(const twai_message_t& msg);
  void handleMotorFeedbackSpeeds(const twai_message_t& msg);
  void handleMotorFeedbackTicks(const twai_message_t& msg);
  void handleFloatDebug1(const twai_message_t& msg);
  void handleFloatDebug2(const twai_message_t& msg);
  void handleUint32Debug1(const twai_message_t& msg);
  void handleUint32Debug2(const twai_message_t& msg);
  void handleTrajectoryStatus(const twai_message_t& msg);
  void handleMotorStatus(const twai_message_t& msg);
  void handleTargets(const twai_message_t& msg);

  void     trajectoryStateStep();
  void     sendWaypoint(Waypoint* w, uint8_t index, uint8_t cnt);
  void     sendReset();
  void     sendStart(uint8_t waypoints, uint32_t movement_time, uint32_t crc);
  uint32_t computeTrajectoryCRC(Waypoint* waypoints, uint8_t cnt);

  void loadTrajectories();

  QueueHandle_t queueTrajectory;

  struct MotorData {
    float    speed                = 0;
    uint32_t position             = 0;
    float    encoder_speed        = 0;
    float    needed_speed         = 0;
    uint16_t target               = 0;
    float    motor_feedback_speed = 0;
    uint32_t motor_feedback_ticks = 0;
  };

  MotorData       motor1;
  MotorData       motor2;
  TrajectoryState trajectory_state;
  uint32_t        debug_uint32[4] = {0};
  float           debug_float[4]  = {0};

  bool                                        shouldReload = false;
  nvs_handle_t                                trajectory_handle;
  std::unordered_map<std::string, Trajectory> trajectories;
  std::string                                 default_trajectory;
  SemaphoreHandle_t                           trajectorySemaphore;
};