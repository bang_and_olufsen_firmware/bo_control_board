#include "Speaker.hpp"

#include "ArduinoJson.h"
#include "Common.hpp"
#include "WebServer/WebServerUtils.hpp"
#include "tasks/can_task/can_task.hpp"
#include <algorithm>
#include <string>
namespace Device
{

void addSpeakerStatus(JsonVariant& json)
{
  auto& speaker = can_task_get_speaker();

  JsonVariant device     = WebServerUtils::getOrCreateObject(json, "device");
  JsonVariant speakerObj = WebServerUtils::getOrCreateObject(device, "speaker");

  JsonVariant status          = WebServerUtils::getOrCreateObject(speakerObj, "status");
  status["time_since_update"] = speaker.getTimeSinceLastStatus() * 0.000001;
  status["wanted_target"]     = speakermotor::BOSpeakerControlTarget_str(speaker.getWantedTarget());
  status["actual_target"]     = speakermotor::BOSpeakerControlTarget_str(speaker.getActualTarget());
  status["state"]             = speakermotor::BOSpeakerControlState_str(speaker.getState());
  status["target_speed"]      = speaker.getTargetSpeed();
  status["recieved_speed"]    = speaker.getRecievedSpeed();
  status["target_pwm"]        = speaker.getTargetPWMFrequency();
  status["recieved_pwm"]      = speaker.getRecievedPWMFrequency();

  JsonVariant bootstatus = WebServerUtils::getOrCreateObject(speakerObj, "bootstatus");
  addBootloaderStatus(bootstatus, speaker.getBootloader());
  JsonVariant stats = WebServerUtils::getOrCreateObject(speakerObj, "stats");
  addStats(stats, speaker);
}

void handleSpeakerMessage(const JsonVariant& json)
{
  if(json.containsKey("action"))
  {
    if(json["action"].as<const char*>() == std::string("reload"))
    {
      can_task_get_speaker().reload();
    }
  }
  if(json.containsKey("target"))
  {
    std::string targetStr = json["target"].as<const char*>();
    std::transform(targetStr.begin(), targetStr.end(), targetStr.begin(), ::toupper);
    auto target = speakermotor::str_BOSpeakerControlTarget(targetStr);
    if(target != speakermotor::BOSpeakerControlTarget::UNKNOWN)
    {
      auto& speaker = can_task_get_speaker();
      speaker.setNewTarget(target);
    }
    else
    {
      log_i("Unknown speaker target %s", targetStr.c_str());
    }
  }
  if(json.containsKey("speed") && json["speed"].is<float>())
  {
    can_task_get_speaker().setTargetSpeed(json["speed"].as<float>());
  }
  if(json.containsKey("pwm") && json["pwm"].is<uint16_t>())
  {
    can_task_get_speaker().setTargetPWMFrequency(json["pwm"].as<uint16_t>());
  }
}

}  // namespace Device