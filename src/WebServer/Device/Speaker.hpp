#pragma once

#include "ArduinoJson.h"

namespace Device
{
void addSpeakerStatus(JsonVariant& json);
void handleSpeakerMessage(const JsonVariant& json);

}  // namespace Device