#include "mount_littlefs_blob.hpp"

#include <string.h>

#include "esp_littlefs.h"

esp_err_t mount_littlefs_blob(const void* start, const void* end, const char* base_path)
{
  esp_partition_t* new_part = (esp_partition_t*)malloc(sizeof(esp_partition_t));

  new_part->flash_chip = esp_flash_default_chip;
  new_part->type       = ESP_PARTITION_TYPE_DATA;
  new_part->subtype    = ESP_PARTITION_SUBTYPE_DATA_FAT;
  new_part->address    = spi_flash_cache2phys(start);
  new_part->size       = ((uint32_t)end) - ((uint32_t)start);
  memset(new_part->label, 0, sizeof(new_part->label));
  new_part->encrypted = false;

  esp_vfs_littlefs_conf_t conf = {
    .base_path              = base_path,
    .partition_label        = NULL,
    .partition              = new_part,
    .format_if_mount_failed = false,
    .read_only              = true,
    .dont_mount             = false,
    .grow_on_mount          = false,
  };

  return esp_vfs_littlefs_register(&conf);
}