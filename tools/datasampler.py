import websocket
import json
import zlib
import sys
import csv
ws = websocket.WebSocket()

ws.connect("ws://192.168.203.108/ws")

channels = [ 
    ({"internal" : "uptime"}, "Uptime"),
    ({"device" : {"stand" : { "status" : {"motors" : {0 : "speed"}}}}}, "M0 Speed"),
    ({"device" : {"stand" : { "status" : {"motors" : {0 : "encoder_speed"}}}}}, "M0 Encoder Speed"),
    ({"device" : {"stand" : { "status" : {"motors" : {0 : "encoder"}}}}}, "M0 Encoder"),
    ({"device" : {"stand" : { "status" : {"motors" : {0 : "target"}}}}}, "M0 target"),
    ({"device" : {"stand" : { "status" : {"motors" : {0 : "last_needed_speed"}}}}}, "M0 Last Needed Speed"),
    ({"device" : {"stand" : { "status" : {"motors" : {0 : "feedback_speed"}}}}}, "M0 Feedback Speed"),
    ({"device" : {"stand" : { "status" : {"motors" : {0 : "feedback_ticks"}}}}}, "M0 Feedback Ticks"),
    ({"device" : {"stand" : { "status" : {"motors" : {1 : "speed"}}}}}, "M1 Speed"),
    ({"device" : {"stand" : { "status" : {"motors" : {1 : "encoder_speed"}}}}}, "M1 Encoder Speed"),
    ({"device" : {"stand" : { "status" : {"motors" : {1 : "encoder"}}}}}, "M1 Encoder"),
    ({"device" : {"stand" : { "status" : {"motors" : {1 : "target"}}}}}, "M1 target"),
    ({"device" : {"stand" : { "status" : {"motors" : {1 : "last_needed_speed"}}}}}, "M1 Last Needed Speed"),
    ({"device" : {"stand" : { "status" : {"motors" : {1 : "feedback_speed"}}}}}, "M1 Feedback speed"),
    ({"device" : {"stand" : { "status" : {"motors" : {1 : "feedback_ticks"}}}}}, "M1 Feedback Ticks"),
    ({"device" : {"stand" : { "status" : {"debug_float" : 0}}}}, "Debug 0 Float"),
    ({"device" : {"stand" : { "status" : {"debug_float" : 1}}}}, "Debug 1 Float"),
    ({"device" : {"stand" : { "status" : {"debug_float" : 2}}}}, "Debug 2 Float"),
    ({"device" : {"stand" : { "status" : {"debug_float" : 3}}}}, "Debug 3 Float"),

]
output = sys.argv[1]


def extract_data(access, data):
    if(isinstance(access, str)) or isinstance(access, int):
        try:
            return data[access]
        except KeyError as e:
            print(access)
            print(json.dumps(data, sort_keys=True, indent=4))
            raise e
    key = list(access.keys())[0]
    try:
        subdata = data[key]
    except KeyError as e:
        print(key)
        print(json.dumps(data, sort_keys=True, indent=4))
        raise e

    return extract_data(access[key], data[key])

with open(output, "w") as csvfile:
    fieldnames = [channel[1] for channel in channels]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    cnt = 0
    while True:
        row = {}
        recieved = json.loads(zlib.decompress(ws.recv()))
        for channel in channels:
            row[channel[1]] = extract_data(channel[0], recieved)
        writer.writerow(row)
        cnt += 1
        if(cnt % 3 == 0):
            csvfile.flush()

