#pragma once

#include "ArduinoJson.h"

namespace Current
{
void addCurrent(JsonVariant& json);
void handleMessage(const JsonVariant& json);
}  // namespace Current
