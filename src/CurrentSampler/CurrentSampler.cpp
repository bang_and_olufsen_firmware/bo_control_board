#include "CurrentSampler.hpp"

#include <Arduino.h>
#include <driver/adc.h>

#include "esp_adc_cal.h"
#include "esp_timer.h"

#define GET_UNIT(x) ((x >> 3) & 0x1)

CurrentSampler::CurrentSampler(adc_channel_t channel) : channel(channel) {}

void CurrentSampler::start()
{
  ESP_ERROR_CHECK(esp_adc_cal_check_efuse(ESP_ADC_CAL_VAL_EFUSE_TP_FIT));

  esp_adc_cal_characteristics_t calibration;
  esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_11db, ADC_WIDTH_12Bit, 0, &calibration);

  for(uint32_t i = 0; i < calibration_lookup.size(); i++)
  {
    calibration_lookup[i] = esp_adc_cal_raw_to_voltage(i, &calibration) * CONVERSION_VOLTAGE_TO_MEASUREMENT;
  }

  adc_digi_init_config_t adc_dma_config = {
    .max_store_buf_size = 2048,
    .conv_num_each_intr = 256,
    .adc1_chan_mask     = BIT(channel),
    .adc2_chan_mask     = 0,
  };

  ESP_ERROR_CHECK(adc_digi_initialize(&adc_dma_config));

  adc_digi_pattern_config_t adc_pattern = {
    .atten     = ADC_ATTEN_11db,
    .channel   = channel,
    .unit      = (uint8_t)GET_UNIT(channel),
    .bit_width = 12,
  };

  adc_digi_configuration_t dig_cfg = {
    .conv_limit_en  = 0,
    .conv_limit_num = 250,
    .pattern_num    = 1,
    .adc_pattern    = &adc_pattern,
    .sample_freq_hz = SAMPLE_RATE,
    .conv_mode      = ADC_CONV_SINGLE_UNIT_1,
    .format         = ADC_DIGI_OUTPUT_FORMAT_TYPE2,
  };
  ESP_ERROR_CHECK(adc_digi_controller_configure(&dig_cfg));
  ESP_ERROR_CHECK(adc_digi_start());

  rms.begin(MAX_SAMPLE_VOLTAGE, RMS_WINDOW, 12, BLR_ON, CNT_SCAN);
  rms.start();

  next_publish = RMS_WINDOW;

  sample_start_time = esp_timer_get_time();
}

void CurrentSampler::step()
{
  uint32_t sample_count = 0;
  uint8_t  samples[1024];
  adc_digi_read_bytes(samples, sizeof(samples), &sample_count, 0);
  for(uint32_t sample = 0; sample < sample_count; sample += sizeof(adc_digi_output_data_t))
  {
    adc_digi_output_data_t* p = (adc_digi_output_data_t*)&samples[sample];
    rms.update(movingSampleAvg.update(calibration_lookup[p->type2.data]) * 0.001f);
    total_samples++;
    if(total_samples > next_publish)
    {
      next_publish += RMS_WINDOW;
      rms.publish();
      if(rms.error < 5)
      {
        rmsMedian.update(rms.rmsVal);
        if(rmsMedian.ready())
        {
          new_measurement_ready = true;
        }
      }
    }
  }
}

float CurrentSampler::getSampleRate() const
{
  return total_samples / ((esp_timer_get_time() - sample_start_time) * 0.000001f);
}