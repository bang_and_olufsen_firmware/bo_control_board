#include "FirmwareBuffer.hpp"

#include "filesystem/filesystem.hpp"
#include <iostream>

FirmwareBuffer::FirmwareBuffer(std::string path) : path(path)
{
  readIntoBuffer(0);
}

bool FirmwareBuffer::readIntoBuffer(uint32_t address)
{
  File f = FirmwareFS.open(path.c_str());
  size   = f.size();

  bool ret = true;
  if(!f.available())
  {
    ret = false;
  }
  if(!ret || !f.seek(address) || !f.available())
  {
    size = f.size();
    ret  = false;
  }

  if(ret)
  {
    auto remaining = f.available();
    auto readSize  = remaining > buffer.size() ? buffer.size() : remaining;
    f.read(buffer.data(), readSize);
    startIndex = address;
    endIndex   = startIndex + readSize;
  }

  if(!ret)
  {
    startIndex = 0;
    endIndex   = 0;
  }

  f.close();
  return ret;
}

uint32_t FirmwareBuffer::getData(uint32_t address, uint8_t* data, uint32_t len)
{
  if(size == 0)
  {
    return 0;
  }
  for(uint32_t addr = address; addr < address + len; addr++)
  {
    if(addr < startIndex || addr >= endIndex || size == 0)
    {
      return addr - address;
    }
    data[addr - address] = buffer[addr - startIndex];
  }
  return len;
}