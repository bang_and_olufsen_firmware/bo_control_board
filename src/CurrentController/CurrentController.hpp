#pragma once
#include "CurrentSampler/CurrentSampler.hpp"
#include "GPIOController/GPIOController.hpp"
#include "OnOffDetector/OnOffDetector.hpp"
class CurrentController : public OnOffDetector {
 public:
  CurrentController(SpeakerController& speaker, StandController& stand, TVLEDController& tv_led, GPIOController& gpio_controller);
  ~CurrentController() = default;
  void                  step();
  const CurrentSampler& get_sampler() { return sampler; };
  void                  start() { sampler.start(); }
  void                  setOnThreshold(float val);
  void                  setOffThreshold(float val);
  float                 offThreshold() const { return off_threshold; }
  float                 onThreshold() const { return on_threshold; }
  void                  setAmperageGain(float gain);
  float                 getAmperageGain() const { return sampler.getAmperageGain(); }
  float                 getMaxAmperageInState() const { return max_current_in_state; }
  float                 getMinAmperageInState() const { return min_current_in_state; }

 private:
  void         loadSubSettings();
  virtual void stateChangeHook();

  CurrentSampler sampler;
  float          off_threshold        = 0;
  float          on_threshold         = 0;
  float          max_current_in_state = 0;
  float          min_current_in_state = 0;
  float          lastRMSCurrent       = 0;
};