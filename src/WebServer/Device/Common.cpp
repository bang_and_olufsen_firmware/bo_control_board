#include "Common.hpp"

namespace Device
{

void addBootloaderStatus(JsonVariant& json, CANBootloader& bootloader)
{
  json["loaded_crc"] = bootloader.getLoadedCRC();
  json["target_crc"] = bootloader.getCachedCRC();
  json["progress"]   = bootloader.getProgress();
}

void addStats(JsonVariant& json, Controller& controller)
{
  json["total_heap"] = controller.getTotalHeap();
  json["used_heap"]  = controller.getUsedHeap();
  json["uptime"]     = controller.getUptime() / 1000.;
  json["load"]       = controller.getLoad();
}

}  // namespace Device