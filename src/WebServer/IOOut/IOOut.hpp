#pragma once

#include "ArduinoJson.h"

namespace IOOut
{
void addIOOut(JsonVariant& json);
void handleMessage(const JsonVariant& json);
}  // namespace IOOut
