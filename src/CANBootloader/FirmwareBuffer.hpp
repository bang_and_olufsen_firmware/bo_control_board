#pragma once
#include <stdint.h>

#include "FS.h"
#include <array>
#include <string>

class FirmwareBuffer {
 public:
  FirmwareBuffer(std::string path);
  ~FirmwareBuffer() = default;
  uint32_t    getData(uint32_t address, uint8_t* data, uint32_t len);
  uint32_t    getSize() { return size; }
  std::string getPath() { return path; }
  void        reset() { readIntoBuffer(0); }

 private:
  bool                        readIntoBuffer(uint32_t address);
  std::string                 path;
  uint32_t                    startIndex;
  uint32_t                    endIndex;
  uint32_t                    size;
  std::array<uint8_t, 100000> buffer;
};