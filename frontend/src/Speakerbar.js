import React, { Component } from "react";
import Container from "@mui/material/Container";
import { InfoTable } from "./InfoTable";
import getDeviceStatRows from "./DeviceStats";
import getDeviceBootStatusRows from "./BootStatus";
import { Paper } from "@mui/material";
import VolumeUpIcon from "@mui/icons-material/VolumeUp";
import { TextField, Button } from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";
import Box from "@mui/material/Box";

function createData(name, value) {
  return { name, value };
}
class Speakerbar extends Component {
  constructor(props, context) {
    super();
    this.sendStop = this.sendStop.bind(this);
    this.sendFold = this.sendFold.bind(this);
    this.sendUnfold = this.sendUnfold.bind(this);
    this.sendSpeed = this.sendSpeed.bind(this);
    this.getSpeed = this.getSpeed.bind(this);
    this.sendPWM = this.sendPWM.bind(this);
    this.getPWM = this.getPWM.bind(this);

    this.state = {
      speed_set_time: new Date(),
      target_pwm_set_time: new Date(),
    };
  }

  getPWM() {
    if (
      Date.now() - this.state["target_pwm_set_time"] < 2000 &&
      "target_pwm" in this.state
    ) {
      return this.state.target_pwm;
    }
    return this.props.state.status.target_pwm;
  }

  sendPWM(pwm) {
    this.props.sendJsonMessage({
      device: { speaker: { pwm: parseInt(pwm) } },
    });
    this.setState({
      target_pwm: pwm,
      target_pwm_set_time: new Date(),
    });
  }


  getSpeed() {
    if (
      Date.now() - this.state["target_speed_set_time"] < 2000 &&
      "target_speed" in this.state
    ) {
      return this.state.target_speed;
    }
    return this.props.state.status.target_speed;
  }

  sendSpeed(speed) {
    this.props.sendJsonMessage({
      device: { speaker: { speed: parseFloat(speed) } },
    });
    this.setState({
      target_speed: speed,
      target_speed_set_time: new Date(),
    });
  }

  sendStop() {
    this.props.sendJsonMessage({ device: { speaker: { target: "stop" } } });
  }

  sendFold() {
    this.props.sendJsonMessage({ device: { speaker: { target: "fold_in" } } });
  }

  sendUnfold() {
    this.props.sendJsonMessage({ device: { speaker: { target: "unfold" } } });
  }

  static getPageName() {
    return "Speakerbar";
  }

  static getPageIcon() {
    return <VolumeUpIcon />;
  }

  render() {
    const rows = [createData("State", this.props.state.status.state)]
      .concat(
        createData("Wanted Target", this.props.state.status.wanted_target)
      )
      .concat(
        createData("Actual Target", this.props.state.status.actual_target)
      )
      .concat(
        createData(
          "Speed",
          (this.props.state.status.recieved_speed * 100).toFixed(2) + "%"
        )
      )
      .concat(
        createData(
          "PWM Frequency",
          this.props.state.status.recieved_pwm + "Hz"
        )
      )

      .concat(getDeviceStatRows("", this.props.state.stats))
      .concat(getDeviceBootStatusRows("", this.props.state.bootstatus))
      .concat(
        createData(
          "Time since update",
          this.props.state.status.time_since_update.toFixed(1) + " s"
        )
      );

    return (
      <Container disableGutters={true} maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
          }}
        >
          <Button
            variant="contained"
            style="margin:5px"
            onClick={() => {
              this.sendStop();
            }}
          >
            Stop
          </Button>
          <Button
            variant="contained"
            style="margin:5px"
            onClick={() => {
              this.sendUnfold();
            }}
          >
            Unfold
          </Button>
          <Button
            variant="contained"
            style="margin:5px"
            onClick={() => {
              this.sendFold();
            }}
          >
            Fold
          </Button>
          <Box sx={{ m: 2 }}>
            <TextField
              margin="normal"
              value={this.getSpeed()}
              step={0.1}
              type="number"
              min={0}
              max={100}
              InputProps={{
                endAdornment: <InputAdornment position="end">%</InputAdornment>,
              }}
              id="outlined-basic"
              label="Speed"
              variant="outlined"
              onChange={(event) => {
                this.sendSpeed(event.target.value);
              }}
            />
            {'    '}
            <TextField
              margin="normal"
              value={this.getPWM()}
              step={1}
              type="number"
              min={50}
              max={65000}
              InputProps={{
                endAdornment: <InputAdornment position="end">Hz</InputAdornment>,
              }}
              id="outlined-basic"
              label="PWM Frequency"
              variant="outlined"
              onChange={(event) => {
                this.sendPWM(event.target.value);
              }}
            />
          </Box>
        </Paper>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
          }}
        >
          <InfoTable name="Speakerbar Status" valuecolumn="Value" rows={rows} />
        </Paper>
      </Container>
    );
  }
}

export default Speakerbar;
