#include "misc_task.hpp"

#include <Arduino.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "CurrentController/CurrentController.hpp"
#include "GPIOController/GPIOController.hpp"
#include "OnOffDetector/IoOnOffDetector.hpp"
#include "tasks/can_task/can_task.hpp"

static void MISC_Task(void* arg);

static TaskHandle_t task_handle = NULL;

static ContinuousLEDController* led;
static TVLEDController*         tv_led_controller;

static CurrentController* current_controller;
static IoOnOffDetector*   io_controller;
static GPIOController*    gpio_controller;

void misc_task_init()
{
  tv_led_controller = new TVLEDController(
    *(new OneShotLEDController(GPIO_NUM_17, LEDC_CHANNEL_1, "nvs_tvled_red", true)),
    *(new OneShotLEDController(GPIO_NUM_18, LEDC_CHANNEL_2, "nvs_tvled_green", true)));

  gpio_controller = new GPIOController(GPIO_NUM_15, "gpio_cont_1");

  current_controller =
    new CurrentController(can_task_get_speaker(), can_task_get_stand(), *tv_led_controller, *gpio_controller);
  io_controller = new IoOnOffDetector(can_task_get_speaker(), can_task_get_stand(), *tv_led_controller, *gpio_controller);

  led = new ContinuousLEDController(GPIO_NUM_12, LEDC_CHANNEL_0, "nvs_led");
  xTaskCreatePinnedToCore(MISC_Task, "MISC Task", 5000, nullptr, 12, &task_handle, 1);
}

void MISC_Task(void* arg)
{
  current_controller->start();

  TickType_t lastWake = xTaskGetTickCount();
  while(true)
  {
    current_controller->step();
    io_controller->step();
    led->step();
    tv_led_controller->step();
    gpio_controller->step();

    xTaskDelayUntil(&lastWake, 1 / portTICK_PERIOD_MS);
  }
}

CurrentController& misc_task_get_current()
{
  return *current_controller;
}

GPIOController& misc_task_get_gpio_controller()
{
  return *gpio_controller;
}

IoOnOffDetector& misc_task_get_io_controller()
{
  return *io_controller;
}

ContinuousLEDController& misc_task_get_led()
{
  return *led;
}

TVLEDController& misc_task_get_tv_led()
{
  return *tv_led_controller;
}
