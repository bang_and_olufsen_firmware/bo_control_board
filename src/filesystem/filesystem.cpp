#include "filesystem.hpp"

#include <dirent.h>
#include <sys/types.h>

#include "esp_littlefs.h"
#include "esp_log.h"
#include "mount_littlefs_blob.hpp"
#include "vfs_api.h"

#define FW_MOUNTPOINT "/firmware"

class VFSWithMountPoint : public VFSImpl {
 public:
  VFSWithMountPoint(const char* _mountpoint) { this->mountpoint(_mountpoint); }
};

fs::FS FirmwareFS(fs::FSImplPtr(new VFSWithMountPoint(FW_MOUNTPOINT)));

struct dir_size_t {
  uint64_t filesize;
  int      file_count;
};

[[maybe_unused]] static dir_size_t list(const char* path, size_t path_len, size_t depth)
{
  DIR*           dir = NULL;
  struct dirent* ent;
  char           type;
  char           size[30];
  char           tpath[255];
  char           tbuffer[80];
  struct stat    sb;
  struct tm*     tm_info;
  char*          lpath = NULL;
  int            statok;

  if(depth == 0)
  {
    printf("\nList of Directory [%s]\n", path);
    printf("-----------------------------------\n");
  }
  // Open directory
  dir = opendir(path);
  if(!dir)
  {
    printf("Error opening directory\n");
    return {0, 0};
  }

  // Read directory entries
  uint64_t total  = 0;
  int      nfiles = 0;
  if(depth == 0)
  {
    printf("T  Size      Date/Time         Name\n");
    printf("-----------------------------------\n");
  }
  while((ent = readdir(dir)) != NULL)
  {
    sprintf(tpath, path);
    if(path[strlen(path) - 1] != '/')
      strcat(tpath, "/");
    strcat(tpath, ent->d_name);
    tbuffer[0] = '\0';

    // Get file stat
    statok = stat(tpath, &sb);

    if(statok == 0)
    {
      tm_info = localtime(&sb.st_mtime);
      strftime(tbuffer, 80, "%d/%m/%Y %R", tm_info);
    }
    else
      sprintf(tbuffer, "                ");

    if(ent->d_type == DT_REG)
    {
      type = 'f';
      nfiles++;
      if(statok)
        strcpy(size, "       ?");
      else
      {
        total += sb.st_size;
        if(sb.st_size < (1024 * 1024))
          sprintf(size, "%8d", (int)sb.st_size);
        else if((sb.st_size / 1024) < (1024 * 1024))
          sprintf(size, "%6dKB", (int)(sb.st_size / 1024));
        else
          sprintf(size, "%6dMB", (int)(sb.st_size / (1024 * 1024)));
      }
    }
    else
    {
      type = 'd';
      strcpy(size, "       -");
    }

    printf("%c  %s  %s", type, size, tbuffer);
    for(size_t i = 0; i < depth; i++)
    {
      printf("  ");
    }
    printf("  %s\r\n", ent->d_name);

    if(type == 'd')
    {
      const size_t subpath_len = path_len + strlen(ent->d_name) + 5;
      char*        subpath     = new char[subpath_len];
      int          n           = snprintf(subpath, subpath_len, "%s/%s", path, ent->d_name);
      if(n > 0)
      {
        auto res = list(subpath, n, depth + 1);
        total += res.filesize;
        nfiles += res.file_count;
      }
      delete[] subpath;
    }
  }
  if(total && depth == 0)
  {
    printf("-----------------------------------\n");
    if(total < (1024 * 1024))
      printf("   %8d", (int)total);
    else if((total / 1024) < (1024 * 1024))
      printf("   %6dKB", (int)(total / 1024));
    else
      printf("   %6dMB", (int)(total / (1024 * 1024)));
    printf(" in %d file(s)\n", nfiles);
    printf("-----------------------------------\n");
  }

  closedir(dir);

  free(lpath);
  return {total, nfiles};
}

void filesystem_init()
{
  esp_vfs_littlefs_conf_t fw_conf = {
    .base_path              = FW_MOUNTPOINT,
    .partition_label        = "firmware",
    .partition              = NULL,
    .format_if_mount_failed = true,
    .read_only              = false,
    .dont_mount             = false,
    .grow_on_mount          = false,
  };

  ESP_ERROR_CHECK(esp_vfs_littlefs_register(&fw_conf));

  extern const uint8_t website_blob_start[] asm("_binary_website_bin_start");
  extern const uint8_t website_blob_end[] asm("_binary_website_bin_end");

  ESP_ERROR_CHECK(mount_littlefs_blob(website_blob_start, website_blob_end, "/website"));

  // list("/firmware", sizeof("/firmware"), 0);
  // list("/website", sizeof("/website"), 0);
}