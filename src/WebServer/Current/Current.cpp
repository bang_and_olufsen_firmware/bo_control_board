#include "Current.hpp"

#include "ArduinoJson.h"
#include "WebServer/WebServerUtils.hpp"
#include "tasks/misc_task/misc_task.hpp"

namespace Current
{

void addCurrent(JsonVariant& json)
{
  auto&       controller = misc_task_get_current();
  const auto& sampler    = controller.get_sampler();
  JsonVariant current    = WebServerUtils::getOrCreateObject(json, "current");
  JsonVariant info       = WebServerUtils::getOrCreateObject(current, "info");

  info["rms_current"]      = sampler.getRMS() * 1000.f;
  info["bias"]             = sampler.getBias() * 1000.f;
  info["error"]            = sampler.getError();
  info["samplerate"]       = sampler.getSampleRate();
  info["state"]            = controller.getStateStr();
  info["time_since_state"] = controller.getTimeSinceStateChange();
  info["max_ma_in_state"]  = controller.getMaxAmperageInState();
  info["min_ma_in_state"]  = controller.getMinAmperageInState();

  info["controls_stand"]   = controller.controlsStand();
  info["controls_speaker"] = controller.controlsSpeaker();
  info["controls_tv_led"]  = controller.controlsTVLED();
  info["controls_gpio"]    = controller.controlsGPIO();

  info["on_threshold"]  = controller.onThreshold();
  info["off_threshold"] = controller.offThreshold();
  info["amp_gain"]      = controller.getAmperageGain();
}

void handleMessage(const JsonVariant& json)
{
  if(json.containsKey("amp_gain") && json["amp_gain"].is<float>())
  {
    misc_task_get_current().setAmperageGain(json["amp_gain"].as<float>());
  }
  if(json.containsKey("set_on_level") && json["set_on_level"].is<float>())
  {
    misc_task_get_current().setOnThreshold(json["set_on_level"].as<float>());
  }
  if(json.containsKey("set_off_level") && json["set_off_level"].is<float>())
  {
    misc_task_get_current().setOffThreshold(json["set_off_level"].as<float>());
  }
  if(json.containsKey("enable_action"))
  {
    auto actions = json["enable_action"];
    if(actions.containsKey("controls_speaker") && actions["controls_speaker"].is<bool>())
    {
      misc_task_get_current().setControlsSpeaker(actions["controls_speaker"].as<bool>());
    }
    if(actions.containsKey("controls_stand") && actions["controls_stand"].is<bool>())
    {
      misc_task_get_current().setControlsStand(actions["controls_stand"].as<bool>());
    }
    if(actions.containsKey("controls_tv_led") && actions["controls_tv_led"].is<bool>())
    {
      misc_task_get_current().setControlsTVLED(actions["controls_tv_led"].as<bool>());
    }
    if(actions.containsKey("controls_gpio") && actions["controls_gpio"].is<bool>())
    {
      misc_task_get_current().setControlsGPIO(actions["controls_gpio"].as<bool>());
    }
  }
}
}  // namespace Current