#pragma once

#include "CurrentController/CurrentController.hpp"
#include "LEDController/LEDController.hpp"
#include "LEDController/TVLEDController.hpp"
#include "OnOffDetector/IoOnOffDetector.hpp"

ContinuousLEDController& misc_task_get_led();
CurrentController&       misc_task_get_current();
IoOnOffDetector&         misc_task_get_io_controller();
GPIOController&          misc_task_get_gpio_controller();

TVLEDController& misc_task_get_tv_led();

void misc_task_init();