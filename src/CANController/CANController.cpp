#include "CANController.hpp"

#include <Arduino.h>

#include "esp_err.h"
#include "esp_log.h"

static const char TAG[] = "CANCONTROL";

CANController::CANController(gpio_num_t tx, gpio_num_t rx, gpio_num_t err, gpio_num_t stb, gpio_num_t enable)
  : err(err),
    stb(stb),
    enable(enable)
{
  twai_config                 = TWAI_TIMING_CONFIG_125KBITS();
  filter_config               = TWAI_FILTER_CONFIG_ACCEPT_ALL();
  general_config              = TWAI_GENERAL_CONFIG_DEFAULT(tx, rx, TWAI_MODE_NORMAL);
  general_config.rx_queue_len = 100;
  general_config.tx_queue_len = 100;

  ESP_ERROR_CHECK(twai_driver_install(&general_config, &twai_config, &filter_config));
  ESP_ERROR_CHECK(twai_start());

  gpio_pad_select_gpio(err);
  gpio_pad_select_gpio(stb);
  gpio_pad_select_gpio(enable);

  gpio_set_direction(err, GPIO_MODE_INPUT);
  gpio_set_direction(stb, GPIO_MODE_OUTPUT);
  gpio_set_direction(enable, GPIO_MODE_OUTPUT);

  gpio_set_level(stb, 1);
  gpio_set_level(enable, 1);
}

void CANController::registerCallback(CanMessageType msgType, std::function<void(const twai_message_t&)> callback)
{
  callbacks[static_cast<int>(msgType)] = callback;
}

void CANController::recieveMessages()
{
  twai_message_t msg;
  while(twai_receive(&msg, 0) != ESP_ERR_TIMEOUT)
  {
    // stats[msg.identifier] += 1;
    // recvCount++;
    // if(recvCount % 2000 == 0)
    // {
    //   print_stats();
    // }
    auto callback = msg.identifier < callbacks.size() ? callbacks[msg.identifier] : nullptr;
    if(callback == nullptr)
    {
      continue;
    }
    callback(msg);
  }
}

void CANController::print_stats(void)
{
  for(auto& stat : stats)
  {
    log_i("MSG: %d, CNT: %d", stat.first, stat.second);
  }
}

void CANController::sendMessage(twai_message_t& msg)
{
  if(twai_transmit(&msg, pdMS_TO_TICKS(0)) != ESP_OK)
  {
    twai_status_info_t twai_info;

    twai_get_status_info(&twai_info);
    if(twai_info.state == TWAI_STATE_BUS_OFF)
    {
      ESP_ERROR_CHECK(twai_initiate_recovery());
    }
    else if(twai_info.state == TWAI_STATE_STOPPED)
    {
      ESP_ERROR_CHECK(twai_start());
    }
  }
}

bool CANController::sendMessageWithSmallQueue(twai_message_t& msg, uint32_t max_queue_size)
{
  twai_status_info_t twai_info;
  twai_get_status_info(&twai_info);
  if(twai_info.msgs_to_tx <= max_queue_size)
  {
    sendMessage(msg);
    return true;
  }
  return false;
}