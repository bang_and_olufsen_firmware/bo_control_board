#pragma once

#include "driver/gpio.h"
#include "driver/ledc.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "nvs_flash.h"

class LEDController {
 public:
  LEDController(gpio_num_t led, ledc_channel_t ledc_channel, const char* nvs_namespace, bool invert = false);
  ~LEDController() = default;
  bool  step();
  float getMaxBrightness() const { return max_brightness; }
  float getMinBrightness() const { return min_brightness; }
  float getFadeTime() const { return fade_time_ms; }
  void  setMaxBrightness(float val);
  void  setMinBrightness(float val);
  bool  fadingOn();
  void  setOnPower(float val);

  bool         isFinished() const { return finished; }
  virtual void forceNewFade(bool fadeDirection, bool restartIfInProgress = true);

 protected:
  void              loadSettings();
  void              startFade(bool fadeDirection);
  nvs_handle_t      nvs_handle;
  ledc_channel_t    channel;
  SemaphoreHandle_t settingsSemaphore;
  bool              invert;

  float    fade_time_ms  = 500;
  uint64_t fadeStartTime = 0;

  float                     onPower = 1;
  float                     max_brightness;
  float                     min_brightness;
  static constexpr uint32_t MAX_DUTY         = ((1 << 11) - 1);
  int32_t                   fade_target_duty = 0;
  int32_t                   fade_start_duty  = 0;
  int32_t                   last_duty        = 0;

  bool faded_up = 0;
  bool finished = true;
};

class ContinuousLEDController : public LEDController {
 public:
  ContinuousLEDController(gpio_num_t led, ledc_channel_t ledc_channel, const char* nvs_namespace, bool invert = false)
    : LEDController(led, ledc_channel, nvs_namespace, invert)
  {
    loadSettings();
  }
  void setFadeTime(float time);

 private:
  void loadSettings();
};

class OneShotLEDController : public LEDController {
 public:
  using LEDController::LEDController;
  bool step();

 private:
};