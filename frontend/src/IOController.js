import React, { Component } from "react";
import Container from "@mui/material/Container";
import { InfoTable } from "./InfoTable";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Checkbox, TextField, FormControlLabel } from "@mui/material";
import { Button } from "@mui/material";
import ElectricalServicesIcon from "@mui/icons-material/ElectricalServices";
function createData(name, value) {
  return { name, value };
}

class IOOptions extends Component {
  constructor(props, context) {
    super();
    this.getControlsStand = this.getControlsStand.bind(this);
    this.getControlsSpeaker = this.getControlsSpeaker.bind(this);
    this.getControlsTVLED = this.getControlsTVLED.bind(this);
    this.getControlsGPIO = this.getControlsGPIO.bind(this);

    this.state = {
      controls_stand_set_time: new Date(),
      controls_speaker_set_time: new Date(),
      controls_tv_led_set_time: new Date(),
      controls_gpio_set_time: new Date(),
      gpio: 0,
    };
  }

  getControlsSpeaker() {
    if (
      Date.now() - this.state["controls_speaker_set_time"] < 2000 &&
      "controls_speaker" in this.state
    ) {
      return this.state.controls_speaker;
    }
    return this.props.state.controls_speaker;
  }

  getControlsGPIO() {
    if (
      Date.now() - this.state["controls_gpio_set_time"] < 2000 &&
      "controls_gpio" in this.state
    ) {
      return this.state.controls_gpio;
    }
    return this.props.state.controls_gpio;
  }


  getControlsStand() {
    if (
      Date.now() - this.state["controls_stand_set_time"] < 2000 &&
      "controls_stand" in this.state
    ) {
      return this.state.controls_stand;
    }
    return this.props.state.controls_stand;
  }

  getControlsTVLED() {
    if (
      Date.now() - this.state["controls_tv_led_set_time"] < 2000 &&
      "controls_tv_led" in this.state
    ) {
      return this.state.controls_tv_led;
    }
    return this.props.state.controls_tv_led;
  }

  render() {
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 200 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={{ borderBottom: "none" }}>Options</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow
              key="control_stand"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell
                component="th"
                scope="row"
                style={{ borderBottom: "none" }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.getControlsStand()}
                      inputProps={{ "aria-label": "controlled" }}
                      onChange={(event) => {
                        this.setState({
                          controls_stand: event.target.checked,
                          controls_stand_set_time: new Date(),
                        });
                        this.props.sendJsonMessage({
                          io: {
                            enable_action: {
                              controls_stand: event.target.checked,
                            },
                          },
                        });
                      }}
                    />
                  }
                  label="Control Stand"
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="control_speaker"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell
                component="th"
                scope="row"
                style={{ borderBottom: "none" }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.getControlsSpeaker()}
                      inputProps={{ "aria-label": "controlled" }}
                      onChange={(event) => {
                        this.setState({
                          controls_speaker: event.target.checked,
                          controls_speaker_set_time: new Date(),
                        });
                        this.props.sendJsonMessage({
                          io: {
                            enable_action: {
                              controls_speaker: event.target.checked,
                            },
                          },
                        });
                      }}
                    />
                  }
                  label="Control Speaker"
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="control_tv_led"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell
                component="th"
                scope="row"
                style={{ borderBottom: "none" }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.getControlsTVLED()}
                      inputProps={{ "aria-label": "controlled" }}
                      onChange={(event) => {
                        this.setState({
                          controls_tv_led: event.target.checked,
                          controls_tv_led_set_time: new Date(),
                        });
                        this.props.sendJsonMessage({
                          io: {
                            enable_action: {
                              controls_tv_led: event.target.checked,
                            },
                          },
                        });
                      }}
                    />
                  }
                  label="Control TV LED"
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="control_io_out"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell
                component="th"
                scope="row"
                style={{ borderBottom: "none" }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.getControlsGPIO()}
                      inputProps={{ "aria-label": "controlled" }}
                      onChange={(event) => {
                        this.setState({controls_gpio: event.target.checked,
                          controls_gpio_set_time : new Date()})
                        this.props.sendJsonMessage({
                          io: {
                            enable_action: {
                              controls_gpio: event.target.checked,
                            },
                          },
                        });
                      }}
                    />
                  }
                  label="Control IO Out"
                />
              </TableCell>
            </TableRow>

            <TableRow
              key="gpio"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.state.gpio}
                  type="number"
                  id="outlined-basic"
                  label="Set GPIO"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      gpio: parseFloat(event.target.value).toFixed(0),
                    });
                  }}
                />
              </TableCell>

              <TableCell align="left" style={{ borderBottom: "none" }}>
                <Button
                  variant="contained"
                  style="margin:5px"
                  onClick={() => {
                    this.props.sendJsonMessage({
                      io: {
                        set_pin: parseInt(this.state.gpio),
                      },
                    });
                  }}
                >
                  Apply
                </Button>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

class IOController extends Component {
  constructor(props, context) {
    super();
  }

  static getPageName() {
    return "IO";
  }

  static getPageIcon() {
    return <ElectricalServicesIcon />;
  }

  render() {
    const rows = [
      createData("PIN", this.props.state.pin),
      createData("State", this.props.state.state),
      createData(
        "Time in State",
        this.props.state.time_since_state.toFixed(1) + " s"
      ),
    ];

    return (
      <React.Fragment>
        <Container disableGutters={true} maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
          <InfoTable name="IO state" valuecolumn="Value" rows={rows} />
        </Container>
        <Container disableGutters={true} maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
          <IOOptions
            state={this.props.state}
            sendJsonMessage={this.props.sendJsonMessage}
          />
        </Container>
      </React.Fragment>
    );
  }
}

export default IOController;
