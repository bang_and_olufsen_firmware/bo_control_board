#pragma once

#include "CANBootloader/CANBootloader.hpp"
#include "CANController/CANController.hpp"
#include "Controller/Controller.hpp"
#include "common/SpeakerTypes.hpp"
#include "freertos/semphr.h"
#include "nvs_flash.h"

class SpeakerController : public Controller {
 public:
  SpeakerController(CANController& bus, CANBootloader& bootloader);
  ~SpeakerController() = default;
  void step() override;
  void reload() { shouldReload = true; }

  speakermotor::BOSpeakerControlTarget getWantedTarget() const { return setTarget; }
  speakermotor::BOSpeakerControlTarget getActualTarget() const { return recievedTarget; }
  speakermotor::BOSpeakerControlState  getState() const { return state; }
  float                                getTargetSpeed() { return targetSpeed; }
  float                                getRecievedSpeed() { return recievedSpeed; }
  uint16_t                             getRecievedPWMFrequency() { return pwmFrequencyRecieved; }
  uint16_t                             getTargetPWMFrequency() { return pwmFrequencyTarget; }

  void setTargetSpeed(float val);
  void setTargetPWMFrequency(uint16_t val);

  void setNewTarget(speakermotor::BOSpeakerControlTarget target) { setTarget = target; }

 private:
  void handleStatus(const twai_message_t& msg);
  void publishTarget();
  void loadSettings();

  nvs_handle_t      nvs_handle;
  SemaphoreHandle_t semaphore;

  speakermotor::BOSpeakerControlState  state                = speakermotor::BOSpeakerControlState::STOPPED;
  speakermotor::BOSpeakerControlTarget setTarget            = speakermotor::BOSpeakerControlTarget::STOP;
  speakermotor::BOSpeakerControlTarget recievedTarget       = speakermotor::BOSpeakerControlTarget::STOP;
  float                                targetSpeed          = 0;
  float                                recievedSpeed        = -1;
  int64_t                              last_broadcast_time  = 0;
  uint16_t                             pwmFrequencyTarget   = 30000;
  uint16_t                             pwmFrequencyRecieved = 0;
  static constexpr int64_t             TARGET_BROADCAST_INTERVAL_MICROSECONDS = 100000;
  bool                                 shouldReload                           = false;
};