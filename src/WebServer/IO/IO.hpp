#pragma once

#include "ArduinoJson.h"

namespace IO
{
void addIO(JsonVariant& json);
void handleMessage(const JsonVariant& json);
}  // namespace IO
