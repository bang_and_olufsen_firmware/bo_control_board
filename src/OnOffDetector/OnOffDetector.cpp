#include "OnOffDetector.hpp"

#include "utilities/nvs_utilities.hpp"

#define CONTROL_STAND_KEY_NAME   "control_stand"
#define CONTROL_SPEAKER_KEY_NAME "control_speaker"
#define CONTROL_TVLED_KEY_NAME   "control_tv_led"
#define CONTROL_GPIO_KEY_NAME    "control_gpio"

OnOffDetector::OnOffDetector(
  SpeakerController& speaker,
  StandController&   stand,
  TVLEDController&   tv_led,
  GPIOController&    gpio_controller,
  const char*        nvs_namespace)
  : speaker(speaker),
    stand(stand),
    tv_led(tv_led),
    gpio_controller(gpio_controller)
{
  settingsSemaphore = xSemaphoreCreateBinary();
  xSemaphoreGive(settingsSemaphore);

  esp_err_t err = nvs_open(nvs_namespace, NVS_READWRITE, &nvs_handle);
  if(err != ESP_OK)
  {
    log_w("Error opening NVS for current settings: %s", esp_err_to_name(err));
  }
  loadSettings();
}

void OnOffDetector::loadSettings()
{
  control_stand      = nvs_utilities::get_bool_setting(CONTROL_STAND_KEY_NAME, nvs_handle, false);
  control_speakerbar = nvs_utilities::get_bool_setting(CONTROL_SPEAKER_KEY_NAME, nvs_handle, false);
  control_tv_led     = nvs_utilities::get_bool_setting(CONTROL_TVLED_KEY_NAME, nvs_handle, false);
  control_gpio       = nvs_utilities::get_bool_setting(CONTROL_GPIO_KEY_NAME, nvs_handle, false);
}

void OnOffDetector::setControlsGPIO(bool val)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_bool_setting(CONTROL_GPIO_KEY_NAME, nvs_handle, val);
  control_gpio = val;
  xSemaphoreGive(settingsSemaphore);
}

void OnOffDetector::setControlsSpeaker(bool val)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_bool_setting(CONTROL_SPEAKER_KEY_NAME, nvs_handle, val);
  control_speakerbar = val;
  xSemaphoreGive(settingsSemaphore);
}

void OnOffDetector::setControlsStand(bool val)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_bool_setting(CONTROL_STAND_KEY_NAME, nvs_handle, val);
  control_stand = val;
  xSemaphoreGive(settingsSemaphore);
}

void OnOffDetector::setControlsTVLED(bool val)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_bool_setting(CONTROL_TVLED_KEY_NAME, nvs_handle, val);
  control_tv_led = val;
  xSemaphoreGive(settingsSemaphore);
}

void OnOffDetector::setState(OnOffDetector::LastCurrentState state)
{
  last_state_change_time = esp_timer_get_time();
  last_state             = state;
  stateChangeHook();
}

void OnOffDetector::handleOn()
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  if(last_state != LastCurrentState::ON)
  {
    if(control_speakerbar)
    {
      speaker.setNewTarget(speakermotor::BOSpeakerControlTarget::UNFOLD);
    }
    if(control_stand && last_state != LastCurrentState::UNDEFINED)
    {
      stand.goToTrajectoryEnd(stand.getDefaultTrajectory());
    }
    if(control_tv_led && last_state != LastCurrentState::UNDEFINED)
    {
      tv_led.gotoOn();
    }
    if(control_gpio && last_state != LastCurrentState::UNDEFINED)
    {
      gpio_controller.setState(GPIOController::GPIOControllerState::ON);
    }

    setState(LastCurrentState::ON);
  }
  xSemaphoreGive(settingsSemaphore);
}
void OnOffDetector::handleOff()
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  if(last_state != LastCurrentState::OFF)
  {
    if(control_speakerbar)
    {
      speaker.setNewTarget(speakermotor::BOSpeakerControlTarget::FOLD_IN);
    }
    if(control_stand && last_state != LastCurrentState::UNDEFINED)
    {
      stand.goToTrajectoryStart(stand.getDefaultTrajectory());
    }
    if(control_tv_led && last_state != LastCurrentState::UNDEFINED)
    {
      tv_led.gotoOff();
    }
    if(control_gpio && last_state != LastCurrentState::UNDEFINED)
    {
      gpio_controller.setState(GPIOController::GPIOControllerState::OFF);
    }

    setState(LastCurrentState::OFF);
  }
  xSemaphoreGive(settingsSemaphore);
}
