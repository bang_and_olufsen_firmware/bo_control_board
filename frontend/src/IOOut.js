import React, { Component } from "react";
import Container from "@mui/material/Container";
import { InfoTable } from "./InfoTable";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { TextField } from "@mui/material";
import { Button } from "@mui/material";
import OutputIcon from "@mui/icons-material/Output";
import InputAdornment from "@mui/material/InputAdornment";

class IOOutOptions extends Component {
  constructor(props, context) {
    super();
    this.getOffDelay = this.getOffDelay.bind(this);
    this.getOnDelay = this.getOnDelay.bind(this);

    this.state = {
      off_delay_set_time: new Date(),
      on_delay_set_time: new Date(),
    };
  }

  getOnDelay() {
    if (
      Date.now() - this.state["on_delay_set_time"] < 2000 &&
      "on_delay" in this.state
    ) {
      return this.state.on_delay;
    }
    return this.props.state.on_delay;
  }

  getOffDelay() {
    if (
      Date.now() - this.state["off_delay_set_time"] < 2000 &&
      "off_delay" in this.state
    ) {
      return this.state.off_delay;
    }
    return this.props.state.off_delay;
  }

  render() {
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 200 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={{ borderBottom: "none" }}>Options</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow
              key="off_delay"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getOffDelay()}
                  type="number"
                  id="outlined-basic"
                  label="Off Delay"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">ms</InputAdornment>
                    ),
                  }}
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      off_delay: parseFloat(event.target.value).toFixed(0),
                      off_delay_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      io_out: {
                        off_delay: Math.floor(parseFloat(event.target.value)),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="on_delay"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getOnDelay()}
                  type="number"
                  id="outlined-basic"
                  label="On Delay"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">ms</InputAdornment>
                    ),
                  }}
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({
                      on_delay: parseFloat(event.target.value).toFixed(0),
                      on_delay_set_time: new Date(),
                    });
                    this.props.sendJsonMessage({
                      io_out: {
                        on_delay: Math.floor(parseFloat(event.target.value)),
                      },
                    });
                  }}
                />
              </TableCell>
            </TableRow>

            <TableRow
              key="Turn on"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <Button
                  variant="contained"
                  style="margin:5px"
                  onClick={() => {
                    this.props.sendJsonMessage({
                      io_out: {
                        set_state: true,
                      },
                    });
                  }}
                >
                  ON
                </Button>
              </TableCell>
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <Button
                  variant="contained"
                  style="margin:5px"
                  onClick={() => {
                    this.props.sendJsonMessage({
                      io_out: {
                        set_state: false,
                      },
                    });
                  }}
                >
                  OFF
                </Button>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

class IOOut extends Component {
  constructor(props, context) {
    super();
  }

  static getPageName() {
    return "IO Output";
  }

  static getPageIcon() {
    return <OutputIcon />;
  }

  render() {
    return (
      <React.Fragment>
        <Container disableGutters={true} maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
          <IOOutOptions
            state={this.props.state}
            sendJsonMessage={this.props.sendJsonMessage}
          />
        </Container>
      </React.Fragment>
    );
  }
}

export default IOOut;
