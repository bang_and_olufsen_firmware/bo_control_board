
export default function getDeviceIP() {
    if (process.env.NODE_ENV && process.env.NODE_ENV === 'development') {
        return "192.168.203.108"
    }
    return window.location.hostname;
}