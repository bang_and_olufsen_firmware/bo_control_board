import React, { Component } from "react";
import DashboardIcon from "@mui/icons-material/Dashboard";
import { Grid } from "@mui/material";
import { Container } from "@mui/system";
import Typography from "@mui/material/Typography";
import { Paper } from "@mui/material";
import { Button } from "@mui/material";
class Dashboard extends Component {
  constructor(props, context) {
    super();
    this.sendStopSpeaker = this.sendStopSpeaker.bind(this);
    this.sendStopStand = this.sendStopStand.bind(this);
    this.sendFold = this.sendFold.bind(this);
    this.sendUnfold = this.sendUnfold.bind(this);
    this.sendMoveToStart = this.sendMoveToStart.bind(this);
    this.sendMoveToEnd = this.sendMoveToEnd.bind(this);
  }

  sendStopSpeaker() {
    this.props.sendJsonMessage({ device: { speaker: { target: "stop" } } });
  }

  sendStopStand() {
    this.props.sendJsonMessage({
      device: {
        stand: {
          stop_movement: true,
        },
      },
    });
  }

  sendFold() {
    this.props.sendJsonMessage({ device: { speaker: { target: "fold_in" } } });
  }

  sendUnfold() {
    this.props.sendJsonMessage({ device: { speaker: { target: "unfold" } } });
  }

  sendMoveToStart(trajectoryName) {
    this.props.sendJsonMessage({
      device: { stand: { move_to_trajectory_start: trajectoryName } },
    });
  }

  sendMoveToEnd(trajectoryName) {
    this.props.sendJsonMessage({
      device: { stand: { move_to_trajectory_end: trajectoryName } },
    });
  }

  static getPageName() {
    return "Dashboard";
  }

  static getPageIcon() {
    return <DashboardIcon />;
  }

  render() {
    const default_trajectory =
      this.props.state.device.stand.trajectories.default;
    let trajectories = [];
    trajectories = trajectories.concat([default_trajectory]);
    for (const [name, values] of Object.entries(
      this.props.state.device.stand.trajectories.trajectory_list
    )) {
      if (name != default_trajectory) {
        trajectories = trajectories.concat([name]);
      }
    }

    return (
      <Container disableGutters={true} maxWidth="lg" sx={{ mt: 10, mb: 4 }}>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <Paper
              sx={{
                p: 2,
                display: "flex",
                flexDirection: "column",
              }}
            >
              <Typography align="center" variant="h6" noWrap component="div">
                Stand
              </Typography>
              <Button
                variant="contained"
                style="margin:5px"
                onClick={() => {
                  this.sendStopStand();
                }}
              >
                Stop
              </Button>
              <Button
                variant="contained"
                style="margin:5px"
                onClick={() => {
                  this.sendMoveToStart(default_trajectory);
                }}
              >
                Move to start
              </Button>
              {trajectories.map((name) => (
                <Button
                  variant="contained"
                  style="margin:5px"
                  onClick={() => {
                    this.sendMoveToEnd(name);
                  }}
                >
                  Move to {name}
                </Button>
              ))}
            </Paper>
          </Grid>
          <Grid item xs={6}>
            <Paper
              sx={{
                p: 2,
                display: "flex",
                flexDirection: "column",
              }}
            >
              <Typography align="center" variant="h6" noWrap component="div">
                Speaker Bar
              </Typography>
              <Button
                variant="contained"
                style="margin:5px"
                onClick={() => {
                  this.sendStopSpeaker();
                }}
              >
                Stop
              </Button>
              <Button
                variant="contained"
                style="margin:5px"
                onClick={() => {
                  this.sendUnfold();
                }}
              >
                Unfold
              </Button>
              <Button
                variant="contained"
                style="margin:5px"
                onClick={() => {
                  this.sendFold();
                }}
              >
                Fold
              </Button>
            </Paper>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

export default Dashboard;
