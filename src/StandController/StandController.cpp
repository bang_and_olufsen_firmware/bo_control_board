#include "StandController.hpp"

#include <CRC32.h>
#include <string.h>

#include "StringUtils.hpp"
#include "common/CANMessages.hpp"
#include "endian.h"
#include "esp_timer.h"

#define DEFAULT_TRAJECTORY_KEYNAME "KEY_DEFAULT_TRJ"
#define NVS_NAMESPACE              "stand_tgts_v3"

StandController::StandController(CANController& bus, CANBootloader& bootloader)
  : Controller(
    bus,
    bootloader,
    CanMessageType::STAND_DEVICE_STATS_HEAP,
    CanMessageType::STAND_DEVICE_STATS_UPTIME,
    CanMessageType::STAND_DEVICE_STATS_LOAD),
    queueTrajectory(xQueueCreate(10, sizeof(Trajectory)))

{
  bus.registerCallback(CanMessageType::STAND_CONTROL_STATUS_ENCODERS, [this](const twai_message_t& msg) {
    this->handleEncoderStatus(msg);
  });

  bus.registerCallback(CanMessageType::STAND_CONTROL_STATUS_MOTORS, [this](const twai_message_t& msg) {
    this->handleMotorStatus(msg);
  });
  bus.registerCallback(CanMessageType::STAND_CONTROL_STATUS_TARGET, [this](const twai_message_t& msg) {
    this->handleTargets(msg);
  });
  bus.registerCallback(CanMessageType::STAND_CONTROL_STATUS_NEEDED_SPEED, [this](const twai_message_t& msg) {
    this->handleNeededSpeeds(msg);
  });

  bus.registerCallback(CanMessageType::STAND_CONTROL_STATUS_ENCODER_SPEED, [this](const twai_message_t& msg) {
    this->handleEncoderSpeeds(msg);
  });

  bus.registerCallback(CanMessageType::STAND_CONTROL_STATUS_MOTOR_FEEDBACK_SPEED, [this](const twai_message_t& msg) {
    this->handleMotorFeedbackSpeeds(msg);
  });
  bus.registerCallback(CanMessageType::STAND_CONTROL_STATUS_MOTOR_FEEDBACK_TICKS, [this](const twai_message_t& msg) {
    this->handleMotorFeedbackTicks(msg);
  });
  bus.registerCallback(CanMessageType::STAND_CONTROL_TRAJECTORY_STATUS, [this](const twai_message_t& msg) {
    this->handleTrajectoryStatus(msg);
  });
  bus.registerCallback(CanMessageType::STAND_DEBUG_CHANNEL_FLOAT1, [this](const twai_message_t& msg) {
    this->handleFloatDebug1(msg);
  });

  bus.registerCallback(CanMessageType::STAND_DEBUG_CHANNEL_FLOAT2, [this](const twai_message_t& msg) {
    this->handleFloatDebug2(msg);
  });

  bus.registerCallback(CanMessageType::STAND_DEBUG_CHANNEL_UINT32_1, [this](const twai_message_t& msg) {
    this->handleUint32Debug1(msg);
  });

  bus.registerCallback(CanMessageType::STAND_DEBUG_CHANNEL_UINT32_2, [this](const twai_message_t& msg) {
    this->handleUint32Debug2(msg);
  });

  trajectorySemaphore = xSemaphoreCreateBinary();
  xSemaphoreGive(trajectorySemaphore);

  esp_err_t err = nvs_open(NVS_NAMESPACE, NVS_READWRITE, &trajectory_handle);
  if(err != ESP_OK)
  {
    log_w("Error opening NVS for stand trajectories: %s", esp_err_to_name(err));
  }
  loadTrajectories();
}

void StandController::loadTrajectories()
{
  xSemaphoreTake(trajectorySemaphore, portMAX_DELAY);

  // Populate saved trajectories
  nvs_iterator_t   it = nvs_entry_find("nvs", NVS_NAMESPACE, NVS_TYPE_BLOB);
  nvs_entry_info_t info;
  while(it)
  {
    nvs_entry_info(it, &info);
    Trajectory traj;
    size_t     length = sizeof(traj);
    nvs_get_blob(trajectory_handle, info.key, &traj, &length);
    std::string trajectory_name = info.key;
    StringUtils::trim(trajectory_name);
    trajectories[trajectory_name] = traj;
    it                            = nvs_entry_next(it);
  }
  nvs_release_iterator(it);

  // Populate default trajectory
  char   default_trajectory_name[1024]  = {0};
  size_t default_trajectory_name_length = sizeof(default_trajectory_name);
  bool   default_found                  = false;
  if(ESP_OK == nvs_get_str(trajectory_handle, DEFAULT_TRAJECTORY_KEYNAME, default_trajectory_name, &default_trajectory_name_length))
  {
    default_found      = true;
    default_trajectory = std::string(default_trajectory_name);
    StringUtils::trim(default_trajectory);
  }

  xSemaphoreGive(trajectorySemaphore);

  if(!default_found)
  {
    log_w("No default trajectory! Creating one");
    setTrajectory(Trajectory(), "Default", true);
  }
}

std::unordered_map<std::string, Trajectory> StandController::getTrajectories()
{
  xSemaphoreTake(trajectorySemaphore, portMAX_DELAY);
  auto _trajectories = trajectories;
  xSemaphoreGive(trajectorySemaphore);
  return _trajectories;
}

std::pair<Trajectory, bool> StandController::getTrajectory(const std::string& name)
{
  xSemaphoreTake(trajectorySemaphore, portMAX_DELAY);
  auto       it = trajectories.find(name);
  Trajectory traj;
  bool       valid = false;

  if(it != trajectories.end())
  {
    traj  = it->second;
    valid = true;
  }
  xSemaphoreGive(trajectorySemaphore);

  return {traj, valid};
}

void StandController::goToTrajectoryEnd(const std::string& name)
{
  auto traj = getTrajectory(name);
  if(!traj.second)
  {
    return;
  }
  traj.first.setWaypointCount();
  if(traj.first.waypoint_cnt < 2)
  {
    return;
  }

  setTrajectory(traj.first);
}

void StandController::goToWaypoint(const Waypoint w)
{
  Trajectory traj;
  traj.movement_time = 15000;
  traj.waypoints[0]  = w;
  traj.setWaypointCount();
  setTrajectory(traj);
}

void StandController::goToTrajectoryStart(const std::string& name)
{
  auto traj = getTrajectory(name);
  if(!traj.second)
  {
    return;
  }
  traj.first.setWaypointCount();

  if(traj.first.waypoint_cnt < 2)
  {
    return;
  }
  traj.first.Reverse();

  setTrajectory(traj.first);
}

void StandController::stopMovement()
{
  setTrajectory(Trajectory());
}

std::string StandController::getDefaultTrajectory()
{
  xSemaphoreTake(trajectorySemaphore, portMAX_DELAY);
  auto trajectory_name = default_trajectory;
  xSemaphoreGive(trajectorySemaphore);
  return trajectory_name;
}

void StandController::setTrajectory(Trajectory traj, const std::string& name, bool is_default)
{
  xSemaphoreTake(trajectorySemaphore, portMAX_DELAY);
  esp_err_t err = nvs_set_blob(trajectory_handle, name.c_str(), &traj, sizeof(traj));
  if(err != ESP_OK)
  {
    log_w("Error saving trajectory %s: %s", name.c_str(), esp_err_to_name(err));
    return;
  }
  trajectories[name] = traj;

  if(is_default)
  {
    err = nvs_set_str(trajectory_handle, DEFAULT_TRAJECTORY_KEYNAME, name.c_str());
    if(err != ESP_OK)
    {
      log_w("Error setting default trajectory %s: %s", name.c_str(), esp_err_to_name(err));
      return;
    }
    default_trajectory = name;
  }
  xSemaphoreGive(trajectorySemaphore);
}

void StandController::deleteTrajectory(const std::string& name)
{
  xSemaphoreTake(trajectorySemaphore, portMAX_DELAY);
  if(name != default_trajectory)
  {
    auto it = trajectories.find(name);
    if(it != trajectories.end())
    {
      trajectories.erase(it);
      nvs_erase_key(trajectory_handle, name.c_str());
    }
  }

  xSemaphoreGive(trajectorySemaphore);
}

void StandController::handleEncoderStatus(const twai_message_t& msg)
{
  motor1.position = *(reinterpret_cast<const uint16_t*>(msg.data + 0));
  motor2.position = *(reinterpret_cast<const uint16_t*>(msg.data + 2));

  last_status_time = esp_timer_get_time();
}

void StandController::handleEncoderSpeeds(const twai_message_t& msg)
{
  motor1.encoder_speed = *(reinterpret_cast<const float*>(msg.data + 0));
  motor2.encoder_speed = *(reinterpret_cast<const float*>(msg.data + 4));

  last_status_time = esp_timer_get_time();
}

void StandController::handleNeededSpeeds(const twai_message_t& msg)
{
  motor1.needed_speed = *(reinterpret_cast<const float*>(msg.data + 0));
  motor2.needed_speed = *(reinterpret_cast<const float*>(msg.data + 4));

  last_status_time = esp_timer_get_time();
}

void StandController::handleTrajectoryStatus(const twai_message_t& msg)
{
  trajectory_state.acked_crc       = *(reinterpret_cast<const uint32_t*>(msg.data + 0));
  trajectory_state.acked_waypoints = *(reinterpret_cast<const uint8_t*>(msg.data + 4));
  trajectory_state.m1_waypoint     = *(reinterpret_cast<const uint8_t*>(msg.data + 5));
  trajectory_state.m2_waypoint     = *(reinterpret_cast<const uint8_t*>(msg.data + 6));
}

void StandController::handleMotorFeedbackSpeeds(const twai_message_t& msg)
{
  motor1.motor_feedback_speed = *(reinterpret_cast<const float*>(msg.data + 0));
  motor2.motor_feedback_speed = *(reinterpret_cast<const float*>(msg.data + 4));

  last_status_time = esp_timer_get_time();
}

void StandController::handleMotorFeedbackTicks(const twai_message_t& msg)
{
  motor1.motor_feedback_ticks = *(reinterpret_cast<const uint32_t*>(msg.data + 0));
  motor2.motor_feedback_ticks = *(reinterpret_cast<const uint32_t*>(msg.data + 4));

  last_status_time = esp_timer_get_time();
}

void StandController::handleFloatDebug1(const twai_message_t& msg)
{
  debug_float[0] = *(reinterpret_cast<const float*>(msg.data + 0));
  debug_float[1] = *(reinterpret_cast<const float*>(msg.data + 4));
}
void StandController::handleFloatDebug2(const twai_message_t& msg)
{
  debug_float[2] = *(reinterpret_cast<const float*>(msg.data + 0));
  debug_float[3] = *(reinterpret_cast<const float*>(msg.data + 4));
}

void StandController::handleUint32Debug1(const twai_message_t& msg)
{
  debug_uint32[0] = *(reinterpret_cast<const uint32_t*>(msg.data + 0));
  debug_uint32[1] = *(reinterpret_cast<const uint32_t*>(msg.data + 4));
}
void StandController::handleUint32Debug2(const twai_message_t& msg)
{
  debug_uint32[2] = *(reinterpret_cast<const uint32_t*>(msg.data + 0));
  debug_uint32[3] = *(reinterpret_cast<const uint32_t*>(msg.data + 4));
}

void StandController::handleMotorStatus(const twai_message_t& msg)
{
  motor1.speed = *(reinterpret_cast<const float*>(msg.data + 0));
  motor2.speed = *(reinterpret_cast<const float*>(msg.data + 4));

  last_status_time = esp_timer_get_time();
}

void StandController::handleTargets(const twai_message_t& msg)
{
  motor1.target = *(reinterpret_cast<const uint32_t*>(msg.data + 0));
  motor2.target = *(reinterpret_cast<const uint32_t*>(msg.data + 4));

  last_status_time = esp_timer_get_time();
}

void StandController::setTrajectory(Trajectory traj)
{
  xQueueSend(queueTrajectory, (void*)&traj, (TickType_t)0);
}

void StandController::sendWaypoint(Waypoint* w, uint8_t index, uint8_t cnt)
{
  twai_message_t message;
  memset(&message, 0, sizeof(message));
  *reinterpret_cast<uint16_t*>(message.data + 0) = (w[0].m1_pos & 0xFFF) + (((uint32_t)(index & 0xF0)) << 8);
  *reinterpret_cast<uint16_t*>(message.data + 2) = (w[0].m2_pos & 0xFFF) + (((uint32_t)(index & 0x0F)) << 12);
  message.data_length_code                       = 4;

  if(cnt > 1)
  {
    *reinterpret_cast<uint16_t*>(message.data + 4) = (w[1].m1_pos & 0xFFF) + (((uint32_t)((index + 1) & 0xF0)) << 8);
    *reinterpret_cast<uint16_t*>(message.data + 6) = (w[1].m2_pos & 0xFFF) + (((uint32_t)((index + 1) & 0x0F)) << 12);
    message.data_length_code                       = 8;
  }

  message.identifier = static_cast<uint32_t>(CanMessageType::STAND_CONTROL_WAYPOINT);
  bus.sendMessage(message);
}

void StandController::sendReset()
{
  twai_message_t message;
  memset(&message, 0, sizeof(message));
  message.identifier       = static_cast<uint32_t>(CanMessageType::STAND_CONTROL_STOP_MOVEMENT);
  message.data_length_code = 0;
  bus.sendMessage(message);
}

void StandController::sendStart(uint8_t waypoints, uint32_t movement_time, uint32_t crc)
{
  twai_message_t message;
  memset(&message, 0, sizeof(message));
  *reinterpret_cast<uint32_t*>(message.data + 0) = movement_time;
  message.data[4]                                = waypoints;
  message.data[5]                                = (crc >> 16) & 0xFF;
  message.data[6]                                = (crc >> 8) & 0xFF;
  message.data[7]                                = (crc >> 0) & 0xFF;

  message.identifier       = static_cast<uint32_t>(CanMessageType::STAND_CONTROL_START_MOVEMENT);
  message.data_length_code = 8;
  bus.sendMessage(message);
}

uint32_t StandController::computeTrajectoryCRC(Waypoint* waypoints, uint8_t cnt)
{
  CRC32 crc;
  for(uint8_t i = 0; i < cnt; i++)
  {
    crc.update(waypoints[i].m1_pos);
    crc.update(waypoints[i].m2_pos);
  }
  return crc.finalize();
}

void StandController::trajectoryStateStep()
{
  switch(trajectory_state.state)
  {
    case TrajectoryState::State::IDLE:
      break;
    case TrajectoryState::State::STOPPING:
      sendReset();
      if(trajectory_state.acked_waypoints == 0 && trajectory_state.m1_waypoint == 0xFF && trajectory_state.m2_waypoint == 0xFF)
      {
        if(trajectory_state.traj.waypoint_cnt == 0)
        {
          trajectory_state.state = TrajectoryState::State::IDLE;
        }
        else
        {
          trajectory_state.state = TrajectoryState::State::WAYPOINTS;
        }
      }
      break;
    case TrajectoryState::State::WAYPOINTS: {
      if(trajectory_state.acked_waypoints < trajectory_state.traj.waypoint_cnt)
      {
        const uint8_t waypoints_to_send = trajectory_state.traj.waypoint_cnt - trajectory_state.acked_waypoints;
        sendWaypoint(
          trajectory_state.traj.waypoints.data() + trajectory_state.acked_waypoints,
          trajectory_state.acked_waypoints,
          waypoints_to_send);
      }

      const uint32_t crc =
        computeTrajectoryCRC(trajectory_state.traj.waypoints.data(), trajectory_state.traj.waypoint_cnt);
      if(trajectory_state.acked_waypoints == trajectory_state.traj.waypoint_cnt && trajectory_state.acked_crc != crc)
      {
        trajectory_state.acked_waypoints = 0;
      }
      if(trajectory_state.acked_waypoints == trajectory_state.traj.waypoint_cnt && trajectory_state.acked_crc == crc)
      {
        trajectory_state.state = TrajectoryState::State::STARTING;
      }
      break;
    }
    case TrajectoryState::State::STARTING:
      const uint32_t crc =
        computeTrajectoryCRC(trajectory_state.traj.waypoints.data(), trajectory_state.traj.waypoint_cnt);

      sendStart(trajectory_state.traj.waypoint_cnt, trajectory_state.traj.movement_time, crc);

      if(trajectory_state.m1_waypoint != 0xFF && trajectory_state.m2_waypoint != 0xFF)
      {
        trajectory_state.state = TrajectoryState::State::IDLE;
      }
      break;
  }
}

void StandController::step()
{
  if(shouldReload)
  {
    bootloader.reloadFile();
    shouldReload = false;
  }

  bootloader.step();

  Trajectory trajectory;
  while(xQueueReceive(queueTrajectory, &trajectory, (TickType_t)0))
  {
    trajectory_state.traj            = trajectory;
    trajectory_state.acked_crc       = 0xFFFFFFFF;
    trajectory_state.acked_waypoints = 0xFF;
    trajectory_state.state           = TrajectoryState::State::STOPPING;
    trajectory_state.traj.setWaypointCount();
  }

  trajectoryStateStep();
}