#include "IoOnOffDetector.hpp"

#include "utilities/nvs_utilities.hpp"

#define IO_KEYNAME "io_pin"

IoOnOffDetector::IoOnOffDetector(
  SpeakerController& speaker,
  StandController&   stand,
  TVLEDController&   tv_led,
  GPIOController&    gpio_controller)
  : OnOffDetector(speaker, stand, tv_led, gpio_controller, "io_settings")
{
  loadSubSettings();

  pinMode(input_pin, INPUT_PULLDOWN);
}

void IoOnOffDetector::loadSubSettings()
{
  input_pin = (gpio_num_t)nvs_utilities::get_uint32_setting(IO_KEYNAME, nvs_handle, (uint32_t)GPIO_NUM_16);
}

void IoOnOffDetector::setInputPin(gpio_num_t pin)
{
  if(pin < GPIO_NUM_0 || pin >= GPIO_NUM_MAX)
  {
    return;
  }
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_uint32_setting(IO_KEYNAME, nvs_handle, (uint32_t)pin);
  input_pin = pin;
  pinMode(input_pin, INPUT_PULLDOWN);
  xSemaphoreGive(settingsSemaphore);
}

void IoOnOffDetector::step()
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  bool state = digitalRead(input_pin);
  xSemaphoreGive(settingsSemaphore);

  if(state != last_pin_state)
  {
    last_bounce_time = esp_timer_get_time();
    last_pin_state   = state;
  }
  else if(esp_timer_get_time() - last_bounce_time > 50000)
  {
    if(state)
    {
      handleOn();
    }
    else
    {
      handleOff();
    }
  }
}
