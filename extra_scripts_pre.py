import subprocess
import sys
import os
import os.path

Import('env')

platform = env.PioPlatform()

def get_dir_size(dir_path):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(dir_path):
        for file in filenames:
            file_path = os.path.join(dirpath, file)
            if not os.path.islink(file_path):
                total_size += os.path.getsize(file_path)
    return total_size

def get_file_count(dir_path):
    count = 0
    for _, _, files in os.walk(dir_path):
        count += len(files)
    return count

def build_frontend():
    print("Building frontend")
    p = subprocess.Popen("npm install".split(), cwd = "frontend")
    p.wait()
    if(p.returncode):
        print("'npm install' failed, do you have npm installed?")
        sys.exit(-1)
    p = subprocess.Popen("npm run build".split(), cwd = "frontend")
    p.wait()
    if(p.returncode):
        print("'npm run build' failed, do you have npm installed?")
        sys.exit(-1)
    
    build_size = get_dir_size("frontend/build")
    file_count = get_file_count("frontend/build")
    print(f"Frontend raw size is {build_size} with {file_count} files")
    image_size = build_size + file_count * 4096
    align = 0x1000
    if image_size % align != 0:
        image_size += align - (image_size % align)

    print(f"Packaging frontend in {image_size} byte littlefs image")
    p = subprocess.Popen([f"{platform.get_package_dir('tool-mkfatfs')}/mkfatfs -c frontend/build -t littlefs -s {image_size} website.bin"], stderr=subprocess.PIPE, shell=True)
    p.wait()



if env.GetProjectOption("custom_skip_frontend") != 'y':
    build_frontend()
