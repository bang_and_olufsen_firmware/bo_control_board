#include "Stand.hpp"

#include <math.h>

#include "Common.hpp"
#include "StringUtils.hpp"
#include "WebServer/WebServerUtils.hpp"
#include "tasks/can_task/can_task.hpp"
#include <iostream>

namespace Device
{

static std::unordered_map<std::string, Trajectory> cached_trajectories;

void updateStandTrajectoryCache()
{
  auto& stand         = can_task_get_stand();
  cached_trajectories = stand.getTrajectories();
}

void addTrajectories(JsonVariant& json)
{
  auto& stand     = can_task_get_stand();
  json["default"] = stand.getDefaultTrajectory();

  JsonVariant trajectoryList = WebServerUtils::getOrCreateObject(json, "trajectory_list");
  for(const auto& trajectory : cached_trajectories)
  {
    JsonObject t = WebServerUtils::getOrCreateObject(trajectoryList, trajectory.first.c_str());
    trajectory.second.SerializeToJson(t);
  }
}

void addStandStatus(JsonVariant& json)
{
  auto& stand = can_task_get_stand();

  JsonVariant device   = WebServerUtils::getOrCreateObject(json, "device");
  JsonVariant standObj = WebServerUtils::getOrCreateObject(device, "stand");

  JsonVariant status = WebServerUtils::getOrCreateObject(standObj, "status");
  JsonArray   motors = status.createNestedArray("motors");

  JsonObject motor1           = motors.createNestedObject();
  motor1["speed"]             = stand.getMotor1Speed();
  motor1["encoder_speed"]     = stand.getEncoder1Speed();
  motor1["encoder"]           = (stand.getEncoder1Position() / 4095.f) * 360;
  motor1["last_needed_speed"] = stand.getMotor1NeededSpeed();
  motor1["feedback_speed"]    = stand.getMotor1FeedbackSpeed();
  motor1["feedback_ticks"]    = stand.getMotor1FeedbackTicks();

  motor1["target"] = (stand.getMotor1Target() / 4095.f) * 360;

  JsonObject motor2       = motors.createNestedObject();
  motor2["speed"]         = stand.getMotor2Speed();
  motor2["encoder_speed"] = stand.getEncoder2Speed();

  motor2["encoder"]           = (stand.getEncoder2Position() / 4095.f) * 360;
  motor2["target"]            = (stand.getMotor2Target() / 4095.f) * 360;
  motor2["last_needed_speed"] = stand.getMotor2NeededSpeed();
  motor2["feedback_speed"]    = stand.getMotor2FeedbackSpeed();
  motor2["feedback_ticks"]    = stand.getMotor2FeedbackTicks();

  status["time_since_update"] = stand.getTimeSinceLastStatus() * 0.000001;

  JsonArray debug_u32   = status.createNestedArray("debug_u32");
  JsonArray debug_float = status.createNestedArray("debug_float");

  for(uint32_t i = 0; i < 4; i++)
  {
    debug_u32[i]   = stand.getUint32Debug(i);
    debug_float[i] = stand.getFloatDebug(i);
  }

  JsonVariant bootstatus = WebServerUtils::getOrCreateObject(standObj, "bootstatus");
  addBootloaderStatus(bootstatus, stand.getBootloader());
  JsonVariant stats = WebServerUtils::getOrCreateObject(standObj, "stats");
  addStats(stats, stand);

  JsonVariant trajectories = WebServerUtils::getOrCreateObject(standObj, "trajectories");
  addTrajectories(trajectories);
}

void handleStandMessage(const JsonVariant& json)
{
  if(json.containsKey("action"))
  {
    if(json["action"].as<const char*>() == std::string("reload"))
    {
      can_task_get_stand().reload();
    }
  }
  if(json.containsKey("move_to_trajectory_start"))
  {
    can_task_get_stand().goToTrajectoryStart(json["move_to_trajectory_start"].as<const char*>());
  }

  if(json.containsKey("move_to_waypoint"))
  {
    const JsonVariant& waypoint = json["move_to_waypoint"];
    if(waypoint.containsKey("m1") && waypoint["m1"].is<float>() && waypoint.containsKey("m2") && waypoint["m2"].is<float>())

      can_task_get_stand().goToWaypoint(
        Waypoint(waypoint["m1"].as<float>() * 4095.f / 360.f, waypoint["m2"].as<float>() * 4095.f / 360.f));
  }
  if(json.containsKey("stop_movement"))
  {
    can_task_get_stand().stopMovement();
  }
  if(json.containsKey("move_to_trajectory_end"))
  {
    can_task_get_stand().goToTrajectoryEnd(json["move_to_trajectory_end"].as<const char*>());
  }
  if(json.containsKey("set_default_trajectory"))
  {
    std::string trajectory_name = json["set_default_trajectory"].as<const char*>();
    trajectory_name.resize(15);
    StringUtils::trim(trajectory_name);
    log_i("Setting new default stand trajectory %s", trajectory_name.c_str());
    auto traj = can_task_get_stand().getTrajectory(trajectory_name);
    if(traj.second)
    {
      can_task_get_stand().setTrajectory(traj.first, trajectory_name, true);
    }
    else
    {
      log_w("Trajectory %s not found", trajectory_name.c_str());
    }
  }
  if(json.containsKey("create_trajectory"))
  {
    JsonVariant new_trajectory = json["create_trajectory"];
    if(!Trajectory::validateJson(new_trajectory))
    {
      return;
    }
    std::string trajectory_name = new_trajectory["name"].as<const char*>();
    trajectory_name.resize(15);
    StringUtils::trim(trajectory_name);

    can_task_get_stand().setTrajectory(Trajectory::fromJson(new_trajectory), trajectory_name, false);
  }

  if(json.containsKey("delete_trajectory"))
  {
    std::string trajectory_name = json["delete_trajectory"].as<const char*>();
    trajectory_name.resize(15);
    StringUtils::trim(trajectory_name);

    can_task_get_stand().deleteTrajectory(trajectory_name);
  }
}

}  // namespace Device