#include "SpeakerController.hpp"

#include <string.h>

#include "common/CANMessages.hpp"
#include "esp_timer.h"
#include "utilities/nvs_utilities.hpp"

#define SPEED_KEY_NAME         "speed"
#define PWM_FREQUENCY_KEY_NAME "pwm"

#define NVS_NAMESPACE "speaker"

SpeakerController::SpeakerController(CANController& bus, CANBootloader& bootloader)
  : Controller(
    bus,
    bootloader,
    CanMessageType::SPEAKER_DEVICE_STATS_HEAP,
    CanMessageType::SPEAKER_DEVICE_STATS_UPTIME,
    CanMessageType::SPEAKER_DEVICE_STATS_LOAD)
{
  bus.registerCallback(CanMessageType::SPEAKER_CONTROL_STATUS, [this](const twai_message_t& msg) {
    this->handleStatus(msg);
  });

  vSemaphoreCreateBinary(semaphore);
  esp_err_t err = nvs_open(NVS_NAMESPACE, NVS_READWRITE, &nvs_handle);
  if(err != ESP_OK)
  {
    log_w("Error opening NVS for speaker settings: %s", esp_err_to_name(err));
  }
  loadSettings();
}

void SpeakerController::loadSettings()
{
  targetSpeed        = nvs_utilities::get_float_setting(SPEED_KEY_NAME, nvs_handle, 50);
  pwmFrequencyTarget = nvs_utilities::get_uint32_setting(PWM_FREQUENCY_KEY_NAME, nvs_handle, 30000);
}

void SpeakerController::setTargetSpeed(float val)
{
  val = std::max(val, 0.f);
  val = std::min(val, 100.f);

  xSemaphoreTake(semaphore, portMAX_DELAY);
  nvs_utilities::set_float_setting(SPEED_KEY_NAME, nvs_handle, val);
  targetSpeed = val;
  xSemaphoreGive(semaphore);
}

void SpeakerController::setTargetPWMFrequency(uint16_t val)
{
  val = std::max(val, (uint16_t)50U);
  val = std::min(val, (uint16_t)65000U);

  xSemaphoreTake(semaphore, portMAX_DELAY);
  nvs_utilities::set_uint32_setting(PWM_FREQUENCY_KEY_NAME, nvs_handle, val);
  pwmFrequencyTarget = val;
  xSemaphoreGive(semaphore);
}

void SpeakerController::handleStatus(const twai_message_t& msg)
{
  state          = static_cast<speakermotor::BOSpeakerControlState>(msg.data[0]);
  recievedTarget = static_cast<speakermotor::BOSpeakerControlTarget>(msg.data[1]);
  if(msg.data_length_code >= 6)
  {
    recievedSpeed = *reinterpret_cast<const float*>(msg.data + 2);
  }
  if(msg.data_length_code >= 8)
  {
    pwmFrequencyRecieved = *reinterpret_cast<const uint16_t*>(msg.data + 6);
  }

  last_status_time = esp_timer_get_time();
}

void SpeakerController::publishTarget()
{
  twai_message_t message;
  memset(&message, 0, sizeof(message));
  message.data[0]                                = static_cast<uint8_t>(setTarget);
  *reinterpret_cast<float*>(message.data + 1)    = targetSpeed / 100.f;
  *reinterpret_cast<uint16_t*>(message.data + 5) = pwmFrequencyTarget;

  message.identifier       = static_cast<uint32_t>(CanMessageType::SPEAKER_CONTROL_COMMAND);
  message.data_length_code = 7;
  bus.sendMessage(message);
}

void SpeakerController::step()
{
  if(shouldReload)
  {
    bootloader.reloadFile();
    shouldReload = false;
  }

  bootloader.step();

  auto time = esp_timer_get_time();
  if(time - last_broadcast_time > TARGET_BROADCAST_INTERVAL_MICROSECONDS)
  {
    xSemaphoreTake(semaphore, portMAX_DELAY);
    publishTarget();
    xSemaphoreGive(semaphore);

    last_broadcast_time = time;
  }
}