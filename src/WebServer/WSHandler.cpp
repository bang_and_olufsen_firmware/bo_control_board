#include "WSHandler.hpp"

#include "Current/Current.hpp"
#include "Device/Device.hpp"
#include "Device/Stand.hpp"
#include "IO/IO.hpp"
#include "IOOut/IOOut.hpp"
#include "Internal/Internal.hpp"
#include "OTA/OTA.hpp"
#include "WebServerUtils.hpp"
#include "mongoose/mongoose.h"
#include "zlib.h"
#include <iostream>
#include <memory>

namespace WSHandler
{
struct MongooseReader {
  MongooseReader(mg_str* str) : str(str), position(0) {}
  // Reads one byte, or returns -1
  int read()
  {
    if(position >= str->len)
    {
      return -1;
    }
    int tmp = str->ptr[position];
    position++;
    return tmp;
  }
  // Reads several bytes, returns the number of bytes read.
  size_t readBytes(char* buffer, size_t length)
  {
    size_t cnt = 0;
    for(size_t i = 0; i < length && position < str->len; i++)
    {
      buffer[i] = str->ptr[position];
      position++;
      cnt++;
    }
    return cnt;
  }

 private:
  mg_str* str;
  size_t  position;
};

std::unique_ptr<uint8_t[]> generateJsonString(size_t* len)
{
  Device::updateStandTrajectoryCache();
  StaticJsonDocument<8000> jsonBuffer;
  JsonVariant              variant = jsonBuffer.to<JsonVariant>();
  Current::addCurrent(variant);
  IO::addIO(variant);
  IOOut::addIOOut(variant);
  Internal::addStats(variant);
  Device::addStates(variant);
  *len = measureJson(jsonBuffer);
  std::unique_ptr<uint8_t[]> buffer(new uint8_t[*len + 1]);

  if(buffer)
  {
    serializeJson(jsonBuffer, (char*)buffer.get(), *len + 1);
    return buffer;
  }
  return nullptr;
}

std::unique_ptr<uint8_t[]> compressBuffer(const std::unique_ptr<uint8_t[]> buffer, size_t* len)
{
  std::unique_ptr<uint8_t[]> outBuf(new uint8_t[*len]);

  z_stream defstream;
  defstream.zalloc    = Z_NULL;
  defstream.zfree     = Z_NULL;
  defstream.opaque    = Z_NULL;
  defstream.avail_in  = *len;
  defstream.next_in   = buffer.get();
  defstream.avail_out = *len;
  defstream.next_out  = outBuf.get();

  deflateInit(&defstream, Z_BEST_COMPRESSION);
  deflate(&defstream, Z_FINISH);
  deflateEnd(&defstream);

  *len = defstream.total_out;
  return outBuf;
}

void broadcastUpdate(void* arg)
{
  struct mg_mgr* mgr    = (struct mg_mgr*)arg;
  size_t         len    = 0;
  auto           buffer = compressBuffer(generateJsonString(&len), &len);
  if(buffer)
  {
    for(struct mg_connection* c = mgr->conns; c != NULL; c = c->next)
    {
      if(!c->is_websocket || c->send.len)
      {
        continue;
      }
      mg_ws_send(c, buffer.get(), len, WEBSOCKET_OP_BINARY);
    }
  }
}

static void handleWebsocketMessage(mg_ws_message* wm)
{
  StaticJsonDocument<8196> json;
  auto                     reader = MongooseReader(&wm->data);
  DeserializationError     err    = deserializeJson(json, reader);
  if(err)
  {
    log_i("deserializeJson() failed with code ");
    log_i("%s", err.c_str());
    return;
  }
  if(json.containsKey("device"))
  {
    Device::handleMessage(json["device"]);
  }
  if(json.containsKey("internal"))
  {
    Internal::handleMessage(json["internal"]);
  }
  if(json.containsKey("current"))
  {
    Current::handleMessage(json["current"]);
  }
  if(json.containsKey("io"))
  {
    IO::handleMessage(json["io"]);
  }
  if(json.containsKey("io_out"))
  {
    IOOut::handleMessage(json["io_out"]);
  }
  if(json.containsKey("tv_led"))
  {
    Internal::handleTVLEDMessage(json["tv_led"]);
  }
  if(json.containsKey("ota"))
  {
    OTA::handleMessage(json["ota"]);
  }
}

void eventHandler(struct mg_connection* c, int ev, void* ev_data, void* fn_data)
{
  if(ev == MG_EV_OPEN)
  {
  }
  else if(ev == MG_EV_HTTP_MSG)
  {
    struct mg_http_message* hm = (struct mg_http_message*)ev_data;
    if(mg_http_match_uri(hm, "/ws"))
    {
      // Upgrade to websocket. From now on, a connection is a full-duplex
      // Websocket connection, which will receive MG_EV_WS_MSG events.
      mg_ws_upgrade(c, hm, NULL);
    }
    else
    {
      // Serve static files
      static const struct mg_http_serve_opts opts = {
        .root_dir      = "/website",
        .ssi_pattern   = NULL,
        .extra_headers = NULL,
        .mime_types    = NULL,
        .page404       = NULL,
        .fs            = NULL,
      };

      auto hm = (mg_http_message*)ev_data;
      log_i("HTTP request: %.*s %.*s\n", (int)hm->method.len, hm->method.ptr, (int)hm->uri.len, hm->uri.ptr);

      mg_http_serve_dir(c, hm, &opts);
    }
  }
  else if(ev == MG_EV_WS_MSG)
  {
    // Got websocket frame.
    handleWebsocketMessage((struct mg_ws_message*)ev_data);
  }
  (void)fn_data;
}

}  // namespace WSHandler