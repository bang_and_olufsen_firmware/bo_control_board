import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";

class InfoTableRows extends Component {
  constructor(props, context) {
    super();
  }
  
  render() {
      return (
        <React.Fragment>
          {this.props.rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell style={row.name_style} component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="left">{row.value}</TableCell>
            </TableRow>
          ))}
        </React.Fragment>
      );
  }
}

class InfoTableHeader extends Component {
  constructor(props, context) {
    super(props, context);
  }
  
  render() {
      return (
        <TableHead>
          <TableRow>
            <TableCell>{this.props.name}</TableCell>
            <TableCell>{this.props.valuecolumn}</TableCell>
          </TableRow>
        </TableHead>
      );
  }
}

class InfoTable extends Component {
    constructor(props, context) {
      super(props, context);
    }
    
    render() {
        return (
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 200 }} aria-label="simple table">
              <InfoTableHeader name = {this.props.name} valuecolumn = {this.props.valuecolumn} />
              <TableBody>
              <InfoTableRows rows = {this.props.rows} />
              </TableBody>
            </Table>
          </TableContainer>
        );
    }
  }
  
export {
  InfoTable, 
  InfoTableRows,
  InfoTableHeader,
}