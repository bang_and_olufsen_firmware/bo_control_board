#pragma once
#include "common/CANMessages.hpp"
#include "driver/gpio.h"
#include "driver/twai.h"
#include <array>
#include <functional>
#include <unordered_map>

class CANController {
 public:
  CANController(gpio_num_t tx, gpio_num_t rx, gpio_num_t err, gpio_num_t stb, gpio_num_t enable);
  void registerCallback(CanMessageType msgType, std::function<void(const twai_message_t&)> callback);
  void recieveMessages();
  void sendMessage(twai_message_t& msg);
  bool sendMessageWithSmallQueue(twai_message_t& msg, uint32_t max_queue_size);

  ~CANController() = default;

 private:
  void                                                        print_stats(void);
  twai_timing_config_t                                        twai_config;
  twai_filter_config_t                                        filter_config;
  twai_general_config_t                                       general_config;
  std::array<std::function<void(const twai_message_t&)>, 256> callbacks = {nullptr};
  std::unordered_map<uint32_t, uint32_t>                      stats;
  uint32_t                                                    recvCount = 0;

  gpio_num_t err;
  gpio_num_t stb;
  gpio_num_t enable;
};