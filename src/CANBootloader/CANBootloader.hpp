#pragma once

#include "BoardType.hpp"
#include "CANController/CANController.hpp"
#include "FirmwareBuffer.hpp"
#include "common/CANMessages.hpp"
#include <string>

class CANBootloader {
  struct BulkChunk {
    uint8_t repeat;
    uint8_t size;
  };

 public:
  CANBootloader(std::string firmware_path, BoardType board, CANController& bus);
  ~CANBootloader() = default;
  void updateProgress(uint32_t _progress)
  {
    if(_progress >= START_ADDRESS)
    {
      progress = _progress;
    }
  }
  void     reloadFile();
  void     computeCRC();
  void     step();
  void     bootload();
  uint32_t getCRC();
  uint32_t getCachedCRC() { return cached_crc; }
  uint32_t getLoadedCRC() { return loaded_crc; }
  uint32_t getProgress() { return progress - START_ADDRESS; }

 private:
  uint32_t  getBulkCount(uint32_t offset, uint8_t size);
  BulkChunk getBulkChunk(uint32_t offset);
  bool      getData(uint32_t address, uint8_t* data, uint32_t len);
  void      handleBootloaderStatus(const twai_message_t& msg);
  void      sendReboot();
  void      sendProgramData();
  void      sendJump();

  void handleMainappCRC(const twai_message_t& msg);

  uint32_t       progress = START_ADDRESS;
  FirmwareBuffer buffer;
  CANController& bus;
  uint32_t       loaded_crc              = 0;
  uint32_t       cached_crc              = 0;
  uint32_t       msg_send_without_status = 0;

  static constexpr uint32_t START_ADDRESS = 0xA000;
  CanMessageType            BOOTLOADER_STATUS;
  CanMessageType            RESTART_BOOTLOAD;
  CanMessageType            REBOOT;
  CanMessageType            PROGRAM_DATA;
  CanMessageType            APPLICATION_CRC;
  CanMessageType            JUMP;
};