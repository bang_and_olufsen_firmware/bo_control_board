function createData(name, value) {
    return { name, value };
  }

export default function getDeviceStatRows(name, status)
{
    return [
        createData(name + " Load", status.load.toFixed(1) + "%"),
        createData(name + " Used heap", status.used_heap),
        createData(name + " Uptime", status.uptime.toFixed(1) + " s"),
    ]
}