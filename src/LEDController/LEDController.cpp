#include "LEDController.hpp"

#include <Arduino.h>

#include "esp_log.h"
#include "utilities/nvs_utilities.hpp"

#define MAX_BRIGHTNESS_KEY_NAME "max_brightness"
#define MIN_BRIGHTNESS_KEY_NAME "min_brightness"
#define FADE_TIME_KEY_NAME      "fade_time"

LEDController::LEDController(gpio_num_t led, ledc_channel_t led_channel, const char* nvs_namespace, bool invert)
  : channel(led_channel),
    invert(invert)
{
  vSemaphoreCreateBinary(settingsSemaphore);

  ledcSetup(led_channel, 10000, LEDC_TIMER_11_BIT);

  esp_err_t err = nvs_open(nvs_namespace, NVS_READWRITE, &nvs_handle);
  if(err != ESP_OK)
  {
    log_w("Error opening NVS for LED settings: %s", esp_err_to_name(err));
  }
  loadSettings();

  const int32_t min_duty = ((1 << 11) - 1) * min_brightness;
  ledcAttachPin(led, led_channel);
  ledcWrite(channel, invert ? MAX_DUTY - min_duty : min_duty);
}

void LEDController::loadSettings()
{
  max_brightness = nvs_utilities::get_float_setting(MAX_BRIGHTNESS_KEY_NAME, nvs_handle, 1);
  min_brightness = nvs_utilities::get_float_setting(MIN_BRIGHTNESS_KEY_NAME, nvs_handle, 0);
}

void ContinuousLEDController::loadSettings()
{
  fade_time_ms = nvs_utilities::get_float_setting(FADE_TIME_KEY_NAME, nvs_handle, 50);
}

void ContinuousLEDController::setFadeTime(float time)
{
  if(time >= 5 && time < 30000)
  {
    xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
    nvs_utilities::set_float_setting(FADE_TIME_KEY_NAME, nvs_handle, time);
    fade_time_ms = time;
    xSemaphoreGive(settingsSemaphore);
  }
}

void LEDController::setMinBrightness(float val)
{
  val = std::clamp(val, 0.f, 1.0f);

  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  val = std::min(val, max_brightness);
  nvs_utilities::set_float_setting(MIN_BRIGHTNESS_KEY_NAME, nvs_handle, val);
  min_brightness = val;
  xSemaphoreGive(settingsSemaphore);
}

void LEDController::setMaxBrightness(float val)
{
  val = std::clamp(val, 0.f, 1.0f);

  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  val = std::max(val, min_brightness);
  nvs_utilities::set_float_setting(MAX_BRIGHTNESS_KEY_NAME, nvs_handle, val);
  max_brightness = val;
  xSemaphoreGive(settingsSemaphore);
}

void LEDController::startFade(bool fadeDirection)
{
  finished        = false;
  fadeStartTime   = esp_timer_get_time();
  fade_start_duty = last_duty;
  if(fadeDirection)
  {
    fade_target_duty = MAX_DUTY * std::max(max_brightness, min_brightness) * onPower;
  }
  else
  {
    fade_target_duty = MAX_DUTY * std::min(min_brightness, max_brightness);
  }
  faded_up = fadeDirection;
}

void LEDController::forceNewFade(bool fadeDirection, bool restartIfInProgress)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  if(restartIfInProgress || fadeDirection != faded_up)
  {
    startFade(fadeDirection);
  }
  xSemaphoreGive(settingsSemaphore);
}

void LEDController::setOnPower(float val)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  onPower = val;
  xSemaphoreGive(settingsSemaphore);
}

bool LEDController::fadingOn()
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  bool ret = faded_up;
  xSemaphoreGive(settingsSemaphore);

  return ret;
}

bool LEDController::step()
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);

  if(fadeStartTime == 0)
  {
    fadeStartTime = esp_timer_get_time();
  }

  if(isFinished())
  {
    startFade(!faded_up);
  }

  const uint64_t time_fading  = std::max(fade_time_ms * 1000, 5000.f);
  const uint64_t current_time = esp_timer_get_time();

  if(current_time - fadeStartTime > time_fading)
  {
    finished = true;
  }

  const uint64_t timeFaded    = current_time - fadeStartTime;
  const float    fadeProgress = std::clamp(((float)timeFaded) / time_fading, 0.0f, 1.0f);

  int32_t next_duty = fade_start_duty * (1 - fadeProgress) + fade_target_duty * fadeProgress;

  const int32_t max_duty = MAX_DUTY * std::max(max_brightness, min_brightness);
  const int32_t min_duty = MAX_DUTY * std::min(max_brightness, min_brightness);

  next_duty = std::clamp(next_duty, min_duty, max_duty);
  last_duty = next_duty;
  xSemaphoreGive(settingsSemaphore);
  ledcWrite(channel, invert ? MAX_DUTY - next_duty : next_duty);
  return finished;
}

bool OneShotLEDController::step()
{
  if(isFinished())
  {
    return false;
  }
  bool ret = LEDController::step();
  if(ret)
  {
    finished = true;
  }
  return ret;
}