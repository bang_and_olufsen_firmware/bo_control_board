#pragma once

#pragma once

#include "CANBootloader/CANBootloader.hpp"
#include "CANController/CANController.hpp"

class Controller {
 public:
  Controller(
    CANController& bus,
    CANBootloader& bootloader,
    CanMessageType heapMsgType,
    CanMessageType uptimeMsgType,
    CanMessageType loadMsgType);

  ~Controller() = default;
  int64_t        getTimeSinceLastStatus() const;
  virtual void   step() = 0;
  CANBootloader& getBootloader() { return bootloader; }
  uint32_t       getUsedHeap() const { return used_heap; }
  uint32_t       getTotalHeap() const { return total_heap; }
  uint64_t       getUptime() const { return uptime; }
  float          getLoad() const { return load; }

 protected:
  void handleHeap(const twai_message_t& msg);
  void handleUptime(const twai_message_t& msg);
  void handleLoad(const twai_message_t& msg);

  CANController& bus;
  CANBootloader& bootloader;
  int64_t        last_status_time = 0;
  uint32_t       used_heap        = 0xFFFFFFFF;
  uint32_t       total_heap       = 0xFFFFFFFF;
  uint64_t       uptime           = 0xFFFFFFFFFFFFFFFF;
  float          load             = -1;
};