#pragma once

#include "OnOffDetector.hpp"
#include "driver/gpio.h"

class IoOnOffDetector : public OnOffDetector {
 public:
  IoOnOffDetector(SpeakerController& speaker, StandController& stand, TVLEDController& tv_led, GPIOController& gpio_controller);
  ~IoOnOffDetector() = default;
  gpio_num_t getInputPin() const { return input_pin; }
  void       setInputPin(gpio_num_t pin);
  void       step();

 private:
  void loadSubSettings();

  uint64_t   last_bounce_time = 0;
  bool       last_pin_state;
  gpio_num_t input_pin;
};