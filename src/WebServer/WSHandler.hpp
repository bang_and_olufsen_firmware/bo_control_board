#pragma once

struct mg_connection;
namespace WSHandler
{
void broadcastUpdate(void* arg);
void eventHandler(struct mg_connection* c, int ev, void* ev_data, void* fn_data);
}  // namespace WSHandler