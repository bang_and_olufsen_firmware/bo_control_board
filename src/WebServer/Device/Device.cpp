#include "Device.hpp"

#include <ArduinoJson.h>

#include "Speaker.hpp"
#include "Stand.hpp"

namespace Device
{

void addStates(JsonVariant& json)
{
  addStandStatus(json);
  addSpeakerStatus(json);
}

void handleMessage(const JsonVariant& json)
{
  if(json.containsKey("stand"))
  {
    handleStandMessage(json["stand"]);
  }
  if(json.containsKey("speaker"))
  {
    handleSpeakerMessage(json["speaker"]);
  }
}
}  // namespace Device