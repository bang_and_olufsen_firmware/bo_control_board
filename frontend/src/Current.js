import React, { Component } from "react";
import Container from "@mui/material/Container";
import { InfoTable } from "./InfoTable";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { Checkbox, TextField, FormControlLabel } from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";
import ElectricMeterIcon from "@mui/icons-material/ElectricMeter";

function createData(name, value) {
  return { name, value };
}

class CurrentOptions extends Component {
  constructor(props, context) {
    super();
    this.getOffThreshold = this.getOffThreshold.bind(this);
    this.getOnThreshold = this.getOnThreshold.bind(this);
    this.getControlsStand = this.getControlsStand.bind(this);
    this.getControlsSpeaker = this.getControlsSpeaker.bind(this);
    this.getControlsTVLED = this.getControlsTVLED.bind(this);
    this.getControlsIOOut = this.getControlsGPIO.bind(this);

    this.getAmpGain = this.getAmpGain.bind(this);

    this.state = {
      "off_threshold_set_time" : new Date(),
      "on_threshold_set_time" : new Date(),
      "controls_stand_set_time" : new Date(),
      "controls_speaker_set_time" : new Date(),
      "controls_tv_led_set_time" : new Date(),
      "controls_gpio_set_time" : new Date(),
      "amp_gain_set_time" : new Date(),
    }
  }

  getAmpGain() {
    if(Date.now() - this.state["amp_gain_set_time"] < 2000 && "amp_gain" in this.state) {
      return this.state.amp_gain
    }
    return this.props.state.amp_gain
  }

  getOffThreshold() {
    if(Date.now() - this.state["off_threshold_set_time"] < 2000 && "off_threshold" in this.state) {
      return this.state.off_threshold
    }
    return this.props.state.off_threshold
  }

  getOnThreshold() {
    if(Date.now() - this.state["on_threshold_set_time"] < 2000 && "on_threshold" in this.state) {
      return this.state.on_threshold
    }
    return this.props.state.on_threshold
  }

  getControlsSpeaker() {
    if(Date.now() - this.state["controls_speaker_set_time"] < 2000 && "controls_speaker" in this.state) {
      return this.state.controls_speaker
    }
    return this.props.state.controls_speaker
  }

  getControlsStand() {
    if(Date.now() - this.state["controls_stand_set_time"] < 2000 && "controls_stand" in this.state) {
      return this.state.controls_stand
    }
    return this.props.state.controls_stand
  }

  getControlsGPIO() {
    if(Date.now() - this.state["controls_gpio_set_time"] < 2000 && "controls_gpio" in this.state) {
      return this.state.controls_gpio
    }
    return this.props.state.controls_gpio
  }

  getControlsTVLED() {
    if(Date.now() - this.state["controls_tv_led_set_time"] < 2000 && "controls_tv_led" in this.state) {
      return this.state.controls_tv_led
    }
    return this.props.state.controls_tv_led
  }


  render() {
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 200 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell style={{ borderBottom: "none" }}>Options</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow
              key="control_stand"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell
                component="th"
                scope="row"
                style={{ borderBottom: "none" }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.getControlsStand()}
                      inputProps={{ "aria-label": "controlled" }}
                      onChange={(event) => {
                        this.setState({controls_stand: event.target.checked,
                          controls_stand_set_time : new Date()})
                        this.props.sendJsonMessage({
                          current: {
                            enable_action: {
                              controls_stand: event.target.checked,
                            },
                          },
                        });
                      }}
                    />
                  }
                  label="Control Stand"
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="control_speaker"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell
                component="th"
                scope="row"
                style={{ borderBottom: "none" }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.getControlsSpeaker()}
                      inputProps={{ "aria-label": "controlled" }}
                      onChange={(event) => {
                        this.setState({controls_speaker: event.target.checked,
                          controls_speaker_set_time : new Date()})
                        this.props.sendJsonMessage({
                          current: {
                            enable_action: {
                              controls_speaker: event.target.checked,
                            },
                          },
                        });
                      }}
                    />
                  }
                  label="Control Speaker"
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="control_tv_led"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell
                component="th"
                scope="row"
                style={{ borderBottom: "none" }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.getControlsTVLED()}
                      inputProps={{ "aria-label": "controlled" }}
                      onChange={(event) => {
                        this.setState({controls_tv_led: event.target.checked,
                          controls_tv_led_set_time : new Date()})
                        this.props.sendJsonMessage({
                          current: {
                            enable_action: {
                              controls_tv_led: event.target.checked,
                            },
                          },
                        });
                      }}
                    />
                  }
                  label="Control TV LED"
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="control_io_out"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell
                component="th"
                scope="row"
                style={{ borderBottom: "none" }}
              >
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={this.getControlsGPIO()}
                      inputProps={{ "aria-label": "controlled" }}
                      onChange={(event) => {
                        this.setState({controls_gpio: event.target.checked,
                          controls_gpio_set_time : new Date()})
                        this.props.sendJsonMessage({
                          current: {
                            enable_action: {
                              controls_gpio: event.target.checked,
                            },
                          },
                        });
                      }}
                    />
                  }
                  label="Control IO Out"
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="off_threshold"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getOffThreshold()}
                  step={0.1}
                  type="number"
                  min = {0}
                  max = {10}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">mA</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Off Threshold"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({off_threshold: parseFloat(event.target.value),
                                   off_threshold_set_time : new Date()})
                    this.props.sendJsonMessage({
                      current: { set_off_level: parseFloat(event.target.value) },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
            <TableRow
              key="on_threshold"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getOnThreshold()}
                  step={0.1}
                  type="number"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">mA</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="On Threshold"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({on_threshold: parseFloat(event.target.value),
                                   on_threshold_set_time : new Date()})
                    this.props.sendJsonMessage({
                      current: { set_on_level: parseFloat(event.target.value) },
                    });
                  }}

                />
              </TableCell>
            </TableRow>
            <TableRow
              key="amp_gain"
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell align="left" style={{ borderBottom: "none" }}>
                <TextField
                  value={this.getAmpGain()}
                  step={0.1}
                  type="number"
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">A/V</InputAdornment>
                    ),
                  }}
                  id="outlined-basic"
                  label="Amperage Gain"
                  variant="outlined"
                  onChange={(event) => {
                    this.setState({amp_gain: parseFloat(event.target.value),
                                   amp_gain_set_time : new Date()})
                    this.props.sendJsonMessage({
                      current: { amp_gain: parseFloat(event.target.value) },
                    });
                  }}
                />
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  }
}

class Current extends Component {
  constructor(props, context) {
    super();
  }

  static getPageName() {
    return "Current";
  }

  static getPageIcon() {
    return (
      <ElectricMeterIcon />
    )
  }

  render() {
    const rows = [
      createData(
        "Estimated RMS current",
        this.props.state.rms_current.toFixed(1) + " mA"
      ),
      createData("Bias", this.props.state.bias.toFixed(1) + " mV"),
      createData("Error", this.props.state.error.toFixed(1)),
      createData("Sample Rate", this.props.state.samplerate.toFixed(1) + "/s"),
      createData("State", this.props.state.state),
      createData("Max state current", this.props.state.max_ma_in_state.toFixed(1) + " mA"),
      createData("Min state current", this.props.state.min_ma_in_state.toFixed(1) + " mA"),
      createData("Time in State", this.props.state.time_since_state.toFixed(1) + " s"),

    ];

    return (
      <React.Fragment>
        <Container disableGutters={true} maxWidth="lg" sx={{mt: 4, mb: 4 }}>
            <InfoTable
              name="Current Measurement"
              valuecolumn="Value"
              rows={rows}
            />
          </Container>
          <Container disableGutters={true} maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <CurrentOptions
              state={this.props.state}
              sendJsonMessage={this.props.sendJsonMessage}
            />
          </Container>
          </React.Fragment>
    );
  }
}

export default Current;
