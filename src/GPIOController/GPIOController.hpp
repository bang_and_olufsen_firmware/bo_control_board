#pragma once

#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "nvs_flash.h"

class GPIOController {
 public:
  enum class GPIOControllerState {
    OFF,
    ON,
  };

  GPIOController(gpio_num_t gpio, const char* nvs_namespace);
  ~GPIOController() = default;
  void     step();
  void     setState(GPIOControllerState _state);
  void     setOnDelay(uint32_t delay);
  void     setOffDelay(uint32_t delay);
  uint32_t getOnDelay() const { return on_delay_ms; };
  uint32_t getOffDelay() const { return off_delay_ms; };

  uint32_t off_delay_ms = 0;
  uint32_t on_delay_ms  = 0;

 protected:
  void                loadSettings();
  SemaphoreHandle_t   settingsSemaphore;
  nvs_handle_t        nvs_handle;
  bool                awaiting_delay   = false;
  uint64_t            toggle_timestamp = 0;
  gpio_num_t          pin;
  GPIOControllerState state = GPIOControllerState::OFF;
};