#pragma once

#include "GPIOController/GPIOController.hpp"
#include "LEDController/TVLEDController.hpp"
#include "SpeakerController/SpeakerController.hpp"
#include "StandController/StandController.hpp"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "nvs_flash.h"

class OnOffDetector {
 public:
  OnOffDetector(
    SpeakerController& speaker,
    StandController&   stand,
    TVLEDController&   tv_led,
    GPIOController&    gpio_controller,
    const char*        nvs_namespace);
  ~OnOffDetector() = default;

  void loadSettings();
  bool controlsStand() const { return control_stand; }
  void setControlsStand(bool val);
  bool controlsSpeaker() const { return control_speakerbar; }
  void setControlsSpeaker(bool val);
  void setControlsTVLED(bool val);
  void setControlsGPIO(bool val);

  bool controlsTVLED() const { return control_tv_led; }
  bool controlsGPIO() const { return control_gpio; }

  float       getTimeSinceStateChange() const { return (esp_timer_get_time() - last_state_change_time) * 0.000001; }
  const char* getStateStr() const
  {
    switch(last_state)
    {
      case LastCurrentState::ON:
        return "ON";
      case LastCurrentState::OFF:
        return "OFF";
      default:
        return "UNDEFINED";
    }
  }

 protected:
  enum class LastCurrentState { UNDEFINED, ON, OFF };
  void                            setState(LastCurrentState state);
  virtual void                    stateChangeHook() {}
  void                            handleOn();
  void                            handleOff();
  SemaphoreHandle_t               settingsSemaphore;
  nvs_handle_t                    nvs_handle;
  uint64_t                        last_state_change_time = 0;
  OnOffDetector::LastCurrentState last_state             = LastCurrentState::UNDEFINED;
  SpeakerController&              speaker;
  StandController&                stand;
  TVLEDController&                tv_led;
  GPIOController&                 gpio_controller;
  bool                            control_speakerbar = false;
  bool                            control_stand      = false;
  bool                            control_tv_led     = false;
  bool                            control_gpio       = false;
};