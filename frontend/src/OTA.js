import React, { Component } from "react";
import { Button } from "@mui/material";
import ReactFileReader from "react-file-reader";
import { Grid } from "@mui/material";
import Container from "@mui/material/Container";
import UpgradeIcon from "@mui/icons-material/Upgrade";

import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

class OTA extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleFiles = this.handleFiles.bind(this);
    this.handleData = this.handleData.bind(this);

    this.state = {
      fileArray: null,
      offset: 0,
      size: 1,
    };
  }

  static getPageName() {
    return "OTA";
  }

  static getPageIcon() {
    return (
      <UpgradeIcon />
    )
  }

  handleData(data) {
    this.setState({ offset: 0 });
  }

  handleFiles = (files) => {
    let fileReader = new FileReader();
    var page = this;
    fileReader.onload = function (event) {
      page.setState({ offset: 0 });
      page.setState({ size: fileReader.result.byteLength });
      page.setState({ fileArray: fileReader.result });

      var id = setInterval(function () {
          let data = page.state.fileArray.slice(
            page.state.offset,
            page.state.offset + 3000
          )
          page.props.sendJsonMessage({
            ota : {data : btoa(String.fromCharCode(...new Uint8Array(data))),
                   offset : page.state.offset,
                   size : page.state.size,
                   final : page.state.offset + data.byteLength == page.state.offset}
        })

        page.setState({ offset: page.state.offset + data.byteLength });
        if (page.state.offset === page.state.size) {
          clearInterval(id);
        }
      }, 25);
    };

    fileReader.readAsArrayBuffer(files[0]);
  };

  render() {
    return (
        <Container disableGutters={true} maxWidth="lg" sx={{ mt: 10, mb: 4 }}>
            <Grid spacing = {5}
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid item xs={12}>
                <ReactFileReader
                  fileTypes=".bin"
                  base64={false}
                  multipleFiles={false}
                  handleFiles={this.handleFiles}
                >
                  <Button variant="contained" color="primary">
                    Upload Firmware
                  </Button>
                </ReactFileReader>
              </Grid>
              <Grid item xs={3} zeroMinWidth>
                {this.state.offset === 0 ? (
                  <CircularProgressbar value={0} maxValue={1} text={`${0}%`} />
                ) : (
                  <CircularProgressbar
                    value={this.state.offset / this.state.size}
                    maxValue={1}
                    text={`${Math.floor(
                      (this.state.offset / this.state.size) * 100
                    )}%`}
                  />
                )}
              </Grid>
            </Grid>
        </Container>
    );
  }
}

export default OTA;
