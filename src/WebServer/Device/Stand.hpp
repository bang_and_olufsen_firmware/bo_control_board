#pragma once

#include "ArduinoJson.h"
namespace Device
{
void addStandStatus(JsonVariant& json);
void handleStandMessage(const JsonVariant& json);
void updateStandTrajectoryCache();
}  // namespace Device