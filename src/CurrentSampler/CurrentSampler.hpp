#pragma once

#include <TrueRMS.h>
#include <math.h>
#include <stdint.h>

#include "common/Filters/Median.hpp"
#include "common/Filters/MovingAverage.hpp"
#include "driver/adc.h"
#include <array>

class CurrentSampler {
 public:
  CurrentSampler(adc_channel_t channel);
  ~CurrentSampler() = default;
  float getRMS() const { return rmsMedian.median * volt_to_amp; }
  float getBias() const { return rms.dcBias * CONVERSION_MEASUREMENT_TO_VOLTAGE; }
  float getError() const { return rms.error; }

  float getSampleRate() const;
  void  step();
  void  start();
  void  setAmperageGain(float gain) { volt_to_amp = gain; }
  float getAmperageGain() const { return volt_to_amp; }
  bool  newMeasurementReady()
  {
    bool tmp              = new_measurement_ready;
    new_measurement_ready = false;
    return tmp;
  }

 private:
  static constexpr uint32_t SAMPLE_RATE                       = 80000;
  static constexpr uint32_t RMS_WINDOW                        = SAMPLE_RATE / 5;
  static constexpr size_t   BUFFER_SIZE                       = SAMPLE_RATE / 100;
  static constexpr float    MAX_SAMPLE_VOLTAGE                = 3.1f;
  static constexpr uint32_t MAX_SAMPLE_VALUE                  = (1 << 12) - 1;
  static constexpr float    CONVERSION_MEASUREMENT_TO_VOLTAGE = MAX_SAMPLE_VOLTAGE / MAX_SAMPLE_VALUE;
  static constexpr float    CONVERSION_VOLTAGE_TO_MEASUREMENT = MAX_SAMPLE_VALUE / MAX_SAMPLE_VOLTAGE;
  float                     volt_to_amp                       = 10;
  adc_channel_t             channel;
  std::array<float, 4096>   calibration_lookup;
  uint64_t                  total_samples     = 0;
  int64_t                   sample_start_time = 0;
  uint64_t                  next_publish      = 0;
  Rms                       rms;
  MovingAverage<float, 4>   movingSampleAvg;
  Median<float, 8>          rmsMedian;
  bool                      new_measurement_ready = false;
};
