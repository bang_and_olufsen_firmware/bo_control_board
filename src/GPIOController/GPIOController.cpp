#include "GPIOController.hpp"

#include <Arduino.h>

#include "utilities/nvs_utilities.hpp"

#define ON_DELAY_KEY  "ON_DELAY_KEY"
#define OFF_DELAY_KEY "OFF_DELAY_KEY"

GPIOController::GPIOController(gpio_num_t gpio, const char* nvs_namespace) : pin(gpio)
{
  settingsSemaphore = xSemaphoreCreateBinary();
  xSemaphoreGive(settingsSemaphore);

  esp_err_t err = nvs_open(nvs_namespace, NVS_READWRITE, &nvs_handle);
  if(err != ESP_OK)
  {
    log_w("Error opening NVS for current settings: %s", esp_err_to_name(err));
  }
  loadSettings();

  pinMode(gpio, OUTPUT);
  digitalWrite(gpio, HIGH);
}

void GPIOController::loadSettings()
{
  on_delay_ms  = nvs_utilities::get_uint32_setting(ON_DELAY_KEY, nvs_handle, false);
  off_delay_ms = nvs_utilities::get_uint32_setting(OFF_DELAY_KEY, nvs_handle, false);
}

void GPIOController::setOnDelay(uint32_t delay)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_uint32_setting(ON_DELAY_KEY, nvs_handle, delay);
  on_delay_ms = delay;
  xSemaphoreGive(settingsSemaphore);
}

void GPIOController::setOffDelay(uint32_t delay)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_uint32_setting(OFF_DELAY_KEY, nvs_handle, delay);
  off_delay_ms = delay;
  xSemaphoreGive(settingsSemaphore);
}

void GPIOController::setState(GPIOControllerState _state)
{
  if(state != _state)
  {
    state            = _state;
    toggle_timestamp = esp_timer_get_time();
    toggle_timestamp += state == GPIOControllerState::ON ? on_delay_ms * 1000 : off_delay_ms * 1000;
    awaiting_delay = true;
  }
}

void setOnDelay(uint32_t delay);
void setOffDelay(uint32_t delay);

void GPIOController::step()
{
  if(awaiting_delay && toggle_timestamp < esp_timer_get_time())
  {
    awaiting_delay = false;
    digitalWrite(pin, state == GPIOControllerState::ON ? LOW : HIGH);
  }
}