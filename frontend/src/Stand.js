import React, { Component } from "react";
import Container from "@mui/material/Container";
import { InfoTable } from "./InfoTable";
import getDeviceStatRows from "./DeviceStats";
import getDeviceBootStatusRows from "./BootStatus";
import StandTrajectories from "./StandTrajectories";
import Paper from "@mui/material/Paper";
import TvIcon from "@mui/icons-material/Tv";
import { Button } from "@mui/material";

function createData(name, value) {
  return { name, value };
}

function getMotorRows(name, state) {
  return [
    createData(name + " Target Speed", (state.speed * 100).toFixed(0) + " %"),
    createData(
      name + " Encoder Speed",
      state.encoder_speed.toFixed(2) + " ticks/s"
    ),
    createData(name + " Encoder", state.encoder.toFixed(1) + "º"),
    createData(name + " Target", state.target.toFixed(1) + "º"),
  ];
}

class Stand extends Component {
  constructor(props, context) {
    super();
    this.sendStop = this.sendStop.bind(this);
  }

  static getPageName() {
    return "Stand";
  }

  static getPageIcon() {
    return <TvIcon />;
  }

  sendStop() {
    this.props.sendJsonMessage({
      device: {
        stand: {
          stop_movement: true,
        },
      },
    });
  }

  render() {
    const rows = getMotorRows("Motor 1", this.props.state.status.motors[0])
      .concat(getMotorRows("Motor 2", this.props.state.status.motors[1]))
      .concat(getDeviceStatRows("", this.props.state.stats))
      .concat(getDeviceBootStatusRows("", this.props.state.bootstatus))
      .concat(
        createData(
          "Time since update",
          this.props.state.status.time_since_update.toFixed(1) + " s"
        )
      );

    return (
      <Container disableGutters={true} maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
          }}
        >
          <StandTrajectories
            position={this.props.state.status.motors}
            state={this.props.state.trajectories}
            sendJsonMessage={this.props.sendJsonMessage}
          />
          <Button
            variant="contained"
            style="margin:5px"
            onClick={() => {
              this.sendStop();
            }}
          >
            Stop
          </Button>
        </Paper>
        <Paper
          sx={{
            p: 2,
            display: "flex",
            flexDirection: "column",
          }}
        >
          <InfoTable name="Stand Status" valuecolumn="Value" rows={rows} />
        </Paper>
      </Container>
    );
  }
}

export default Stand;
