#include "CurrentController/CurrentController.hpp"

#include "utilities/nvs_utilities.hpp"

#define OFF_THRESHOLD_KEY_NAME "off_threshold"
#define ON_THRESHOLD_KEY_NAME  "on_threshold"
#define AMPERAGE_GAIN_KEY_NAME "amp_gain"

CurrentController::CurrentController(
  SpeakerController& speaker,
  StandController&   stand,
  TVLEDController&   tv_led,
  GPIOController&    gpio_controller)
  : OnOffDetector(speaker, stand, tv_led, gpio_controller, "cur_settings"),
    sampler(ADC_CHANNEL_7)
{
  loadSubSettings();
}

void CurrentController::loadSubSettings()
{
  off_threshold = nvs_utilities::get_float_setting(OFF_THRESHOLD_KEY_NAME, nvs_handle, 0);
  on_threshold  = nvs_utilities::get_float_setting(ON_THRESHOLD_KEY_NAME, nvs_handle, 1);
  sampler.setAmperageGain(nvs_utilities::get_float_setting(AMPERAGE_GAIN_KEY_NAME, nvs_handle, 10));
}

void CurrentController::setAmperageGain(float gain)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_float_setting(AMPERAGE_GAIN_KEY_NAME, nvs_handle, gain);
  sampler.setAmperageGain(gain);
  xSemaphoreGive(settingsSemaphore);
}

void CurrentController::setOffThreshold(float val)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_float_setting(OFF_THRESHOLD_KEY_NAME, nvs_handle, val);
  off_threshold = val;
  xSemaphoreGive(settingsSemaphore);
}

void CurrentController::setOnThreshold(float val)
{
  xSemaphoreTake(settingsSemaphore, portMAX_DELAY);
  nvs_utilities::set_float_setting(ON_THRESHOLD_KEY_NAME, nvs_handle, val);
  on_threshold = val;
  xSemaphoreGive(settingsSemaphore);
}

void CurrentController::step()
{
  sampler.step();
  if(sampler.newMeasurementReady())
  {
    lastRMSCurrent       = sampler.getRMS() * 1000.f;
    max_current_in_state = std::max(lastRMSCurrent, max_current_in_state);
    min_current_in_state = std::min(lastRMSCurrent, min_current_in_state);

    if(lastRMSCurrent > on_threshold)
    {
      handleOn();
    }
    else if(lastRMSCurrent < off_threshold)
    {
      handleOff();
    }
  }
}

void CurrentController::stateChangeHook()
{
  max_current_in_state = lastRMSCurrent;
  min_current_in_state = lastRMSCurrent;
}