function createData(name, value) {
    return { name, value };
  }

export default function getDeviceBootStatusRows(name, status)
{
    return [
        createData(name + " Loaded CRC",  "0x" + status.loaded_crc.toString(16).padStart(8, '0').toUpperCase()),
        createData(name + " Target CRC",  "0x" + status.target_crc.toString(16).padStart(8, '0').toUpperCase()),
        createData(name + " Progress",  "0x" + status.progress.toString(16).padStart(5, '0').toUpperCase() + " Bytes"),
  ]
}