#pragma once
#include "nvs_flash.h"

namespace nvs_utilities
{
void     set_float_setting(const char* keyname, nvs_handle_t handle, float value);
void     set_bool_setting(const char* keyname, nvs_handle_t handle, bool value);
void     set_uint32_setting(const char* keyname, nvs_handle_t handle, uint32_t value);
float    get_float_setting(const char* keyname, nvs_handle_t handle, float default_value);
bool     get_bool_setting(const char* keyname, nvs_handle_t handle, bool default_value);
uint32_t get_uint32_setting(const char* keyname, nvs_handle_t handle, uint32_t default_value);
}  // namespace nvs_utilities