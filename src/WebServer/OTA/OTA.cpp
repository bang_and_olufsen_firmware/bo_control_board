#include "OTA.hpp"

#include "ArduinoJson.h"
#include "Update.h"
#include "mbedtls/base64.h"
#include <memory>

namespace OTA
{
void handleMessage(const JsonVariant& json)
{
  if(!json.containsKey("offset") || !json["offset"].is<uint32_t>())
  {
    log_w("No 'offset' in OTA frame");
    return;
  }
  if(!json.containsKey("size") || !json["size"].is<uint32_t>())
  {
    log_w("No 'size' in OTA frame");
    return;
  }

  if(!json.containsKey("data") || !json["data"].is<const char*>())
  {
    log_w("No 'data' in OTA frame");
    return;
  }
  if(!json.containsKey("final") || !json["final"].is<bool>())
  {
    log_w("No 'final' in OTA frame");
    return;
  }

  uint32_t offset   = json["offset"].as<uint32_t>();
  bool     is_final = json["final"].as<bool>();

  size_t                     size         = 0;
  const size_t               input_length = strlen(json["data"].as<const char*>());
  std::unique_ptr<uint8_t[]> data(new uint8_t[input_length]);

  if(mbedtls_base64_decode(
       (unsigned char*)data.get(),
       input_length,
       &size,
       (const unsigned char*)json["data"].as<const char*>(),
       input_length))
  {
    log_e("Error decoding base64");
    return;
  }

  if(offset == 0)
  {
    log_i("OTA Upgrade started");
    Update.abort();
    Update.begin();
  }

  size_t written = Update.write(data.get(), size);
  if(written != size)
  {
    log_e("Only wrote 0x%08X of 0x%08X OTA bytes", written, size);
  }
  if(is_final)
  {
    if(Update.end(true))
    {
      esp_restart();
    }
    else
    {
      log_e("OTA failed with: %s", Update.errorString());
    }
  }
}
}  // namespace OTA