#pragma once

#include <ArduinoJson.h>

#include "CANBootloader/CANBootloader.hpp"
#include "Controller/Controller.hpp"
namespace Device
{
void addBootloaderStatus(JsonVariant& json, CANBootloader& bootloader);
void addStats(JsonVariant& json, Controller& controller);
}  // namespace Device