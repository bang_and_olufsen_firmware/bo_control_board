#pragma once
#include "ArduinoJson.h"

namespace Internal
{
void addStats(JsonVariant& json);
void handleMessage(const JsonVariant& json);
void handleTVLEDMessage(const JsonVariant& json);
}  // namespace Internal
