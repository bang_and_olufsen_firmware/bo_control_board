#include <ESPmDNS.h>
#include <WiFi.h>

#include "WebServer/WebServer.hpp"
#include "esp_err.h"
#include "filesystem/filesystem.hpp"
#include "nvs_flash.h"
#include "tasks/can_task/can_task.hpp"
#include "tasks/misc_task/misc_task.hpp"
#include <ets-appender.hpp>
#include <iostream>
#include <logging.hpp>
#include <udp-appender.hpp>

extern "C" void app_main(void)
{
  ESP_ERROR_CHECK(nvs_flash_init());
  filesystem_init();
  can_task_init();
  misc_task_init();
  webserver::webserver_init();
  MDNS.begin("tvcontrol");
  MDNS.addService("http", "tcp", 80);
  esp32m::Logging::addAppender(&esp32m::ETSAppender::instance());
  esp32m::Logging::addAppender(new esp32m::UDPAppender(nullptr, 1024));
  esp32m::Logging::hookUartLogger();
  esp32m::Logging::hookEsp32Logger();
}