#include "IO.hpp"

#include "ArduinoJson.h"
#include "WebServer/WebServerUtils.hpp"
#include "tasks/misc_task/misc_task.hpp"

namespace IO
{

void addIO(JsonVariant& json)
{
  auto&       controller = misc_task_get_io_controller();
  JsonVariant io         = WebServerUtils::getOrCreateObject(json, "io");

  io["state"]            = controller.getStateStr();
  io["time_since_state"] = controller.getTimeSinceStateChange();
  io["pin"]              = controller.getInputPin();

  io["controls_stand"]   = controller.controlsStand();
  io["controls_speaker"] = controller.controlsSpeaker();
  io["controls_tv_led"]  = controller.controlsTVLED();
  io["controls_gpio"]    = controller.controlsGPIO();
}

void handleMessage(const JsonVariant& json)
{
  if(json.containsKey("enable_action"))
  {
    auto actions = json["enable_action"];
    if(actions.containsKey("controls_speaker") && actions["controls_speaker"].is<bool>())
    {
      misc_task_get_io_controller().setControlsSpeaker(actions["controls_speaker"].as<bool>());
    }
    if(actions.containsKey("controls_stand") && actions["controls_stand"].is<bool>())
    {
      misc_task_get_io_controller().setControlsStand(actions["controls_stand"].as<bool>());
    }
    if(actions.containsKey("controls_tv_led") && actions["controls_tv_led"].is<bool>())
    {
      misc_task_get_io_controller().setControlsTVLED(actions["controls_tv_led"].as<bool>());
    }
    if(actions.containsKey("controls_gpio") && actions["controls_gpio"].is<bool>())
    {
      misc_task_get_io_controller().setControlsGPIO(actions["controls_gpio"].as<bool>());
    }
  }
  if(json.containsKey("set_pin") && json["set_pin"].is<uint32_t>())
  {
    misc_task_get_io_controller().setInputPin((gpio_num_t)json["set_pin"].as<uint32_t>());
  }
}
}  // namespace IO