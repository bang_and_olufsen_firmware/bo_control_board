#include "CANBootloader.hpp"

#include <Arduino.h>

#include "endian.h"

#define MAX_BULK_CHUNK_SIZE 3
#define MAX_BULK_REPEAT     255U

CANBootloader::CANBootloader(std::string firmware_path, BoardType board, CANController& bus)
  : progress(START_ADDRESS),
    buffer(firmware_path),
    bus(bus)
{
  if(board == BoardType::STAND)
  {
    BOOTLOADER_STATUS = CanMessageType::STAND_CONTROL_PROGRAM_STATUS;
    RESTART_BOOTLOAD  = CanMessageType::RESTART_STAND_CONTROL_BOOTLOAD;
    REBOOT            = CanMessageType::STAND_CONTROL_REBOOT;
    PROGRAM_DATA      = CanMessageType::PROGRAM_STAND_CONTROL;
    APPLICATION_CRC   = CanMessageType::STAND_APPLICATION_CRC;
    JUMP              = CanMessageType::STAND_JUMP_TO_MAINAPP;
  }
  else
  {
    BOOTLOADER_STATUS = CanMessageType::SPEAKER_CONTROL_PROGRAM_STATUS;
    RESTART_BOOTLOAD  = CanMessageType::RESTART_SPEAKER_CONTROL_BOOTLOAD;
    REBOOT            = CanMessageType::SPEAKER_CONTROL_REBOOT;
    PROGRAM_DATA      = CanMessageType::PROGRAM_SPEAKER_CONTROL;
    APPLICATION_CRC   = CanMessageType::SPEAKER_APPLICATION_CRC;
    JUMP              = CanMessageType::SPEAKER_JUMP_TO_MAINAPP;
  }

  bus.registerCallback(BOOTLOADER_STATUS, [this](const twai_message_t& msg) { this->handleBootloaderStatus(msg); });
  bus.registerCallback(APPLICATION_CRC, [this](const twai_message_t& msg) { this->handleMainappCRC(msg); });
}

void CANBootloader::handleBootloaderStatus(const twai_message_t& msg)
{
  progress   = bswap32(*(reinterpret_cast<const uint32_t*>(msg.data + 0)));
  loaded_crc = *(reinterpret_cast<const uint32_t*>(msg.data + 4));

  msg_send_without_status = 0;
  if(progress == START_ADDRESS && loaded_crc == getCRC())
  {
    sendJump();
  }
}

void CANBootloader::handleMainappCRC(const twai_message_t& msg)
{
  loaded_crc = *(reinterpret_cast<const uint32_t*>(msg.data + 0));

  if(getCRC() != 0 && getCRC() != loaded_crc)
  {
    sendReboot();
  }
  else
  {
    progress = START_ADDRESS;
  }
}

void CANBootloader::sendReboot()
{
  twai_message_t message;
  memset(&message, 0, sizeof(message));
  message.identifier       = static_cast<uint32_t>(REBOOT);
  message.data_length_code = 0;
  bus.sendMessage(message);
}

void CANBootloader::sendJump()
{
  twai_message_t message;
  memset(&message, 0, sizeof(message));
  message.identifier       = static_cast<uint32_t>(JUMP);
  message.data_length_code = 0;
  bus.sendMessage(message);
}

void CANBootloader::reloadFile()
{
  buffer.reset();
}

bool CANBootloader::getData(uint32_t offset, uint8_t* data, uint32_t len)
{
  if(offset >= buffer.getSize())
  {
    reloadFile();
    if(!buffer.getSize())
    {
      return false;
    }
  }

  return buffer.getData(offset, data, len) == len;
}

uint32_t CANBootloader::getBulkCount(uint32_t offset, uint8_t size)
{
  uint8_t data[MAX_BULK_CHUNK_SIZE];
  if(!getData(offset, data, size))
  {
    return 0;
  }

  uint32_t       count    = 0;
  const uint32_t filesize = buffer.getSize();
  for(; offset < filesize - size; offset += size)
  {
    uint8_t cmpData[MAX_BULK_CHUNK_SIZE];
    if(!getData(offset, cmpData, size))
    {
      return count;
    }
    if(memcmp(data, cmpData, size) == 0)
    {
      count++;
      if(count >= MAX_BULK_REPEAT)
      {
        return count;
      }
    }
    else
    {
      return count;
    }
  }
  return count;
}

uint32_t CANBootloader::getCRC()
{
  uint32_t crc = 0;
  buffer.getData(buffer.getSize() - 4, reinterpret_cast<uint8_t*>(&crc), sizeof(crc));
  crc        = bswap32(crc);
  cached_crc = crc;
  return crc;
}

void CANBootloader::sendProgramData()
{
  twai_message_t message;
  memset(&message, 0, sizeof(message));
  message.identifier = static_cast<uint32_t>(PROGRAM_DATA);
  message.data[0]    = progress >> 16;
  message.data[1]    = progress >> 8;
  message.data[2]    = progress;

  const uint32_t last_progress = progress;
  const uint32_t offset        = progress - START_ADDRESS;

  const auto bulk = getBulkChunk(offset);
  if(bulk.size)
  {
    message.data[3] = bulk.repeat;
    if(!getData(offset, message.data + 4, bulk.size))
    {
      return;
    }
    progress += bulk.repeat * bulk.size;
    message.data_length_code = bulk.size + 4;
  }
  else
  {
    const uint32_t data_left = buffer.getSize() - offset;
    if(!getData(offset, message.data + 3, data_left >= 5 ? 5 : data_left))
    {
      return;
    }
    message.data_length_code = 8;
    progress += 5;
  }

  if(!bus.sendMessageWithSmallQueue(message, 5))
  {
    progress = last_progress;
  }
}

CANBootloader::BulkChunk CANBootloader::getBulkChunk(uint32_t offset)
{
  for(uint8_t size = 3; size > 0; size--)
  {
    uint8_t bulkCount = getBulkCount(offset, size);
    if(bulkCount > 5 / size)
    {
      return {bulkCount, size};
    }
  }
  return {0, 0};
}

void CANBootloader::step()
{
  if(progress == START_ADDRESS || cached_crc == 0)
  {
    const uint32_t crc = getCRC();
    if(crc != loaded_crc && crc != 0 && loaded_crc != 0)
    {
      printf("RESTARTING BOOTLOAD :: loaded CRC: 0x%08X, target CRC: 0x%08X, progess: 0x%08X\n", loaded_crc, crc, progress);
      sendProgramData();
    }
    else
    {
      return;
    }
  }
  else if(progress < START_ADDRESS + buffer.getSize() && msg_send_without_status < 500)
  {
    msg_send_without_status++;
    sendProgramData();
  }
}