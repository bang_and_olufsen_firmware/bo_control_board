#pragma once
#include "LEDController.hpp"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "nvs_flash.h"

class TVLEDController {
 public:
  enum class State {
    POWERING_OFF,
    OFF,
    POWERING_ON,
    ON,
  };

  TVLEDController(OneShotLEDController& red, OneShotLEDController& green);
  void                        step();
  OneShotLEDController&       getRed() { return red; }
  OneShotLEDController&       getGreen() { return green; }
  const OneShotLEDController& getRedConst() const { return red; }
  const OneShotLEDController& getGreenConst() const { return green; }

  void     gotoOn();
  void     gotoOff();
  uint64_t getTimeSinceStateChange() const;
  void     setOrangeCompensation(float val);
  void     setTransitionTime(float val);
  float    getOrangeCompensation() const { return orange_compensation; }
  float    getTransitionTime() const { return transition_time_ms; }

 private:
  void                  loadSettings();
  nvs_handle_t          nvs_handle;
  SemaphoreHandle_t     semaphore;
  OneShotLEDController& red;
  OneShotLEDController& green;
  State                 led_state       = State::OFF;
  uint64_t              last_state_time = 0;
  float                 transition_time_ms;
  float                 orange_compensation;

  void setLEDState();
  void progressState();
  void setState(State state);
};