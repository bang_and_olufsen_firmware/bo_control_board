
#include "CANBootloader/CANBootloader.hpp"
#include "SpeakerController/SpeakerController.hpp"
#include "StandController/StandController.hpp"

void               can_task_init();
StandController&   can_task_get_stand();
SpeakerController& can_task_get_speaker();
