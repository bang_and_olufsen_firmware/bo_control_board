#include "can_task.hpp"

#include <Arduino.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "CANBootloader/CANBootloader.hpp"
#include "CANController/CANController.hpp"
#include "SpeakerController/SpeakerController.hpp"
#include "StandController/StandController.hpp"
#include "driver/gpio.h"
#include <iostream>

static TaskHandle_t       can_task_handle = NULL;
static CANController      bus(GPIO_NUM_11, GPIO_NUM_10, GPIO_NUM_9, GPIO_NUM_46, GPIO_NUM_3);
static CANBootloader*     stand_bootloader   = new CANBootloader("/stand_fw.bin", BoardType::STAND, bus);
static CANBootloader*     speaker_bootloader = new CANBootloader("/speaker_fw.bin", BoardType::SPEAKER, bus);
static SpeakerController* speaker_controller;
static StandController*   stand_controller;

static void CAN_Task(void* arg);

void can_task_init()
{
  speaker_controller = new SpeakerController(bus, *speaker_bootloader);
  stand_controller   = new StandController(bus, *stand_bootloader);

  stand_bootloader->reloadFile();
  speaker_bootloader->reloadFile();
  xTaskCreatePinnedToCore(CAN_Task, "CAN Task", 5000, nullptr, 11, &can_task_handle, 1);
}

void CAN_Task(void* arg)
{
  uint32_t timer = 0;

  TickType_t lastWake = xTaskGetTickCount();
  while(true)
  {
    timer++;
    bus.recieveMessages();
    speaker_controller->step();
    bus.recieveMessages();
    stand_controller->step();
    bus.recieveMessages();

    if(timer % (portTICK_PERIOD_MS * 2000) == 0)
    {
      log_i("ALIVE!!!\n");
    }
    bus.recieveMessages();
    xTaskDelayUntil(&lastWake, 1 / portTICK_PERIOD_MS);
  }
}

StandController& can_task_get_stand()
{
  return *stand_controller;
}

SpeakerController& can_task_get_speaker()
{
  return *speaker_controller;
}