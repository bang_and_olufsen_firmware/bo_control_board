#pragma once

#include <ArduinoJson.h>

namespace Device
{
void addStates(JsonVariant& json);
void handleMessage(const JsonVariant& json);
}  // namespace Device
